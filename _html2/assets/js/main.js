
jQuery(function($) {
    var page = new page();
    var earl = new earl();

    // READY
	$(document).ready(function() {
        earl.init();
        page.init();
    });
    

    // CALL LOAD FUNCTION
    $(window).on('load', function() {
        
    });


	function page() {

		var self = this;  

		this.init = function(){
            self.jPlugin();  
            self.jScript();
            self.scrollMe();
            self.resizeMe();
        };

        // ---------------------------------------------------------------------------------------
		// ---------------------------------------------------------------- RUN PLUGIN  ----------
		// ---------------------------------------------------------------------------------------
		this.jPlugin = function() {

            // -----------> Select2
            var sl2 = $('select');
            if (sl2.length > 0) {
                sl2.each(function (i, item) {
                    var self = $(this);
                    console.log(self);
                    self.select2({
                        placeholder: self.data('placeholder')
                    });
                });
            }
        }
		
		// ---------------------------------------------------------------------------------------
		// -------------------------------------------------------------- NORMAL SCRIPT ----------
		// ---------------------------------------------------------------------------------------
		this.jScript = function() {
            var $w_ = $(window).width();

            // -----------> UPLOAD IMAGE
            $('#upload-avatar').click(function (e) {
                $('input[name="avatar"]').trigger('click');
            });
            $('input[name="avatar"]').change(function (e) {
                readURL(this);
            });

            // -----------> ADD LANGUAGE
            $("#add_language").click(function (e) {
                var lang_item = $('.language-infor').first().clone(true).find('select');
                var contain = $('<div class="row language-infor"></div>');
                var BASE_URL = window.location.origin + '/_html2/';
                
                $.each(lang_item.clone(true), function (key, item) {
                    var col = $("<div class='col-md-6'>");
                    var form = $("<div class='form-group'>");
                    form.append("<div class='ico'><img src='"+BASE_URL+"assets/images/ico-name.svg'></div>");
                    form.appendTo(col);
                    form.append($(item).removeAttr('data-select2-id').removeClass('select2-hidden-accessible'));
                    contain.append(col);
                    $('#i-register .field').append(contain);
                });
                
                // var $select = $('.select2').select2();
                // $select.each(function(i, item){
                //     var self = $(this);
                //     console.log(self);
                //     self.select2("destroy");
                // });
                
                // setTimeout(() => {
                //     var sl2 = $('select');
                //     if (sl2.length > 0) {
                //         sl2.each(function (i, item) {
                //             // console.log(item);
                //             var newSl2 = $(this).removeAttr('data-select2-id').removeClass('select2-hidden-accessible');
                //             console.log(newSl2);
                //             newSl2.select2({
                //                 placeholder: newSl2.data('placeholder')
                //             });
                //         });
                //     }
                // }, 1200);
                
            });
		}

		// ---------------------------------------------------------------------
		// -------------------------------------------- WINDOW SCROOL ----------
		// ---------------------------------------------------------------------
		this.scrollMe = function() {
            // -----------> SCROLL FUNCTION
            $(window).scroll(function() {
                var $h_ = $(window).height();
                var $srt_ = $(window).scrollTop();
                
            });
			

        }

        // ---------------------------------------------------------------------
		// -------------------------------------------- WINDOW RESIZE ----------
		// ---------------------------------------------------------------------
        this.resizeMe = function(){
            // -----------> RESIZE FUNCTION
            $(window).resize(function() {
                var $w_ = $(window).width();
                
                if ($w_ < 768) {

                } else {
                    
                }
            });
            
        }
    }


    function earl() {

        var self = this;

		this.init = function(){
            self.assLoad();
        };

        // ---------------------------------------------------------------------------------------
		// --------------------------------------------------------------- WINDOW LOAD  ----------
		// ---------------------------------------------------------------------------------------
		this.assLoad = function() {
            var $h_ = $(window).height();
            var $w_ = $(window).width();

            // ----------->
            if ($w_ < 768) {
            }
        }
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#show-image').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
});