<?php include 'header.php'; ?>

    <!-- TEAM -->
    <div id="" class="container-fluid box-section">
        <div class="main-box">
            <div class="row">
                <div class="col-12">
                    
                    <div class="bg-white mb-4 mb-md-5 pdy10 pdy-md30 pdx10 pdx-md30 bsd bdrd-10">
                        <?php 
                        for( $i= 0 ; $i <= 10 ; $i++ ) { ?>
                        
                        <!-- ITEM -->
                        <div class="bg-blue bdrd-10 bder01 pdt30 pdb10 pdx10 mb-3 mb-md-4">
                            <div class="row">
                                <div class="col-12 col-md-4 col-lg-3 text-center text-md-right mb-3 mb-md-0">
                                    <img class="mwAuto" src="assets/images/team.png" alt="">
                                </div>
                                <div class="col-12 col-md-8 col-lg-9">
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <p class="mb-1 mb-md-3"><span class="font-weight-bold clo-ora">Tên tổ đội:</span> Tổ đội 1</p>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <p class="mb-1 mb-md-3"><span class="font-weight-bold clo-ora">Nhóm trưởng:</span> Nguyễn Văn A</p>
                                        </div>
                                        <div class="col-12">
                                            <p class="mb-1"><span class="font-weight-bold clo-ora">Thông tin tổ đội:</span> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum sus pendisse ultrices gravida. Risus commodo viverra </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center text-md-right">
                                    <div class="btn btn-custom">Tham gia tổ đội</div>
                                </div>
                            </div>
                        </div>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include 'footer.php'; ?>