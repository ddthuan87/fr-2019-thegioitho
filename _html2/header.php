<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="shortcut icon" href="assets/images/favicon.ico" />
    <title>Projects - thegioitho.me</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/plugins/bootstrap-4.3.1/css/bootstrap.min.css" type="text/css" charset="utf-8" />

    <!-- Select2 -->
    <link rel="stylesheet" href="assets/plugins/select2-4.0.6/dist/css/select2.min.css" type="text/css" charset="utf-8" />

    <!-- Custom Css -->
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" charset="utf-8" />

    <!--[if IE 7]>
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->
    <!-- end: MAIN CSS -->
</head>

<body>
    
    <div id="loadding_page" style="display: none">
		<div>
			<img src="https://tgtdemo.thegioitho.com/images/loading.gif" width="50px;"/>
			<br>
			<span> Đang tải dữ liệu...</span>
		</div>
	</div>