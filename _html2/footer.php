


    <!-- Modal MAP -->
	<div id="load_map" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Kéo chọn bản đồ</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div id="map" style="width: 100%; height: 450px"></div>
				</div>
			</div>
		</div>
    </div>
    
	<!-- The Modal -->
	<div class="modal" id="messager">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Thông báo</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                
				<!-- Modal body -->
				<div class="modal-body">
					Modal body..
				</div>
			</div>
		</div>
    </div>
    
    <!-- The Modal LOGIN -->
	<div class="modal fade modal-custom modal-custom-login" id="mdLogin">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header justify-content-center">
					<h4 class="modal-title clo-ora">Đăng Nhập</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                
				<!-- Modal body -->
				<div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="" class="col-form-label">Tên Đăng Nhập:</label>
                            <input type="text" class="form-control input-custom" placeholder="Nhập Email/SĐT">
                        </div>
                        <div class="form-group">
                            <label for="" class="col-form-label">Mật Khẩu:</label>
                            <input type="password" class="form-control input-custom" placeholder="Nhập Password">
                        </div>
                        <div class="text-right">
                            <a href="javascript:;" class="font-italic">Quên mật khẩu</a>
                        </div>
                        <div class="mb-4 mt-3">
                            <button class="btn mt-0">Đăng Nhập</button>
                        </div>
                        <div class="text-center">
                            <a href="javascript:;" class="font-italic">Đăng ký tài khoản mới</a>
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</div>

	<!-- <script>
		var map;
		function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 10.7768284, lng:106.6871938},
                zoom: 13
            });
            map.addListener('center_changed', function () {
                var latLong = map.getCenter();
                $("input[name='latitude']").val(latLong.lat());
                $("input[name='longitude']").val(latLong.lng());
            });
		}
		var BASE_URL = '';
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiou5A_pAkNvTxSj-4kMmL6VYRdpy8bUY&amp;callback=initMap" async defer></script>
 -->



<!-- SCRIPT -->
<!-- start: MAIN JAVASCRIPTS -->

<!-- Jquery -->
<script src="assets/js/jquery-3.3.1.min.js"></script>

<!-- Bootstrap -->
<script src="assets/plugins/bootstrap-4.3.1/js/bootstrap.min.js"></script>

<!-- Select2 -->
<script src="assets/plugins/select2-4.0.6/dist/js/select2.min.js"></script>

<!-- My JS -->
<script src="assets/js/main.js"></script>

</body>

</html>