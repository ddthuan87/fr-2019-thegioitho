<?php include 'header.php'; ?>

    <!-- DANG KY -->
	<div id="invite-register-page" class="container-fluid">
		<div class="main-box">
			<form id="i-register" name="i_register" enctype="multipart/form-data">
                <input type="hidden" name="" value="" />
			
                <p class="sologan">
                    <span>Đăng ký thợ</span>
                    <span>Dễ dàng tìm việc với Thế Giới Thợ</span>
                </p>
                <input type="hidden" id="uid" name="uid" value="">

                <div class="field">
                    <!-- HEADLINE -->
                    <h5 class="title-field">Thông tin Cơ bản </h5>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <input type="text" class="txtFullName" name="name" id="name" placeholder="Họ và tên *">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-phone.svg" alt="">
                                </div>
                                <input type="number" name="phone" id="phone" placeholder="Số điện thoại *">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-password.svg" alt="">
                                </div>
                                <input type="password" name="password" id="password" placeholder="Mật khẩu *">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-password.svg" alt="">
                                </div>
                                <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Xác nhận mật khẩu *">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <select name="day">
                                    <option value="">Ngày sinh</option>
                                    <option value="01">1</option>
                                    <option value="02">2</option>
                                    <option value="03">3</option>
                                    <option value="04">4</option>
                                    <option value="05">5</option>
                                    <option value="06">6</option>
                                    <option value="07">7</option>
                                    <option value="08">8</option>
                                    <option value="09">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <select name="month">
                                    <option value="">Tháng sinh</option>
                                    <option value="01">1</option>
                                    <option value="02">2</option>
                                    <option value="03">3</option>
                                    <option value="04">4</option>
                                    <option value="05">5</option>
                                    <option value="06">6</option>
                                    <option value="07">7</option>
                                    <option value="08">8</option>
                                    <option value="09">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <select name="year">
                                    <option value="0">Năm sinh</option>
                                    <option value="2004">2004</option>
                                    <option value="2003">2003</option>
                                    <option value="2002">2002</option>
                                    <option value="2001">2001</option>
                                    <option value="2000">2000</option>
                                    <option value="1999">1999</option>
                                    <option value="1998">1998</option>
                                    <option value="1997">1997</option>
                                    <option value="1996">1996</option>
                                    <option value="1995">1995</option>
                                    <option value="1994">1994</option>
                                    <option value="1993">1993</option>
                                    <option value="1992">1992</option>
                                    <option value="1991">1991</option>
                                    <option value="1990">1990</option>
                                    <option value="1989">1989</option>
                                    <option value="1988">1988</option>
                                    <option value="1987">1987</option>
                                    <option value="1986">1986</option>
                                    <option value="1985">1985</option>
                                    <option value="1984">1984</option>
                                    <option value="1983">1983</option>
                                    <option value="1982">1982</option>
                                    <option value="1981">1981</option>
                                    <option value="1980">1980</option>
                                    <option value="1979">1979</option>
                                    <option value="1978">1978</option>
                                    <option value="1977">1977</option>
                                    <option value="1976">1976</option>
                                    <option value="1975">1975</option>
                                    <option value="1974">1974</option>
                                    <option value="1973">1973</option>
                                    <option value="1972">1972</option>
                                    <option value="1971">1971</option>
                                    <option value="1970">1970</option>
                                    <option value="1969">1969</option>
                                    <option value="1968">1968</option>
                                    <option value="1967">1967</option>
                                    <option value="1966">1966</option>
                                    <option value="1965">1965</option>
                                    <option value="1964">1964</option>
                                    <option value="1963">1963</option>
                                    <option value="1962">1962</option>
                                    <option value="1961">1961</option>
                                    <option value="1960">1960</option>
                                    <option value="1959">1959</option>
                                    <option value="1958">1958</option>
                                    <option value="1957">1957</option>
                                    <option value="1956">1956</option>
                                    <option value="1955">1955</option>
                                    <option value="1954">1954</option>
                                    <option value="1953">1953</option>
                                    <option value="1952">1952</option>
                                    <option value="1951">1951</option>
                                    <option value="1950">1950</option>
                                    <option value="1949">1949</option>
                                    <option value="1948">1948</option>
                                    <option value="1947">1947</option>
                                    <option value="1946">1946</option>
                                    <option value="1945">1945</option>
                                    <option value="1944">1944</option>
                                    <option value="1943">1943</option>
                                    <option value="1942">1942</option>
                                    <option value="1941">1941</option>
                                    <option value="1940">1940</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <select name="gender" id="gender">
                                    <option value="1">Nam</option>
                                    <option value="2">Nữ</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" style="margin: 15px 0px; background-color: #d8ecf3;padding: 15px; border: 2px dashed #6da8be; border-radius: 5px; margin-bottom: 15px">
                        <div class="row">
                            <div class="col-md-4 text-center" style="display: table; min-height:200px;">
                                <div style="vertical-align: middle;display: table-cell;">
                                    <img id="show-image" width="177px" src="assets/images/avatar.svg" class="img-responsive">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <p>
                                    Đăng hình dưới dạng tập tin PNG, JPG dung lượng không vượt quá 3MB.
                                </p>
                                <p>
                                    Ứng viên nên chọn ảnh nghiêm túc nhất.
                                </p>
                                <p>
                                    * Mẹo: Ứng viên đính kèm ảnh đại diện sẽ được ưu tiên hiển thị trên website.
                                </p>
                                <input  required type="file" accept="image/*" class="form-control d-none" name="avatar" placeholder="Nhập hình ảnh đại diện *">
                                <div id="upload-avatar" class="btn btn-primary">Tải hình ảnh</div>
                            </div>
                        </div>
                    </div>

                    <!-- HEADLINE -->
                    <h5 class="title-field">Thông tin địa chỉ </h5>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <input type="text"  name="email" id="email" placeholder="Email *">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <input type="text" class="txtFullName" name="company" id="company" placeholder="Tên công ty">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <input type="text"  name="identity_card" id="identity_card" placeholder="CMND">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <input type="text" name="tax_code" id="tax_code" placeholder="ĐKKD/MST">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-address.svg" alt="">
                                </div>
                                <input type="text" name="street" placeholder="Nhận số nhà và tên đường *">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-address.svg" alt="">
                                </div>
                                <select name="province_id" id="province_id">
                                    <option value="0">Chọn Tỉnh/Thành phố *</option>
                                    <option value="1">Hồ Chí Minh</option>
                                    <option value="2">Hà Nội</option>
                                    <option value="3">Đà Nẵng</option>
                                    <option value="4">Bình Dương</option>
                                    <option value="5">Đồng Nai</option>
                                    <option value="6">Khánh Hòa</option>
                                    <option value="7">Hải Phòng</option>
                                    <option value="8">Long An</option>
                                    <option value="9">Quảng Nam</option>
                                    <option value="10">Bà Rịa Vũng Tàu</option>
                                    <option value="11">Đắk Lắk</option>
                                    <option value="12">Cần Thơ</option>
                                    <option value="13">Bình Thuận  </option>
                                    <option value="14">Lâm Đồng</option>
                                    <option value="15">Thừa Thiên Huế</option>
                                    <option value="16">Kiên Giang</option>
                                    <option value="17">Bắc Ninh</option>
                                    <option value="18">Quảng Ninh</option>
                                    <option value="19">Thanh Hóa</option>
                                    <option value="20">Nghệ An</option>
                                    <option value="21">Hải Dương</option>
                                    <option value="22">Gia Lai</option>
                                    <option value="23">Bình Phước</option>
                                    <option value="24">Hưng Yên</option>
                                    <option value="25">Bình Định</option>
                                    <option value="26">Tiền Giang</option>
                                    <option value="27">Thái Bình</option>
                                    <option value="28">Bắc Giang</option>
                                    <option value="29">Hòa Bình</option>
                                    <option value="30">An Giang</option>
                                    <option value="31">Vĩnh Phúc</option>
                                    <option value="32">Tây Ninh</option>
                                    <option value="33">Thái Nguyên</option>
                                    <option value="34">Lào Cai</option>
                                    <option value="35">Nam Định</option>
                                    <option value="36">Quảng Ngãi</option>
                                    <option value="37">Bến Tre</option>
                                    <option value="38">Đắk Nông</option>
                                    <option value="39">Cà Mau</option>
                                    <option value="40">Vĩnh Long</option>
                                    <option value="41">Ninh Bình</option>
                                    <option value="42">Phú Thọ</option>
                                    <option value="43">Ninh Thuận</option>
                                    <option value="44">Phú Yên</option>
                                    <option value="45">Hà Nam</option>
                                    <option value="46">Hà Tĩnh</option>
                                    <option value="47">Đồng Tháp</option>
                                    <option value="48">Sóc Trăng</option>
                                    <option value="49">Kon Tum</option>
                                    <option value="50">Quảng Bình</option>
                                    <option value="51">Quảng Trị</option>
                                    <option value="52">Trà Vinh</option>
                                    <option value="53">Hậu Giang</option>
                                    <option value="54">Sơn La</option>
                                    <option value="55">Bạc Liêu</option>
                                    <option value="56">Yên Bái</option>
                                    <option value="57">Tuyên Quang</option>
                                    <option value="58">Điện Biên</option>
                                    <option value="59">Lai Châu</option>
                                    <option value="60">Lạng Sơn</option>
                                    <option value="61">Hà Giang</option>
                                    <option value="62">Bắc Kạn</option>
                                    <option value="63">Cao Bằng</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-address.svg" alt="">
                                </div>
                                <select name="district_id" id="district_id">
                                    <option value="0">Chọn Quận/Huyện *</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-address.svg" alt="">
                                </div>
                                <select name="ward_id" id="ward_id">
                                    <option value="0">Chọn Phường/Xã *</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group-map" style="margin-bottom: 15px;">
                                <button type="button" data-toggle="modal" data-target="#load_map" style="cursor:pointer; text-align: center; padding: 20px; border-radius: 3px; border: 0px solid #ccc; width: 100%; background-color: #bae5f5; color: #000;font-weight: bold;">
                                Vị trí trên bản đồ
                                </button>
                                <input type="hidden" name="address" id="address" placeholder="Full address">
                                <input type="hidden" name="latitude" id="latitude">
                                <input type="hidden" name="longitude" id="longitude">
                            </div>
                        </div>
                    </div>

                    <!-- HEADLINE -->
                    <h5 class="title-field">Thông tin khác </h5>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <select name="workplace[]" id="workplace" multiple data-placeholder="Nơi muốn làm việc (tối đa 5) *">
                                    <option value="0">Nơi muốn làm việc (tối đa 5) *</option>
                                    <option value="1">Hồ Chí Minh</option>
                                    <option value="2">Hà Nội</option>
                                    <option value="3">Đà Nẵng</option>
                                    <option value="4">Bình Dương</option>
                                    <option value="5">Đồng Nai</option>
                                    <option value="6">Khánh Hòa</option>
                                    <option value="7">Hải Phòng</option>
                                    <option value="8">Long An</option>
                                    <option value="9">Quảng Nam</option>
                                    <option value="10">Bà Rịa Vũng Tàu</option>
                                    <option value="11">Đắk Lắk</option>
                                    <option value="12">Cần Thơ</option>
                                    <option value="13">Bình Thuận  </option>
                                    <option value="14">Lâm Đồng</option>
                                    <option value="15">Thừa Thiên Huế</option>
                                    <option value="16">Kiên Giang</option>
                                    <option value="17">Bắc Ninh</option>
                                    <option value="18">Quảng Ninh</option>
                                    <option value="19">Thanh Hóa</option>
                                    <option value="20">Nghệ An</option>
                                    <option value="21">Hải Dương</option>
                                    <option value="22">Gia Lai</option>
                                    <option value="23">Bình Phước</option>
                                    <option value="24">Hưng Yên</option>
                                    <option value="25">Bình Định</option>
                                    <option value="26">Tiền Giang</option>
                                    <option value="27">Thái Bình</option>
                                    <option value="28">Bắc Giang</option>
                                    <option value="29">Hòa Bình</option>
                                    <option value="30">An Giang</option>
                                    <option value="31">Vĩnh Phúc</option>
                                    <option value="32">Tây Ninh</option>
                                    <option value="33">Thái Nguyên</option>
                                    <option value="34">Lào Cai</option>
                                    <option value="35">Nam Định</option>
                                    <option value="36">Quảng Ngãi</option>
                                    <option value="37">Bến Tre</option>
                                    <option value="38">Đắk Nông</option>
                                    <option value="39">Cà Mau</option>
                                    <option value="40">Vĩnh Long</option>
                                    <option value="41">Ninh Bình</option>
                                    <option value="42">Phú Thọ</option>
                                    <option value="43">Ninh Thuận</option>
                                    <option value="44">Phú Yên</option>
                                    <option value="45">Hà Nam</option>
                                    <option value="46">Hà Tĩnh</option>
                                    <option value="47">Đồng Tháp</option>
                                    <option value="48">Sóc Trăng</option>
                                    <option value="49">Kon Tum</option>
                                    <option value="50">Quảng Bình</option>
                                    <option value="51">Quảng Trị</option>
                                    <option value="52">Trà Vinh</option>
                                    <option value="53">Hậu Giang</option>
                                    <option value="54">Sơn La</option>
                                    <option value="55">Bạc Liêu</option>
                                    <option value="56">Yên Bái</option>
                                    <option value="57">Tuyên Quang</option>
                                    <option value="58">Điện Biên</option>
                                    <option value="59">Lai Châu</option>
                                    <option value="60">Lạng Sơn</option>
                                    <option value="61">Hà Giang</option>
                                    <option value="62">Bắc Kạn</option>
                                    <option value="63">Cao Bằng</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <select name="education" id="education">
                                    <option value="0">Trình độ học vấn/ chuyên môn *</option>
                                    <option value="1">Phổ thông</option>
                                    <option value="2">Trung cấp</option>
                                    <option value="3">Cao đẳng</option>
                                    <option value="4">Đại học</option>
                                    <option value="5">Sau đại học</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <select name="career[]" id="career" multiple data-placeholder="Ngành nghề (tối đa 3) *">
                                    <option value="0">Ngành nghề (tối đa 3) *</option>
                                    <option value="1">Công nhân/ Nhân viên</option>
                                    <option value="2">Quản đốc</option>
                                    <option value="3">Trợ lý giám đốc</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <select name="experience" id="experience">
                                    <option value="">Kinh nghiệm *</option>
                                    <option value="0">Chưa có</option>
                                    <option value="1">Dưới 1 năm</option>
                                    <option value="2">Từ 1 đến 3 năm</option>
                                    <option value="3">Từ 3 đến 5 năm</option>
                                    <option value="4">Từ 5 đến 10 năm</option>
                                    <option value="5">Từ 10 đến 20 năm</option>
                                    <option value="6">Trên 20 năm</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <select name="position" id="position">
                                    <option value="0">Vị trí *</option>
                                    <option value="1">Công nhân/ Nhân viên</option>
                                    <option value="2">Quản đốc/ Giám Sát/ Tổ Trưởng</option>
                                    <option value="3">Kỹ sư</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <select name="career_fields[]" id="career_fields" multiple data-placeholder="Lĩnh vực (tối đa 3) *">
                                    <option value="0">Lĩnh vực (tối đa 3) *</option>
                                    <option value="1">Cơ khí, lắp ráp</option>
                                    <option value="2">Luyện kim/ sắt thép</option>
                                    <option value="3">Da giày</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row language-infor">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <select name="language_id[]">
                                    <option value="0">Ngoại ngữ *</option>
                                    <option value="1">Không</option>
                                    <option value="2">Tiếng Anh</option>
                                    <option value="3">Tiếng Nhật</option>
                                    <option value="4">Tiếng Trung</option>
                                    <option value="5">Tiếng Pháp</option>
                                    <option value="6">Tiếng Hàn</option>
                                    <option value="7">Tây Ban Nha</option>
                                    <option value="8">Bồ Đào Nha</option>
                                    <option value="9">Đức</option>
                                    <option value="10">Ý</option>
                                    <option value="11">Thái Lan</option>
                                    <option value="12">Khác (nêu rõ tại giới thiệu bản thân)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="ico">
                                    <img src="assets/images/ico-name.svg" alt="">
                                </div>
                                <select name="language_level[]">
                                    <option value="0">Trình độ ngoại ngữ *</option>
                                    <option value="1">Không biết</option>
                                    <option value="2">Trung bình</option>
                                    <option value="3">Khá</option>
                                    <option value="4">Giỏi</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="javascript:;" id="add_language">[+] Thêm ngôn ngữ</a>
                    </div>
                </div>
                
                <button type="button" id="register-btn" class="btn">Đăng ký</button>
            </form>

            <!-- BTN -->
            <a href="javascript:;" class="butterfly-btn" data-toggle="modal" data-target="#mdLogin">Đăng Nhập</a>
		</div>
	</div>
<?php include 'footer.php'; ?>