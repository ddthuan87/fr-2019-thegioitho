<?php include 'header.php'; ?>
    
<div class="page-aqa">

    <!-- BLOCK SLIDER -->
    <section class="container-fluid sec-main-slider">
        <div class="row">
            <div class="inner">
                <div class="owl-carousel owl-theme main-slider big-slider">
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK ABOUT -->
    <section class="container-fluid pb-4 mt-4">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-about w-34"></span>
                    <h3 class="headline text-bold mt-2 mt-md-3">Thế Giới Thợ Là Gì</h3>
                    <div class="col-sm-8 offset-sm-2 text-center mt-">
                        <p class="lihe22">
                            Thế Giới Thợ là một nền tảng kết nối giữa Thợ và bạn một cách thuận tiện và hiện quả nhất. <br />
                            Ở Thế Giới Thợ, chúng tôi xây dựng một cộng đồng Thợ chuẩn mực hơn!</p>
                    </div>
                </div>
            </div>
            
            <!-- MAIN CONTENT -->
            <div class="row mt-5">
                <!-- CONTENT -->
                <div class="col-sm-12 col-lg-8 offset-lg-2 yt-wrap" style="">
                    <iframe width="100%" height="400" style=""
                        src="https://www.youtube.com/embed/IueE8Zl6Qn0" frameborder="0" 
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                        allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK REASON -->
    <section class="container-fluid pb-0 pb-md-5 mt-4">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-reason w-34"></span>
                    <h3 class="headline text-bold mt-2 mt-md-3">Lý Do Chọn Chúng Tôi</h3>
                </div>
            </div>
            
            <!-- MAIN CONTENT -->
            <div class="row mt-5">
                <!-- CONTENT -->
                <div class="col-12 col-md-4 text-center">
                    <img class="w100" src="./assets/images/icon/icon-ser-l.png" alt="">
                    <p class="text-bold clo-ora mt-4 mb-3">ĐĂNG TUYỂN NHANH CHI PHÍ THẤP</p>
                    <p class="mb-5 mb-md-0 lihe22">Chỉ với 2 bước cơ bản là Tạo gian tuyển dụng và Đăng Tin, doanh nghiệp đã có thể trải nghiệm dịch vụ MIỄN PHÍ</p>
                </div>

                <div class="col-12 col-md-4 text-center">
                    <img class="w100" src="./assets/images/icon/icon-ser-m.png" alt="">
                    <p class="text-bold clo-ora mt-4 mb-3">HỒ SƠ ĐÚNG CHUYÊN NGÀNH</p>
                    <p class="mb-5 mb-md-0 lihe22">90% ứng viên là đối tượng Công Nhân, LĐPT, Nhân Sự Nhà Máy, đảm bảo đúng ngành nghề và đối tượng nhà tuyển dụng hướng tới.</p>
                </div>

                <div class="col-12 col-md-4 text-center">
                    <img class="w100" src="./assets/images/icon/icon-ser-r.png" alt="">
                    <p class="text-bold clo-ora mt-4 mb-3">HỆ THỐNG CỘNG ĐỒNG NGHỀ LỚN</p>
                    <p class="mb-5 mb-md-0 lihe22">Chúng tôi chú trọng vào việc xây dựng và phát triển nền tảng nguồn lực, nên việc phát triển cộng đồng lao động là mũi nhọn của chúng tôi. Tổng cộng quy mô ? 300.000 thành viên.</p>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK COUNTING -->
    <section class="container-fluid sec-num py-4 py-lg-5">
        <div class="container">

            <div class="row">
                <!-- ITEM -->
                <div class="col-6 col-md-6 col-lg-3 mb-4 mb-lg-0 item-1">    
                    <div class="row align-items-center">
                        <!-- infomation -->
                        <div class="col-12 col-sm-5 text-center text-sm-right">
                            <img src="./assets/images/icon/icon-num-1.png" alt="">
                        </div>
                        <!-- couting -->
                        <div class="col-12 col-sm-7 text-center text-sm-left">
                            <p class="mb-3 mt-3 mt-md-0 text-bold text-30 clo-whi">10000</p>
                            <p class="mb-0 text-16 clo-whi">Việc làm</p>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-6 col-md-6 col-lg-3 mb-4 mb-lg-0 item-2">    
                    <div class="row align-items-center">
                        <!-- infomation -->
                        <div class="col-12 col-sm-5 text-center text-sm-right">
                            <img src="./assets/images/icon/icon-num-2.png" alt="">
                        </div>
                        <!-- couting -->
                        <div class="col-12 col-sm-7 text-center text-sm-left">
                            <p class="mb-3 mt-3 mt-md-0 text-bold text-30 clo-whi">10000</p>
                            <p class="mb-0 text-16 clo-whi">Ứng viên</p>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-6 col-md-6 col-lg-3 item-3">    
                    <div class="row align-items-center">
                        <!-- infomation -->
                        <div class="col-12 col-sm-5 text-center text-sm-right">
                            <img src="./assets/images/icon/icon-num-3.png" alt="">
                        </div>
                        <!-- couting -->
                        <div class="col-12 col-sm-7 text-center text-sm-left">
                            <p class="mb-3 mt-3 mt-md-0 text-bold text-30 clo-whi">10000</p>
                            <p class="mb-0 text-16 clo-whi">Nhà tuyển dụng</p>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-6 col-md-6 col-lg-3 item-4">    
                    <div class="row align-items-center">
                        <!-- infomation -->
                        <div class="col-12 col-sm-5 text-center text-sm-right">
                            <img src="./assets/images/icon/icon-num-4.png" alt="">
                        </div>
                        <!-- couting -->
                        <div class="col-12 col-sm-7 text-center text-sm-left">
                            <p class="mb-3 mt-3 mt-md-0 text-bold text-30 clo-whi">10000</p>
                            <p class="mb-0 text-16 clo-whi">Tài liệu nghề</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK SERVICES -->
    <section class="container-fluid pb-4 mt-4">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-reason w-34"></span>
                    <h3 class="headline text-bold mt-2 mt-md-3 mb-4 mb-md-5">Các Dịch Vụ Chính</h3>
                </div>
            </div>
            
            <!-- MAIN CONTENT -->
            <div class="row">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="row mx-sm-n2">

                        <!-- COL LEFT -->
                        <div class="col-12 px-sm-2 mb-2 md-3 mb-xl-0 tab-wrap tab-left tab-type-2">
                            <!-- TAB -->
                            <ul class="row mx-sm-n2 nav nav-tabs" id="" role="tablist">
                                <li class="col-3 col-md-3 px-sm-2 nav-item text-center">
                                    <a class="nav-link h100pr active" id="tab-1" 
                                        data-toggle="tab" href="#tab-content-1" role="tab" 
                                        aria-controls="tab-content-1" aria-selected="true">DỊCH VỤ ĐĂNG TUYỂN</a>
                                </li>
                                <li class="col-3 col-md-3 px-sm-2 nav-item text-center">
                                    <a class="nav-link h100pr" id="tab-2" 
                                        data-toggle="tab" href="#tab-content-2" role="tab" 
                                        aria-controls="tab-content-2" aria-selected="true">DỊCH VỤ GIA TĂNG</a>
                                </li>
                                <li class="col-3 col-md-3 px-sm-2 nav-item text-center">
                                    <a class="nav-link h100pr" id="tab-3" 
                                        data-toggle="tab" href="#tab-content-3" role="tab" 
                                        aria-controls="tab-content-3" aria-selected="true">DỊCH VỤ XEM HỒ SƠ ỨNG VIÊN</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content with-cus with-cus-side" id="myTab">
                                <!-- TAB 1 -->
                                <div class="tab-pane px-4 px-xl-4 fade show active" id="tab-content-1" role="tabpanel" aria-labelledby="tab-1">

                                    <div class="item-wrap">
                                        <!-- LIST SERVICES -->
                                        <div class="item py-3 py-lg-4">
                                            <div class="row" id="serVicesAccor">
                                                <div class="col-12 col-lg-4 mb-4 mb-lg-0 align-self-center text-center">
                                                    <p class="mb-0 text-bold text-uper clo-ora">Quyền lợi gian tuyển dụng</p>
                                                </div>

                                                <!-- ITEM DELUXE -->
                                                <div class="col-12 col-lg-2 mb-2 mb-lg-0 align-self-center text-center item-mb-wrap">
                                                    <div id="serDeluxeExpand" class="item-mb-title">
                                                        <div class="colapsed-cus collapsed" data-toggle="collapse" data-target="#serDeluxePanel" aria-expanded="true" aria-controls="serDeluxePanel">
                                                            <p class="mb-1 text-bold text-uper clo-ora">Deluxe</p>
                                                            <span class="text-12 clo-grey">Từ 600.000 / tháng</span>
                                                            <i class="arr clip-arrow-down-2 d-block d-lg-none"></i>
                                                        </div>
                                                    </div>

                                                    <!-- SHOW ON MOBILE, TABLET - HIDE ON DESKTOP -->
                                                    <div id="serDeluxePanel" class="collapse item-mb-content" aria-labelledby="serDeluxeExpand" data-parent="#serVicesAccor">
                                                        <div class="px-3 d-block d-lg-none">
                                                            <!-- LOOP MOBILE -->
                                                            <?php for ($x = 0; $x <= 4; $x++) { ?>
                                                            <div class="item-mb row no-gutters py-2">
                                                                <div class="col-8 text-left">
                                                                    <p class="mb-0 text-bold text-uper">Sở hữu một gian tuyển dụng riêng, đăng tuyển không giới hạn</p>
                                                                </div>
                                                                <div class="col-4"><img class="w20" src="./assets/images/icon/icon-check-services.png" alt=""></div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- ITEM VIP -->
                                                <div class="col-12 col-lg-2 mb-2 mb-lg-0 align-self-center text-center item-mb-wrap">
                                                    <div id="serVipExpand" class="item-mb-title">
                                                        <div class="colapsed-cus collapsed" data-toggle="collapse" data-target="#serVipPanel" aria-expanded="true" aria-controls="serVipPanel">
                                                            <p class="mb-1 text-bold text-uper clo-ora">Vip</p>
                                                            <span class="text-12 clo-grey">Từ 600.000 / tháng</span>
                                                            <i class="arr clip-arrow-down-2 d-block d-lg-none"></i>
                                                        </div>
                                                    </div>

                                                    <!-- SHOW ON MOBILE, TABLET - HIDE ON DESKTOP -->
                                                    <div id="serVipPanel" class="collapse item-mb-content" aria-labelledby="serVipExpand" data-parent="#serVicesAccor">
                                                        <div class="px-3 d-block d-lg-none">
                                                            <!-- LOOP MOBILE -->
                                                            <?php for ($x = 0; $x <= 4; $x++) { ?>
                                                            <div class="item-mb row no-gutters py-2">
                                                                <div class="col-8 text-left">
                                                                    <p class="mb-0 text-bold text-uper">Sở hữu một gian tuyển dụng riêng, đăng tuyển không giới hạn</p>
                                                                </div>
                                                                <div class="col-4"><img class="w20" src="./assets/images/icon/icon-check-services.png" alt=""></div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- ITEM CHUỖI -->
                                                <div class="col-12 col-lg-2 mb-2 mb-lg-0 align-self-center text-center item-mb-wrap">
                                                    <div id="serChuoiExpand" class="item-mb-title">
                                                        <div class="colapsed-cus collapsed" data-toggle="collapse" data-target="#serChuoiPanel" aria-expanded="true" aria-controls="serChuoiPanel">
                                                            <p class="mb-1 text-bold text-uper clo-ora">Chuỗi</p>
                                                            <span class="text-12 clo-grey">Từ 600.000 / tháng</span>
                                                            <i class="arr clip-arrow-down-2 d-block d-lg-none"></i>
                                                        </div>
                                                    </div>

                                                    <!-- SHOW ON MOBILE, TABLET - HIDE ON DESKTOP -->
                                                    <div id="serChuoiPanel" class="collapse item-mb-content" aria-labelledby="serChuoiExpand" data-parent="#serVicesAccor">
                                                        <div class="px-3 d-block d-lg-none">
                                                            <!-- LOOP MOBILE -->
                                                            <?php for ($x = 0; $x <= 4; $x++) { ?>
                                                            <div class="item-mb row no-gutters py-2">
                                                                <div class="col-8 text-left">
                                                                    <p class="mb-0 text-bold text-uper">Sở hữu một gian tuyển dụng riêng, đăng tuyển không giới hạn</p>
                                                                </div>
                                                                <div class="col-4"><img class="w20" src="./assets/images/icon/icon-check-services.png" alt=""></div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- ITEM SUPER VIP -->
                                                <div class="col-12 col-lg-2 mb-2 mb-lg-0 align-self-center text-center item-mb-wrap">
                                                    <div id="serSuperVipExpand" class="item-mb-title">
                                                        <div class="colapsed-cus collapsed" data-toggle="collapse" data-target="#serSuperVipPanel" aria-expanded="true" aria-controls="serSuperVipPanel">
                                                            <p class="mb-1 text-bold text-uper clo-ora">SUPER VIP</p>
                                                            <span class="text-12 clo-grey">Từ 600.000 / tháng</span>
                                                            <i class="arr clip-arrow-down-2 d-block d-lg-none"></i>
                                                        </div>
                                                    </div>

                                                    <!-- SHOW ON MOBILE, TABLET - HIDE ON DESKTOP -->
                                                    <div id="serSuperVipPanel" class="collapse item-mb-content" aria-labelledby="serSuperVipExpand" data-parent="#serVicesAccor">
                                                        <div class="px-3 d-block d-lg-none">
                                                            <!-- LOOP MOBILE -->
                                                            <?php for ($x = 0; $x <= 4; $x++) { ?>
                                                            <div class="item-mb row no-gutters py-2">
                                                                <div class="col-8 text-left">
                                                                    <p class="mb-0 text-bold text-uper">Sở hữu một gian tuyển dụng riêng, đăng tuyển không giới hạn</p>
                                                                </div>
                                                                <div class="col-4"><img class="w20" src="./assets/images/icon/icon-check-services.png" alt=""></div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- LIST SERVICES DETAIL - HIDE ON TABLE, MOBILE - SHOW ON DESKTOP -->
                                        <!-- LOOP DESKTOP -->
                                        <?php for ($x = 0; $x <= 4; $x++) { ?>
                                        <div class="item py-3 py-lg-4 d-none d-lg-block">
                                            <!-- LINE SERVICES DETAIL -->
                                            <div class="row">
                                                
                                                <div class="col col-12 col-lg-4 mb-3 mb-md-0 align-self-center text-center text-md-left">
                                                    <p class="mb-0 text-bold text-uper"><?php echo $x+1 ?>. Sở hữu một gian tuyển dụng riêng, đăng tuyển không giới hạn</p>
                                                </div>
                                                <div class="col col-12 col-lg-2 mb-3 mb-md-0 align-self-center text-center">
                                                    <p class="mb-0 text-bold text-uper"><img class="w30" src="./assets/images/icon/icon-check-services.png" alt=""></p>
                                                </div>
                                                <div class="col col-12 col-lg-2 mb-3 mb-md-0 align-self-center text-center">
                                                    <p class="mb-0 text-bold text-uper"><img class="w30" src="./assets/images/icon/icon-check-services.png" alt=""></p>
                                                </div>
                                                <div class="col col-12 col-lg-2 mb-3 mb-md-0 align-self-center text-center">
                                                    <p class="mb-0 text-bold text-uper"><img class="w30" src="./assets/images/icon/icon-check-services.png" alt=""></p>
                                                </div>
                                                <div class="col col-12 col-lg-2 mb-3 mb-md-0 align-self-center text-center">
                                                    <p class="mb-0 text-bold text-uper"><img class="w30" src="./assets/images/icon/icon-check-services.png" alt=""></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <!-- BUTTON SELECT -->
                                        <div class="item py-3 py-lg-4">
                                            <!-- LINE SERVICES -->
                                            <div class="row">
                                                <div class="col-12 col-lg-2 offset-lg-4 mb-3 mb-md-0 align-self-center">
                                                    <div class="row">
                                                        <div class="col-6 col-lg-12">
                                                            <p class="mb-2 text-bold text-uper text-center clo-ora">DELUXE</p>
                                                            <select class="select-2" name="VX">
                                                                <option value="AL">12 tháng</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-6 col-lg-12">
                                                            <p class="mb-2 mb-lg-2 mt-lg-3 text-bold text-uper text-center clo-ora">7.200.000 VNĐ</p>
                                                            <div class="text-center mb-md-4">
                                                                <a href="javascript:;" class="btn btn-custom btn-custom-mb ora text-uper">mua ngay</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-lg-2 mb-3 mb-md-0 align-self-center">
                                                    <div class="row">
                                                        <div class="col-6 col-lg-12">
                                                            <p class="mb-2 text-bold text-uper text-center clo-ora">VIP</p>
                                                            <select class="select-2" name="VX">
                                                                <option value="AL">12 tháng</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-6 col-lg-12">
                                                            <p class="mb-2 mb-lg-2 mt-lg-3 text-bold text-uper text-center clo-ora">7.200.000 VNĐ</p>
                                                            <div class="text-center mb-md-4">
                                                                <a href="javascript:;" class="btn btn-custom btn-custom-mb ora text-uper">mua ngay</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-lg-2 mb-3 mb-md-0 align-self-center">
                                                    <div class="row">
                                                        <div class="col-6 col-lg-12">
                                                            <p class="mb-2 text-bold text-uper text-center clo-ora">CHUỖI</p>
                                                            <select class="select-2" name="VX">
                                                                <option value="AL">12 tháng</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-6 col-lg-12">
                                                            <p class="mb-2 mb-lg-2 mt-lg-3 text-bold text-uper text-center clo-ora">7.200.000 VNĐ</p>
                                                            <div class="text-center mb-md-4">
                                                                <a href="javascript:;" class="btn btn-custom btn-custom-mb ora text-uper">mua ngay</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-lg-2 mb-3 mb-md-0 align-self-center">
                                                    <div class="row">
                                                        <div class="col-6 col-lg-12">
                                                            <p class="mb-2 text-bold text-uper text-center clo-ora">SUPER VIP</p>
                                                            <select class="select-2" name="VX">
                                                                <option value="AL">12 tháng</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-6 col-lg-12">
                                                            <p class="mb-2 mb-lg-2 mt-lg-3 text-bold text-uper text-center clo-ora">7.200.000 VNĐ</p>
                                                            <div class="text-center mb-md-4">
                                                                <a href="javascript:;" class="btn btn-custom btn-custom-mb ora text-uper">mua ngay</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane px-xl-4 fade" id="tab-content-2" role="tabpanel" aria-labelledby="tab-2">
                                    
                                </div>



                                <div class="tab-pane px-xl-4 fade" id="tab-content-3" role="tabpanel" aria-labelledby="tab-3">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- BLOCK WE IN MEDIA -->
    <section class="container-fluid pb-4 mt-4">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center">
                    <span class="ico ico-newSpaper w-34"></span>
                    <h3 class="headline text-bold mt-2 mt-md-3 mb-4">Khách Hàng Tiêu Biểu</h3>
                </div>
            </div>

            <div class="row">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="owl-carousel owl-theme newspaper-slider mini-slider nav-ora nav-ora-big-arr">
                        <div class="item"><img src="./assets/images/media-vtv-3.png" alt=""></div>
                        <div class="item"><img src="./assets/images/media-vtv-3.png" alt=""></div>
                        <div class="item"><img src="./assets/images/media-vtv-3.png" alt=""></div>
                        <div class="item"><img src="./assets/images/media-vtv-3.png" alt=""></div>
                        <div class="item"><img src="./assets/images/media-vtv-3.png" alt=""></div>
                        <div class="item"><img src="./assets/images/media-vtv-3.png" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
<?php include 'footer.php'; ?>