<?php include 'header.php'; ?>
    
<div class="page-ab">

    <!-- BLOCK BANNER -->
    <section class="container-fluid sec-banner">
        <div class="row">
            <div class="item-bg">
                <img class="w100p" src="./assets/images/main-banner.jpg" alt="">
            </div>
        </div>
    </section>

    <!-- BLOCK FILTER SEARCH -->
    <section class="container-fluid py-5">
        <div class="container">
            <!-- HEADLINE -->
            <form>
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-sm-12">
                    
                        <!-- Headline -->
                        <h6 class="text-bold text-uper clo-ora mb-3">Tìm kiếm việc làm</h6>
                        <!-- Title cv, phone number -->
                        <div class="form-row align-items-end">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="">Nội dung tìm kiếm<span class="clo-ora">*</span></label>
                                    <input class="form-control" type="text" 
                                    placeholder="Việc làm, tên nhà tuyển dụng">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-2">
                                <div class="form-group">
                                    <select class="select-2" name="">
                                        <option value="AL">Chọn khu vực</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-2">
                                <div class="form-group">
                                    <select class="select-2" name="">
                                        <option value="AL">Nghề nghiệp</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-custom ora text-uper h42 fullw">
                                        <i class="icofont-search-2"></i> Tìm kiếm
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <!-- FILTER ITEM -->
                    <div class="col-sm-12 d-block d-md-none mb-3">
                        <button class="btn btn-custom ora text-uper h42 fullw" 
                            type="button" data-toggle="collapse" data-target="#showFilter" 
                            aria-expanded="false" aria-controls="showFilter">
                            <i class="icofont-filter"></i> Lọc Tìm kiếm
                        </button>
                    </div>
                    <div class="col-sm-12 collapse show" id="showFilter">
                        <div class="form-row align-items-end" id="filter-se">
                            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                                <div class="form-group">
                                    <label for="">Lọc theo<span class="clo-ora">*</span></label>
                                    <button type="button" 
                                        class="btn btn-custom btn-fake ora-line text-left h42 fullw" 
                                        data-toggle="collapse" 
                                        href="#quanhuyen" role="button" 
                                        aria-expanded="false" aria-controls="quanhuyen">
                                        Quận / Huyện
                                        <i class="arr"></i>
                                    </button>
                                    <div class="collapse box-filter-wrap" id="quanhuyen" data-parent="#filter-se">
                                        <div class="py-2 px-2 mt-1 box-filter">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                                
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>

                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                                <div class="form-group">
                                    <button type="button" 
                                        class="btn btn-custom btn-fake ora-line text-left h42 fullw" 
                                        data-toggle="collapse" 
                                        href="#capbac" role="button" 
                                        aria-expanded="false" aria-controls="capbac">
                                        Cấp bậc
                                        <i class="arr"></i>
                                    </button>
                                    <div class="collapse box-filter-wrap" id="capbac" data-parent="#filter-se">
                                        <div class="py-2 px-2 mt-1 box-filter">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                                
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>

                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                                <div class="form-group">
                                    <button type="button" 
                                        class="btn btn-custom btn-fake ora-line text-left h42 fullw" 
                                        data-toggle="collapse" 
                                        href="#linhvuc" role="button" 
                                        aria-expanded="false" aria-controls="linhvuc">
                                        Lĩnh vực
                                        <i class="arr"></i>
                                    </button>
                                    <div class="collapse box-filter-wrap" id="linhvuc" data-parent="#filter-se">
                                        <div class="py-2 px-2 mt-1 box-filter">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                                
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>

                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                                <div class="form-group">
                                    <button type="button" 
                                        class="btn btn-custom btn-fake ora-line text-left h42 fullw" 
                                        data-toggle="collapse" 
                                        href="#ngoaingu" role="button" 
                                        aria-expanded="false" aria-controls="ngoaingu">
                                        Ngoại ngữ
                                        <i class="arr"></i>
                                    </button>
                                    <div class="collapse box-filter-wrap" id="ngoaingu" data-parent="#filter-se">
                                        <div class="py-2 px-2 mt-1 box-filter">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                                
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>

                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                                <div class="form-group">
                                    <button type="button" 
                                        class="btn btn-custom btn-fake ora-line text-left h42 fullw" 
                                        data-toggle="collapse" 
                                        href="#mucluong" role="button" 
                                        aria-expanded="false" aria-controls="mucluong">
                                        Mức lương
                                        <i class="arr"></i>
                                    </button>
                                    <div class="collapse box-filter-wrap" id="mucluong" data-parent="#filter-se">
                                        <div class="py-2 px-2 mt-1 box-filter">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                                
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>

                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                                <div class="form-group">
                                    <button type="button" 
                                        class="btn btn-custom btn-fake ora-line text-left h42 fullw" 
                                        data-toggle="collapse" 
                                        href="#quymo" role="button" 
                                        aria-expanded="false" aria-controls="quymo">
                                        Quy mô
                                        <i class="arr"></i>
                                    </button>
                                    <div class="collapse box-filter-wrap" id="quymo" data-parent="#filter-se">
                                        <div class="py-2 px-2 mt-1 box-filter">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                                
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>

                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                                <div class="form-group">
                                    <button type="button" 
                                        class="btn btn-custom btn-fake ora-line text-left h42 fullw" 
                                        data-toggle="collapse" 
                                        href="#xeptheo" role="button" 
                                        aria-expanded="false" aria-controls="xeptheo">
                                        Xếp theo
                                        <i class="arr"></i>
                                    </button>
                                    <div class="collapse box-filter-wrap" id="xeptheo" data-parent="#filter-se">
                                        <div class="py-2 px-2 mt-1 box-filter">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                                
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>

                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                                <div class="form-group">
                                    <button type="button" 
                                        class="btn btn-custom btn-fake ora-line text-left h42 fullw" 
                                        data-toggle="collapse" 
                                        href="#giolam" role="button" 
                                        aria-expanded="false" aria-controls="giolam">
                                        Giờ làm
                                        <i class="arr"></i>
                                    </button>
                                    <div class="collapse box-filter-wrap" id="giolam" data-parent="#filter-se">
                                        <div class="py-2 px-2 mt-1 box-filter">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                                
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>

                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                                <div class="form-group">
                                    <button type="button" 
                                        class="btn btn-custom btn-fake ora-line text-left h42 fullw" 
                                        data-toggle="collapse" 
                                        href="#khac" role="button" 
                                        aria-expanded="false" aria-controls="khac">
                                        Khác
                                        <i class="arr"></i>
                                    </button>
                                    <div class="collapse box-filter-wrap" id="khac" data-parent="#filter-se">
                                        <div class="py-2 px-2 mt-1 box-filter">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                                
                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>

                                                <div class="col-12">
                                                    <input type="checkbox" class=" iCheckF">
                                                    <label>Checkbox 1</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>            
    </section>

    <!-- BLOCK CAREER NEW -->
    <section class="container-fluid sec-career-new pb-5">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-job-new w-34"></span>
                    <h3 class="headline text-bold mt-3">Việc Làm Mới Nhất Tại HCM</h3>
                    <p class="text-16 clo-ora mt-2 mt-md-3 mb-4 mb-md-5">
                        Hiện có <span class="clo-ora text-bold">1,234</span> công việc tại HCM đang chờ bạn
                    </p>
                </div>
            </div>
            
            <div class="row">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="row mx-sm-n2 row-eq-height">

                        <!-- COL LEFT -->
                        <div class="col-sm-12 col-xl-9 px-sm-2 mb-5 mb-xl-0 tab-wrap tab-left tab-type-1">

                            <!-- TAB -->
                            <ul class="row mx-sm-n2 nav nav-tabs first-tab" id="myTabRecruitment" role="tablist">
                                <li class="col-6 px-sm-2 nav-item text-center">
                                    <a class="nav-link active  h100pr" id="new-recruitment" 
                                        data-toggle="tab" href="#recruitment" role="tab" 
                                        aria-controls="recruitment" aria-selected="true">TUYỂN DỤNG MỚI NHẤT</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content with-cus" id="myTabContent">
                                <!-- TAB 1 -->
                                <div class="tab-pane px-4 px-xl-4 fade show active" id="recruitment" role="tabpanel" aria-labelledby="new-recruitment">
                                    <div class="item-wrap">
                                        <!-- LOOP -->
                                        <?php for ($x = 0; $x <= 10; $x++) { ?>
                                        <div class="item py-4 py-lg-4">
                                            <div class="row no-gutters">
                                                <!-- TIME -->
                                                <div class="col col-12 pl-3 pl-lg-0 order-2 order-lg-1 col-cus-1 align-self-center">
                                                    <p class="text-13 mb-0 d-none d-lg-block">1 giờ trước</p>
                                                </div>
                                                
                                                <!-- CONTENT -->
                                                <div class="col col-12 order-1 order-lg-2 col-cus-2 align-self-center">
                                                    <div class="row mx-sm-n2">
                                                        
                                                        <div class="col-12 col-md-6 col-xl-6 px-sm-2">
                                                            <div class="row mx-sm-n2">
                                                                <div class="col-12 col-sm-3 col-md-4 col-xl-4 px-sm-2 align-self-center text-center">
                                                                    <img class="mw100r" src="./assets/images/k-home.png" alt="" class="">
                                                                </div>
                                                                <div class="col-12 col-sm-9 col-md-8 col-xl-8 px-sm-2 mt-3 mt-sm-0 align-self-center">
                                                                    <p class="text-bold text-uper mb-1">
                                                                        <a href="javascript:;" class="clo-ora ">Chuyên viên tư vấn tài chính</a>
                                                                    </p>
                                                                    <p class="text-13 mb-0">Công ty TNHH K-HOME</p>
                                                                    <p class="text-13 mb-0 mt-2 d-block d-lg-none">
                                                                        <i class="ico-top-1 clip-clock-2 mr-1 clo-ora"></i>1 giờ trước
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-12 col-md-6 col-xl-6 px-sm-2 align-self-center">
                                                            <div class="row mx-sm-n1">
                                                                <div class="col-sm-9 col-md-8 col-xl-8 offset-sm-3 offset-md-0 px-sm-1 align-self-center">
                                                                    <div class="row mx-sm-n1">
                                                                        <div class="col-5 col-sm-5 px-sm-1 my-2 mt-md-0">
                                                                            <p class="text-13 clo-ora mb-0">$5 - 8 triệu</p>
                                                                        </div>
                                                                        <div class="col-4 col-sm-4 px-sm-1 my-2 mt-md-0">
                                                                            <p class="text-13 mb-0"><i class="ico-top-1  clip-location"></i> HCMC</p>
                                                                        </div>
                                                                        <div class="col-3 col-sm-3 px-sm-1 my-2 mt-md-0">
                                                                            <p class="text-13 mb-0"><i class="clip-user"></i> 8</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="col-6 col-sm-6 col-md-4 col-xl-4 offset-3 offset-md-0 px-sm-1">
                                                                    <a href="javascript:;" class="btn btn-custom ora h30 no-padH fullw">Ứng tuyển</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>

                                    <!-- PAGINATION -->
                                    <nav aria-label="" class="page-pagination py-5 py-md-4">
                                        <ul class="pagination justify-content-center justify-content-md-end">
                                            <li class="page-item">
                                                <a class="page-link page-prev" href="javascript:;" aria-label="Previous">
                                                    <span aria-hidden="true"><i class="icofont-caret-left"></i></span>
                                                </a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">...</a></li>
                                            <li class="page-item">
                                                <a class="page-link page-next" href="javascript:;" aria-label="Next">
                                                    <span aria-hidden="true"><i class="icofont-caret-right"></i></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <!-- COL RIGHT -->
                        <div class="col-sm-12 col-xl-3 px-sm-2">
                            <!-- TAB -->
                            <ul class="row mx-sm-n2 nav nav-tabs with-cus-2" id="myTab" role="tablist">
                                <li class="col-sm-12 px-sm-2 nav-item text-center">
                                    <a class="nav-link active" id="hot-recruitment" 
                                        data-toggle="tab" href="#hRcru" role="tab" 
                                        aria-controls="hRcru" aria-selected="true">TIN TUYỂN DỤNG HOT</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content with-cus-2" id="myTabContent">
                                <div class="tab-pane px-4 px-xl-4  fade show active" id="hRcru" role="tabpanel" aria-labelledby="hot-recruitment">
                                    <!-- CONTENT -->
                                    <div class="row">
                                        <?php for ($x = 0; $x <= 5; $x++) { ?>
                                            <div class="col-12 col-md-6 col-xl-12 py-3 py-md-4">
                                                <div class="row">
                                                    <!-- TIME -->
                                                    <div class="col-sm-12">
                                                        <a href="javascript:;" class="text-bold text-uper clo-ora mb-1 lihe20">
                                                            Công ty cổ phần đầu tư TM quốc tế mặt trời đỏ (REDSUN)
                                                        </a>
                                                    </div>
                                                    
                                                    <!-- CONTENT -->
                                                    <div class="col-sm-12 mt-3">
                                                        <div class="row mx-xl-n1">
                                                            <div class="col-6 col-xl-4 px-1 px-md-2 px-xl-1 text-right">
                                                                <img class="mw130" src="./assets/images/ctyRedsun.jpg" alt="" class="">
                                                            </div>
                                                            <div class="col-6 col-xl-8 px-1 px-md-2 px-xl-1">
                                                                <p class="mb-0">Cần tuyển</p>
                                                                <ul class="mb-0 pl-4 pt-1 text-left">
                                                                    <li><p class="mb-1 mb-xl-0"">Công nhân</p></li>
                                                                    <li><p class="mb-1 mb-xl-0">Công nhân SX thịt</p></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                        
                                    <!-- BUTTON VIEW MORE -->
                                    <div class="text-right btn-wrap">
                                        <a href="javascript:;" class="btn btn-custom ora">Xem Thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    </div>
<?php include 'footer.php'; ?>