<?php include 'header.php'; ?>
    
<div class="page-ab">

    <!-- BLOCK SLIDER -->
    <section class="container-fluid sec-main-slider">
        <div class="row">
            <div class="inner">
                <div class="owl-carousel owl-theme main-slider big-slider">
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </section>

    
    <!-- BLOCK APPLY CV -->
    <section class="container-fluid sec-apply py-4 py-md-5">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-cmt w-34"></span>
                    <h3 class="headline text-bold mt-2 mt-md-3 mb-4 mb-md-5">Tạo Bộ Hồ Sơ Nhanh</h3>
                </div>
            </div>

            
            <form>
                <!-- P1. -->
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-sm-12">
                    
                        <!-- Headline -->
                        <h6 class="text-bold text-uper clo-ora mb-3">Phần 1: Thông Tin Cá Nhân</h6>
                        
                        <!-- Title cv, phone number -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Tiêu Đề Hồ Sơ <span class="clo-ora">*</span></label>
                                    <input class="form-control" type="text" 
                                    placeholder="VD: Xin việc thợ vẽ móng tay tại Mini Beauty">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Điện Thoại <span class="clo-ora">*</span></label>
                                    <input class="form-control" type="number" 
                                    placeholder="VD: 0909123456">
                                </div>
                            </div>
                        </div>

                        <!-- Fullname, mail -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Họ & Tên <span class="clo-ora">*</span></label>
                                    <input class="form-control" type="text" 
                                    placeholder="VD: Nguyễn Văn A">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Email <span class="clo-ora">*</span></label>
                                    <input class="form-control" type="email" 
                                    placeholder="VD: nguyenvana@gmail.com">
                                </div>
                            </div>
                        </div>

                        <!-- Date of birth, Gender -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-12 col-xl-6">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-4 col-xl-4">
                                        <div class="form-group">
                                            <label for="">Ngày, Tháng, Năm Sinh <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Ngày sinh</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 col-xl-4">
                                        <div class="form-group">
                                            <select class="select-2" name="">
                                                <option value="AL">Tháng sinh</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 col-xl-4">
                                        <div class="form-group">
                                            <select class="select-2" name="">
                                                <option value="AL">Năm sinh</option>
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-xl-6">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-4 col-xl-4">
                                        <div class="form-group">
                                            <label for="">Giới Tính <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Giới Tính</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Address -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-12 col-xl-12">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-6 col-xl-2">
                                        <div class="form-group">
                                            <label for="">Địa Chỉ Nơi Ở Hiện Tại <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Tỉnh / Thành</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-xl-2">
                                        <div class="form-group">
                                            <select class="select-2" name="">
                                                <option value="AL">Quận / Huyện</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-xl-2">
                                        <div class="form-group">
                                            <select class="select-2" name="">
                                                <option value="AL">Phường / Xã</option>
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-xl-6">
                                        <div class="form-group">
                                            <input class="form-control" type="text" 
                                            placeholder="VD: 77 Nguyễn Cơ Thạch, An Lợi Đông, Q.2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Upload -->
                        <div class="form-row mb-3">
                            <div class="col-sm-12 col-md-12 col-xl-12">
                                <div class="form-group">
                                    <label for="">Hồ Sơ Của Bạn <span class="clo-ora">*</span></label>
                                    <div class="custom-file-my">
                                        <input type="file" 
                                            name="file-1[]" 
                                            id="file-1" 
                                            class="inputfile" 
                                            data-multiple-caption='<i class="icofont-check"></i> Đã chọn {count} files' multiple="">
                                        <label for="file-1">
                                            <span class="text-bold text-uper"><i class="icofont-upload-alt"></i> Tải lên hồ sơ / Ảnh</span>
                                        </label>
                                    </div>
                                    <small id="fileHelp" class="form-text text-muted">
                                        Đăng hình dưới dạng tập tin .PNG, .JPG dung lượng không vượt quá 3MB <br />
                                        Ứng viên nên chọn ảnh nghiêm túc nhất <br />
                                        * Mẹo: Ứng viên đính kèm ảnh địa diện sẽ được ưu tiên hiển thị trên website.
                                    </small>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- P2. -->
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-sm-12">

                        <!-- Headline -->
                        <h6 class="text-bold text-uper clo-ora mb-3">Phần 2: Thông Tin Công việc</h6>

                        <!-- Which Place -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-12 col-xl-12">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-4 col-xl-2">
                                        <div class="form-group">
                                            <label for="">Nơi Muốn Làm Việc <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Tỉnh / Thành</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 col-xl-2">
                                        <div class="form-group">
                                            <select class="select-2" name="">
                                                <option value="AL">Quận / Huyện</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 col-xl-2">
                                        <div class="form-group">
                                            <select class="select-2" name="">
                                                <option value="AL">Phường / Xã</option>
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-xl-2">
                                        <div class="form-group">
                                            <label for="">Trình Độ Học Vấn <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Chọn</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-xl-4">
                                        <div class="form-group">
                                            <label for="">Kinh Nghiệm <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Chọn</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <!-- Career Type -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-12 col-xl-12">
                                        <div class="form-group">
                                            <label for="">Ngành Nghề <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Chọn</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-12 col-xl-12">
                                        <div class="form-group">
                                            <label for="">Vị Trí <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Chọn</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Foreign Language -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-12 col-xl-12">
                                        <div class="form-group">
                                            <label for="">Ngoại Ngữ <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Chọn</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-12 col-xl-12">
                                        <div class="form-group">
                                            <label for="">Trình Độ Ngoại Ngữ <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Chọn</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row mb-3 mt-3">
                            <div class="col-sm-12 col-xl-12">
                                <div class="form-group">
                                    <p class="text-12 text-bold clo-ora">
                                        Hãy luôn là ứng viên chuyên nghiệp. Nghiêm túc đến phỏng vấn theo lịch hẹn bạn nhé. Cảm ơn bạn!
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-sm-12 col-xl-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-custom ora text-uper mr-2">Nộp Hồ Sơ</button>
                                    <button type="submit" class="btn btn-custom blu text-uper">Hủy Bỏ</button>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </form>
        </div>
    </section>

</div>  
<?php include 'footer.php'; ?>