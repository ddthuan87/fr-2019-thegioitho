<?php include 'header.php'; ?>
    
<div class="page-hp">

    <!-- BLOCK SLIDER -->
    <section class="container-fluid sec-main-slider">
        <div class="row">
            <div class="inner">
                <div class="owl-carousel owl-theme main-slider big-slider">
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK SEARCH CAREER -->
    <section class="container-fluid sec-career-search">
        <div class="container with-bg">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
                    <!-- LINK -->
                    <div class="row mb-3 mb-xl-3">
                        <div class="col-sm-12 text-center">
                            <a href="javascript:;" class="text-bold text-16">Tìm việc làm <span class="mx-2 clo-ora">/</span></a>
                            <a href="javascript:;" class="text-bold text-16">Nhà tuyển dụng <span class="mx-2 clo-ora">/</span></a>
                            <a href="javascript:;" class="text-bold text-16">Hồ sơ ứng tuyển</a>
                        </div>
                    </div>

                    <form>
                        <!-- BOX SEARCH -->
                        <div class="form-row mx-sm-n1 ">
                            <div class="col-sm-12 px-sm-1 form-group mgbt-10">
                                <input type="text" class="form-control" id="" aria-describedby="" placeholder="Nhập tên công việc - nhà tuyển dụng - hoặc hồ sơ bạn cần tìm">
                            </div>
                        </div>

                        <!-- BOX FILTER -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-row mx-sm-n1 ">
                                    <div class="form-group col-12 col-md-5 col-lg-4 col-xl-5 px-sm-1 form-group mgbt-10">
                                        <select class="select-2" name="VX">
                                            <option value="AL">Tỉnh / Thành</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-12 col-md-5 col-lg-4 col-xl-5 px-sm-1 form-group mgbt-10">
                                        <select class="select-2" name="VY">
                                            <option value="AL">Nghề nghiệp</option>
                                            <option value="AL">Nghề nghiệp</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-12 col-md-2 col-lg-4 col-xl-2 px-sm-1 form-group mgbt-10">
                                        <button class="btn btn-custom ora h42 w-100">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK CAREER LOCATION -->
    <section class="container-fluid sec-career-province py-3 py-lg-4">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-3 mb-3 mb-lg-0">
                    <div class="row justìy-content-center align-items-center no-gutters">
                        <div class="col-3 col-sm-4 col-lg-3 col-xl-2 align-self-center text-right text-lg-left">
                            <span class="ico ico-locale w-34 mr-3 mr-lg-0"></span>
                        </div>
                        <div class="col-9 col-sm-8 col-lg-9 col-xl-10 text-left text-lg-left">
                            <p class="mb-0 text-bold text-16 clo-whi"> Việc làm theo Vùng / Tỉnh-Thành</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-12 col-lg-9 align-self-center">
                    <div class="owl-carousel owl-theme location-slider mini-slider ">
                        <a class="btn btn-custom ora-w fullw" href="javascript;">Thành phố Hồ Chí Minh</a>
                        <a class="btn btn-custom ora-w fullw" href="javascript;">Thành phố Hồ Chí Minh</a>
                        <a class="btn btn-custom ora-w fullw" href="javascript;">Thành phố Hồ Chí Minh</a>
                        <a class="btn btn-custom ora-w fullw" href="javascript;">Thành phố Hồ Chí Minh</a>
                        <a class="btn btn-custom ora-w fullw" href="javascript;">Thành phố Hồ Chí Minh</a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK CAREER URGENT -->
    <section class="container-fluid sec-career-urgent py-3 py-lg-4">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-job-now w-34"></span>
                    <h3 class="headline text-bold mt-2 mt-md-3 mb-4 mb-md-5">Việc Làm Tuyển Gấp</h3>
                </div>
            </div>
            
            <!-- SLIDER -->
            <div class="owl-carousel owl-theme career-urgent-slider mini-slider nav-ora nav-ora-big-arr">
                
                <!-- ITEM - 3 ITEM gom vô 1 cục, slider 3 cục -->
                <?php $i = 0; for ($x = 0; $x <= 10; $x++) { ?>
                    <?php if ($i == 0) { ?>
                    <div class="col-sm-12 px-sm-2 item">
                    <?php } ?>

                        <?php if ($i <= 2) { ?>
                        <div class="bder01 item mb-3 px-3 px-lg-2 px-xl-3 pt-3 pt-lg-2 pt-xl-3">
                            <div class="row mx-sm-n2">
                                <div class="col-sm-12 px-sm-2">
                                    <img class="brand" src="./assets/images/k-home.png" alt="" class="">
                                    <p class="text-bold text-uper mt-3 mb-1"><a href="javascript:" class="clo-ora "> Nhân viên giám sát</a></p>
                                    <p class="text-13 mb-2">Công ty TNHH K-HOME</p>
                                </div>
                            </div>
                            <div class="row mx-n1">
                                <div class="col-4 px-1">
                                    <p class="clo-ora ">$5 - 8 triệu</p>
                                </div>
                                <div class="col-4 px-1">
                                    <p class=""><i class="ico-top-1 clip-clock-2"></i> 31/12/2019</p>
                                </div>
                                <div class="col-4 px-1">
                                    <p class=""><i class="clip-user"></i> 10 người</p>
                                </div>
                            </div>
                            <a href="javascript:;" class="btn btn-custom ora h30">Ứng tuyển</a>
                        </div>
                        <?php } ?>

                    <?php if ($i == 2) { ?>
                    </div>
                    <?php } $i < 2 ? $i++ : $i = 0; ?>
                <?php } ?>

            </div>
        </div>
    </section>


    <!-- BLOCK CAREER NEW -->
    <section class="container-fluid sec-career-new pb-5">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-job-new w-34"></span>
                    <h3 class="headline text-bold mt-3">Việc Làm Mới Nhất</h3>
                    <p class="text-16 clo-ora mt-2 mt-md-3 mb-4 mb-md-5">Hiện có <span class="clo-ora text-bold">1,234</span> công việc đang chờ bạn</p>
                </div>
            </div>
            
            <div class="row">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="row mx-sm-n2 row-eq-height">

                        <!-- COL LEFT -->
                        <div class="col-sm-12 col-xl-9 px-sm-2 mb-5 mb-xl-0 tab-wrap tab-left tab-type-1">

                            <!-- TAB -->
                            <ul class="row mx-sm-n2 nav nav-tabs first-tab" id="myTabRecruitment" role="tablist">
                                <li class="col-6 px-sm-2 nav-item text-center">
                                    <a class="nav-link active  h100pr" id="new-recruitment" 
                                        data-toggle="tab" href="#recruitment" role="tab" 
                                        aria-controls="recruitment" aria-selected="true">TUYỂN DỤNG MỚI NHẤT</a>
                                </li>
                                <li class="col-6 px-sm-2 nav-item text-center">
                                    <a class="nav-link  h100pr" id="coop-foreign" 
                                        data-toggle="tab" href="#foreign" role="tab" 
                                        aria-controls="foreign" aria-selected="false">CÔNG TY LIÊN DOANH / NƯỚC NGOÀI</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content with-cus" id="myTabContent">
                                <!-- TAB 1 -->
                                <div class="tab-pane px-4 px-xl-4 fade show active" id="recruitment" role="tabpanel" aria-labelledby="new-recruitment">
                                    <div class="item-wrap">
                                        <!-- LOOP -->
                                        <?php for ($x = 0; $x <= 10; $x++) { ?>
                                        <div class="item py-4 py-lg-4">
                                            <div class="row no-gutters">
                                                <!-- TIME -->
                                                <div class="col col-12 pl-3 pl-lg-0 order-2 order-lg-1 col-cus-1 align-self-center">
                                                    <p class="text-13 mb-0 d-none d-lg-block">1 giờ trước</p>
                                                </div>
                                                
                                                <!-- CONTENT -->
                                                <div class="col col-12 order-1 order-lg-2 col-cus-2 align-self-center">
                                                    <div class="row mx-sm-n2">
                                                        
                                                        <div class="col-12 col-md-6 col-xl-6 px-sm-2">
                                                            <div class="row mx-sm-n2">
                                                                <div class="col-12 col-sm-3 col-md-4 col-xl-4 px-sm-2 align-self-center text-center">
                                                                    <img class="mw100r" src="./assets/images/k-home.png" alt="" class="">
                                                                </div>
                                                                <div class="col-12 col-sm-9 col-md-8 col-xl-8 px-sm-2 mt-3 mt-sm-0 align-self-center">
                                                                    <p class="text-bold text-uper mb-1">
                                                                        <a href="javascript:;" class="clo-ora ">Chuyên viên tư vấn tài chính</a>
                                                                    </p>
                                                                    <p class="text-13 mb-0">Công ty TNHH K-HOME</p>
                                                                    <p class="text-13 mb-0 mt-2 d-block d-lg-none">
                                                                        <i class="ico-top-1 clip-clock-2 mr-1 clo-ora"></i>1 giờ trước
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-12 col-md-6 col-xl-6 px-sm-2 align-self-center">
                                                            <div class="row mx-sm-n1">
                                                                <div class="col-sm-9 col-md-8 col-xl-8 offset-sm-3 offset-md-0 px-sm-1 align-self-center">
                                                                    <div class="row mx-sm-n1">
                                                                        <div class="col-5 col-sm-5 px-sm-1 my-2 mt-md-0">
                                                                            <p class="text-13 clo-ora mb-0">$5 - 8 triệu</p>
                                                                        </div>
                                                                        <div class="col-4 col-sm-4 px-sm-1 my-2 mt-md-0">
                                                                            <p class="text-13 mb-0"><i class="ico-top-1  clip-location"></i> HCMC</p>
                                                                        </div>
                                                                        <div class="col-3 col-sm-3 px-sm-1 my-2 mt-md-0">
                                                                            <p class="text-13 mb-0"><i class="clip-user"></i> 8</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="col-6 col-sm-6 col-md-4 col-xl-4 offset-3 offset-md-0 px-sm-1">
                                                                    <a href="javascript:;" class="btn btn-custom ora h30 no-padH fullw">Ứng tuyển</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>

                                    <!-- PAGINATION -->
                                    <nav aria-label="" class="page-pagination py-5 py-md-4">
                                        <ul class="pagination justify-content-center justify-content-md-end">
                                            <li class="page-item">
                                                <a class="page-link page-prev" href="javascript:;" aria-label="Previous">
                                                    <span aria-hidden="true"><i class="icofont-caret-left"></i></span>
                                                </a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">...</a></li>
                                            <li class="page-item">
                                                <a class="page-link page-next" href="javascript:;" aria-label="Next">
                                                    <span aria-hidden="true"><i class="icofont-caret-right"></i></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>

                                <!-- TAB 2 -->
                                <div class="tab-pane px-xl-4 fade" id="foreign" role="tabpanel" aria-labelledby="coop-foreign">
                                    
                                    <!-- PAGINATION -->
                                    <nav aria-label="" class="page-pagination py-5 py-md-4">
                                        <ul class="pagination justify-content-center justify-content-md-end">
                                            <li class="page-item">
                                                <a class="page-link page-prev" href="javascript:;" aria-label="Previous">
                                                    <span aria-hidden="true"><i class="icofont-caret-left"></i></span>
                                                </a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">...</a></li>
                                            <li class="page-item">
                                                <a class="page-link page-next" href="javascript:;" aria-label="Next">
                                                    <span aria-hidden="true"><i class="icofont-caret-right"></i></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <!-- COL RIGHT -->
                        <div class="col-sm-12 col-xl-3 px-sm-2">
                            <!-- TAB -->
                            <ul class="row mx-sm-n2 nav nav-tabs with-cus-2" id="myTab" role="tablist">
                                <li class="col-sm-12 px-sm-2 nav-item text-center">
                                    <a class="nav-link active" id="hot-recruitment" 
                                        data-toggle="tab" href="#hRcru" role="tab" 
                                        aria-controls="hRcru" aria-selected="true">TIN TUYỂN DỤNG HOT</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content with-cus-2" id="myTabContent">
                                <div class="tab-pane px-4 px-xl-4  fade show active" id="hRcru" role="tabpanel" aria-labelledby="hot-recruitment">
                                    <!-- CONTENT -->
                                    <div class="row">
                                        <?php for ($x = 0; $x <= 5; $x++) { ?>
                                            <div class="col-12 col-md-6 col-xl-12 py-3 py-md-4">
                                                <div class="row">
                                                    <!-- TIME -->
                                                    <div class="col-sm-12">
                                                        <a href="javascript:;" class="text-bold text-uper clo-ora mb-1 lihe20">
                                                            Công ty cổ phần đầu tư TM quốc tế mặt trời đỏ (REDSUN)
                                                        </a>
                                                    </div>
                                                    
                                                    <!-- CONTENT -->
                                                    <div class="col-sm-12 mt-3">
                                                        <div class="row mx-xl-n1">
                                                            <div class="col-6 col-xl-4 px-1 px-md-2 px-xl-1 text-right">
                                                                <img class="mw130" src="./assets/images/ctyRedsun.jpg" alt="" class="">
                                                            </div>
                                                            <div class="col-6 col-xl-8 px-1 px-md-2 px-xl-1">
                                                                <p class="mb-0">Cần tuyển</p>
                                                                <ul class="mb-0 pl-4 pt-1 text-left">
                                                                    <li><p class="mb-1 mb-xl-0"">Công nhân</p></li>
                                                                    <li><p class="mb-1 mb-xl-0">Công nhân SX thịt</p></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                        
                                    <!-- BUTTON VIEW MORE -->
                                    <div class="text-right btn-wrap">
                                        <a href="javascript:;" class="btn btn-custom ora">Xem Thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK RECRUITMENT CV NEW -->
    <section class="container-fluid sec-recruitment-new py-3 py-lg-5">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-cv-new w-34"></span>
                    <h3 class="headline text-bold mt-3 clo-white">Hồ sơ mới nhất</h3>
                    <p class="text-16 clo-whi mt-2 mt-md-3 mb-4 mb-md-5">Hiện có <span class="text-bold">1,234</span> ứng viên đang ứng tuyển</p>
                </div>
            </div>

            <form>    
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-sm-12">
                        <div class="form-row mx-sm-n2">
                            <div class="col-sm-6 col-md-3 px-sm-2 form-group ">
                                <input type="text" class="form-control clear" id="" aria-describedby="" placeholder="Nhập từ khóa bạn cần tìm">
                            </div>
                            <div class="col-sm-6 col-md-3 px-sm-2 form-group select-2-clear">
                                <select class="select-2 clear" name="VX">
                                    <option value="AL">Tỉnh / Thành</option>
                                </select>
                            </div>
                            <div class="col-sm-6 col-md-3 px-sm-2 form-group select-2-clear">
                                <select class="select-2" name="VX">
                                    <option value="AL">Nghề Nghiệp</option>
                                </select>
                            </div>
                            <div class="col-sm-6 col-md-3 px-sm-2 form-group ">
                                <button class="btn btn-custom whi h42 fullw">Tìm kiếm</button>
                            </div>
                        </div>     
                    </div>
                </div>
            </form>

            <div class="row">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="row mx-n2 row-eq-height ">
                        <?php for ($x = 1; $x <= 8; $x++) { ?>
                    
                        <!-- ITEM LOOP -->
                        <div class="col-6 col-md-6 col-lg-3 px-2 item mb-3">
                            <div class="row mx-n2 ">
                                <div class="col-sm-12 col-md-4 col-lg-12 px-2 ">
                                    <img class="w100p" src="./assets/images/cv.jpg" alt="">
                                </div>
                                <div class="col-sm-12 col-md-8 col-lg-12 px-2 ">
                                    <div class="wrap px-3 px-md-0 px-lg-3 pb-sm-2 py-2">
                                        <a class="text-bold text-uper clo-ora mb-1" href="javascript:;">Ngyễn Văn A</a> <span>(3<i class="icofont-star"></i>)</span>
                                        <p class="mb-0 mt-2">5 Năm lĩnh vực sản xuất</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>     
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK RECRUITMENT TOP -->
    <section class="container-fluid sec-recruitment-top py-3 py-lg-5">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-rcv-top w-34"></span>
                    <h3 class="headline text-bold mt-3 clo-ora">Nhà tuyển dụng nổi bật</h3>
                    <p class="text-16 clo-ora mt-2 mt-md-3 mb-4 mb-md-5">Hiện có <span class="text-bold">1,234</span> nhà tuyển dụng đã đăng tin tuyển dụng</p>
                </div>
            </div>

            <form>    
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-sm-12">
                        <div class="form-row mx-sm-n2">
                            <div class="col-sm-6 col-md-3 px-sm-2 form-group ">
                                <select class="select-2" name="VX">
                                    <option value="AL">Toàn Quốc</option>
                                </select>
                            </div>
                            <div class="col-sm-6 col-md-3 px-sm-2 form-group">
                                <select class="select-2" name="VX">
                                    <option value="AL">Tỉnh / Thành</option>
                                </select>
                            </div>
                            <div class="col-sm-6 col-md-3 px-sm-2 form-group ">
                                <select class="select-2" name="VX">
                                    <option value="AL">Tỉnh / Thành</option>
                                </select>
                            </div>
                            <div class="col-sm-6 col-md-3 px-sm-2 form-group ">
                                <button class="btn btn-custom ora h42 fullw">Tìm kiếm</button>
                            </div>
                        </div>     
                    </div>
                </div>
            </form>

            <div class="row">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="row mx-sm-n2">

                        <!-- COL LEFT -->
                        <div class="col-sm-12 col-lg-9 px-sm-2 mb-5 mb-xl-0">
                            <!-- TAB -->
                            <ul class="row mx-sm-n2 nav nav-tabs" id="" role="tablist">
                                <li class="col-sm-12 col-sm-4 px-sm-2 nav-item text-center">
                                    <a class="nav-link active" id="home-tab" 
                                        data-toggle="tab" href="#home" role="tab" 
                                        aria-controls="home" aria-selected="true">TUYỂN DỤNG MỚI NHẤT</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content with-cus with-cus-side" id="">
                                <!-- TAB 1 -->
                                <div class="tab-pane px-4 px-xl-4 fade show active" id="top-recrui-content" role="tabpanel" aria-labelledby="">
                                    <div class="row mx-n2">
                                        <!-- LOOP ROW -->
                                    <?php for ($x = 1; $x <= 9; $x++) { ?>
                                        <!-- ITEM -->
                                        <div class="col-12 col-sm-6 col-md-4 px-2 pt-4">
                                            <div class="item-top-recru bder01">
                                                <div class="row mx-n2">
                                                    <div class="col-xl-12 px-2 py-3">
                                                        <div class="px-2 text-center">
                                                            <img class="mw200" src="./assets/images/ba-tam.png" alt="" class="">
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-xl-12 px-2 mt-2">
                                                        <div class="px-2">
                                                            <a class="text-bold text-uper clo-ora" href="javascript:;">Hệ thống siêu thị bigc việt nam</a>
                                                            <p class="text-13 my-2">163 phan đăng lưu, phường 1, HCM, Việt Nam</p>
                                                            <p class="text-13 mb-2">Quy mô: Trên <span class="text-bold">5000</span></p>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <?php } ?>
                                    </div>

                                    <!-- PAGINATION -->
                                    <nav aria-label="" class="page-pagination py-5 py-md-4">
                                        <ul class="pagination justify-content-center justify-content-md-end">
                                            <li class="page-item">
                                                <a class="page-link page-prev" href="javascript:;" aria-label="Previous">
                                                    <span aria-hidden="true"><i class="icofont-caret-left"></i></span>
                                                </a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">...</a></li>
                                            <li class="page-item">
                                                <a class="page-link page-next" href="javascript:;" aria-label="Next">
                                                    <span aria-hidden="true"><i class="icofont-caret-right"></i></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <!-- COL RIGHT -->
                        <div class="col-sm-12 col-lg-3 px-sm-2">
                            <!-- TAB -->
                            <ul class="row mx-sm-n2 nav nav-tabs with-cus-2" id="" role="tablist">
                                <li class="col-sm-12 px-sm-2 nav-item text-center">
                                    <a class="nav-link active" id="abc-tab" 
                                        data-toggle="tab" href="#abc" role="tab" 
                                        aria-controls="abc" aria-selected="true">TIN TỨC</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content with-cus-2" id="">
                                <div class="tab-pane px-4 px-xl-4 with-cus fade show active" id="" role="tabpanel" aria-labelledby="">
                                    <div class="row">
                                    <!-- ITEM LOOP ROW -->
                                    <?php for ($x = 0; $x <= 6; $x++) { ?>
                                
                                        <!-- CONTENT -->
                                        <div class="col-sm-6 col-md-3 col-lg-12 py-3 py-lg-4">
                                
                                            <div class="row mx-lg-n2">
                                                <div class="col-lg-4 px-lg-2 mb-3 mb-lg-0 text-center">
                                                    <img class="mw100 top1" src="./assets/images/ctyRedsun.jpg" alt="" class="">
                                                </div>
                                                <div class="col-lg-8 px-lg-2">
                                                    <a class="text-bold text-uper mb-1" href="javascript:;">5 sai lầm ứng viên thường mắc phải</a>
                                                    <p class="clo-grey mb-1"><span>10/6/2019</span></p>
                                                </div>
                                            </div>

                                        </div>
                                        
                                    <?php } ?>
                                    </div>

                                    <!-- BUTTON VIEW MORE -->
                                    <div class="text-right btn-wrap">
                                        <a href="javascript:;" class="btn btn-custom ora">Xem Thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK CAREEER FIND BY JOB -->
    <section class="container-fluid sec-find-by-job pdy50">
        <div class="container">

            <div class="row">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="row ">
                        <!-- COL LEFT -->
                        <div class="col-sm-12 col-xl-12">
                            <!-- TAB -->
                            <ul class="row mx-n2 nav nav-tabs" id="myTab-find-job-by" role="tablist">
                                <li class="col-3 px-2 nav-item text-center">
                                    <a class="nav-link h100pr active" id="find-job-1" 
                                        data-toggle="tab" href="#find-job-ct-1" role="tab" 
                                        aria-controls="find-job-ct-1" aria-selected="true">NGHỀ NGHIỆP</a>
                                </li>
                                <li class="col-3 px-2 nav-item text-center">
                                    <a class="nav-link h100pr" id="find-job-2" 
                                        data-toggle="tab" href="#find-job-ct-2" role="tab" 
                                        aria-controls="find-job-ct-2" aria-selected="true">ĐỊA ĐIỂM</a>
                                </li>
                                <li class="col-3 px-2 nav-item text-center">
                                    <a class="nav-link h100pr" id="find-job-3" 
                                        data-toggle="tab" href="#find-job-ct-3" role="tab" 
                                        aria-controls="find-job-ct-3" aria-selected="true">LĨNH VỰC</a>
                                </li>
                                <li class="col-3 px-2 nav-item text-center">
                                    <a class="nav-link h100pr" id="find-job-4" 
                                        data-toggle="tab" href="#find-job-ct-4" role="tab" 
                                        aria-controls="find-job-ct-4" aria-selected="true">CHỨC VỤ</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content" id="myTab-find-job-by-Content">
                                <!-- TAB 1 -->
                                <div class="tab-pane fade show active" id="find-job-ct-1" role="tabpanel" aria-labelledby="find-job-1">
                                    
                                    <!-- HEADLINE -->
                                    <div class="row">
                                        <div class="col-sm-12 text-center ">
                                            <h3 class="headline text-bold mt-3 mt-md-5">Tìm kiếm công việc theo nghề nghiệp</h3>
                                            <p class="text-16 clo-ora mt-2 mt-md-3 mb-4 mb-md-5">Hiện tại có <span class="clo-ora text-bold">1,234</span> công việc đang chờ bạn</p>
                                        </div>
                                    </div>

                                    <div class="wrap-item px-5 px-md-5 pb-3 pb-md-5 mx-md-5 ">
                                        <div class="row">
                                            <!-- LOOP ITEM -->
                                            <?php for ($x = 1; $x <= 13; $x++) { ?>
                                                <!-- ITEM -->
                                                <div class="col-sm-6 col-lg-4  mb-2 item">
                                                    <a href="javascript:;" class="">Công nhân / Nhân viên <span>(1333)</span></a>
                                                </div>
                                                <div class="col-sm-6 col-lg-4  mb-2 item item-spec">
                                                    <a href="javascript:;" class="">Công nhân / Nhân viên <span>(1333)</span></a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                                <!-- TAB 2 -->
                                <div class="tab-pane fade" id="find-job-ct-2" role="tabpanel" aria-labelledby="find-job-2">
                                    <div class="row">
                                        ........... 2
                                    </div>
                                </div>

                                <!-- TAB 3 -->
                                <div class="tab-pane fade" id="find-job-ct-3" role="tabpanel" aria-labelledby="find-job-3">
                                    <div class="row">
                                        ........... 3
                                    </div>
                                </div>

                                <!-- TAB 4 -->
                                <div class="tab-pane fade" id="find-job-ct-4" role="tabpanel" aria-labelledby="find-job-4">
                                    <div class="row">
                                        ............. 4
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK WE IN MEDIA -->
    <section class="container-fluid pb-4 mt-4">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center">
                    <span class="ico ico-newSpaper w-34"></span>
                    <h3 class="headline text-bold mt-2 mt-md-3 mb-4">Khách Hàng Tiêu Biểu</h3>
                </div>
            </div>

            <div class="row">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="owl-carousel owl-theme newspaper-slider mini-slider nav-ora nav-ora-big-arr">
                        <div class="item"><img src="./assets/images/media-vtv-3.png" alt=""></div>
                        <div class="item"><img src="./assets/images/media-vtv-3.png" alt=""></div>
                        <div class="item"><img src="./assets/images/media-vtv-3.png" alt=""></div>
                        <div class="item"><img src="./assets/images/media-vtv-3.png" alt=""></div>
                        <div class="item"><img src="./assets/images/media-vtv-3.png" alt=""></div>
                        <div class="item"><img src="./assets/images/media-vtv-3.png" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK SUPPORT -->
    <section class="container-fluid sec-support pt-3 pt-xl-5">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center">
                    <span class="ico ico-support w-34"></span>
                    <h3 class="headline text-bold mt-3">Tổng đài tư vấn và hỗ trợ ứng viên</h3>
                    <a class="btn btn-custom ora-line big-text mt-4" href="tel:1800 1237">1800 1237</a>
                </div>
            </div>

            <div class="row">
                <!-- CONTENT -->
                <div class="col-sm-12 text-center">
                    <h3 class="headline text-bold mb-3 mb-sm-3 mt-3 mt-sm-4 mt-xl-5">Tư vấn nhà tuyển dụng</h3>
                </div>

                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12 col-lg-4">
                            <p class="text-15 text-uper text-bold mb-sm-3 clo-ora">Miền bắc (Từ Huế trở ra)</p>
                            <div class="row">
                            <!-- LOOP -->
                            <?php for ($x = 0; $x <= 1; $x++) { ?>
                                <!-- ITEM -->
                                <div class="col-sm-6 col-lg-12 mb-3 mb-sm-5 item-support">
                                    <div class="row mx-n1">
                                        <!-- avatar -->
                                        <div class="col-4 px-1">
                                            <img src="./assets/images/cv-sq.png" alt="">
                                        </div>
                                        <!-- infomation -->
                                        <div class="col-8 px-1 align-self-center">
                                            <p class="text-15 text-uper text-bold mb-xl-1">Mr. Đặng Văn Tuấn</p>
                                            <span class="logo-skype"></span>
                                            <a href="tel:0909 090909" class="skype-phone text-bold">0909 090909</a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            </div>
                        </div>

                        <div class="col-sm-12 col-lg-8">
                            <p class="text-15 text-uper text-bold mb-xl-3 clo-ora">Miền nam (Từ Đà nẵng trở vào)</p>
                            <div class="row">
                                <!-- LOOP -->
                                <?php for ($x = 0; $x <= 2; $x++) { ?>
                                    <!-- ITEM -->
                                    <div class="col-sm-6 col-lg-6 mb-3 mb-sm-5 item-support">
                                        <div class="row mx-n1">
                                            <!-- avatar -->
                                            <div class="colm-4 px-1">
                                                <img src="./assets/images/cv-sq.png" alt="">
                                            </div>
                                            <!-- infomation -->
                                            <div class="colm-8 px-1 align-self-center">
                                                <p class="text-15 text-uper text-bold mb-xl-1">Mr. Đặng Văn Tuấn</p>
                                                <span class="logo-skype"></span>
                                                <a href="tel:0909 090909" class="skype-phone text-bold">090 090909</a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK COUNTING -->
    <section class="container-fluid sec-num py-4 py-lg-5">
        <div class="container">

            <div class="row">
                <!-- ITEM -->
                <div class="col-6 col-md-6 col-lg-3 col-xl-3 mb-4 mb-lg-0 item-1">    
                    <div class="row align-items-center">
                        <!-- infomation -->
                        <div class="col-12 col-sm-5 text-center text-sm-right">
                            <img src="./assets/images/icon/icon-num-1.png" alt="">
                        </div>
                        <!-- couting -->
                        <div class="col-12 col-sm-7 text-center text-sm-left">
                            <p class="mb-3 mt-3 mt-md-0 text-bold text-30 clo-whi">10000</p>
                            <p class="mb-0 text-16 clo-whi">Việc làm</p>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-6 col-md-6 col-lg-3 mb-4 mb-lg-0 item-2">    
                    <div class="row align-items-center">
                        <!-- infomation -->
                        <div class="col-12 col-sm-5 text-center text-sm-right">
                            <img src="./assets/images/icon/icon-num-2.png" alt="">
                        </div>
                        <!-- couting -->
                        <div class="col-12 col-sm-7 text-center text-sm-left">
                            <p class="mb-3 mt-3 mt-md-0 text-bold text-30 clo-whi">10000</p>
                            <p class="mb-0 text-16 clo-whi">Ứng viên</p>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-6 col-md-6 col-lg-3 item-3">    
                    <div class="row align-items-center">
                        <!-- infomation -->
                        <div class="col-12 col-sm-5 text-center text-sm-right">
                            <img src="./assets/images/icon/icon-num-3.png" alt="">
                        </div>
                        <!-- couting -->
                        <div class="col-12 col-sm-7 text-center text-sm-left">
                            <p class="mb-3 mt-3 mt-md-0 text-bold text-30 clo-whi">10000</p>
                            <p class="mb-0 text-16 clo-whi">Nhà tuyển dụng</p>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-6 col-md-6 col-lg-3 item-4">    
                    <div class="row align-items-center">
                        <!-- infomation -->
                        <div class="col-12 col-sm-5 text-center text-sm-right">
                            <img src="./assets/images/icon/icon-num-4.png" alt="">
                        </div>
                        <!-- couting -->
                        <div class="col-12 col-sm-7 text-center text-sm-left">
                            <p class="mb-3 mt-3 mt-md-0 text-bold text-30 clo-whi">10000</p>
                            <p class="mb-0 text-16 clo-whi">Tài liệu nghề</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    </div>
<?php include 'footer.php'; ?>