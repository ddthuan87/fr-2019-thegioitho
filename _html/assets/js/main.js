
jQuery(function($) {
    var page = new page();
    var earl = new earl();

    // READY
	$(document).ready(function() {
        earl.init();
        page.init();
    });
    

    // CALL LOAD FUNCTION
    $(window).on('load', function() {
        
    });


	function page() {

		var self = this;  

		this.init = function(){
            self.jPlugin();  
            self.jScript();
            self.scrollMe();
            self.resizeMe();
        };

        // ---------------------------------------------------------------------------------------
		// ---------------------------------------------------------------- RUN PLUGIN  ----------
		// ---------------------------------------------------------------------------------------
		this.jPlugin = function() {

            // -----------> Editor
            if ($('.editor').length > 0) {
                $('.editor').trumbowyg({
                    btns: [
                        // ['viewHTML'],
                        ['undo', 'redo'], // Only supported in Blink browsers
                        // ['formatting'],
                        // ['strong', 'em', 'del'],
                        // ['superscript', 'subscript'],
                        // ['link'],
                        // ['insertImage'],
                        // ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                        ['unorderedList', 'orderedList'],
                        // ['horizontalRule'],
                        // ['removeformat'],
                        // ['fullscreen']
                    ]
                });
            }

            // -----------> Select2
            var sl2 = $('.select-2');
            if (sl2.length > 0) {
                sl2.select2({});
                $.fn.select2.defaults.set("theme", "bootstrap");
            }

            // -----------> iCheck
            var inp = $('input.iCheckC');
            var inpF = $('input.iCheckF');
            var inpO = $('input.iCheckO');
            if (inp.length > 0) {
                // inp.iCheck({
                //     checkboxClass: 'icheckbox_square-custom',
                //     radioClass: 'iradio_square-custom',
                //     increaseArea: '100%' // optional
                // });

                inp.iCheck({
                    checkboxClass: 'icheckbox_minimal-orange',
                    radioClass: 'iradio_minimal-orange',
                    increaseArea: '20%' // optional
                });
            }

            if (inpF.length > 0) {

                inpF.each(function(){
                    var self = $(this),
                        label = self.next(),
                        label_text = label.text();
                
                    label.remove();
                    self.iCheck({
                        checkboxClass: 'icheckbox_line-orange icheckbox_filter',
                        radioClass: 'iradio_line-orange',
                        insert: '<div class="icheck_line-icon"></div>' + label_text
                    });
                });
            }

            if (inpO.length > 0) {

                inpO.iCheck({
                    checkboxClass: 'icheckbox_square-orange',
                    radioClass: 'iradio_square-orange',
                    increaseArea: '20%' // optional
                });
            }

            // -----------> rating
            var rating = $('#rating-set');
            rating.rating();

            // -----------> Owl Carousel
            // Owl Carousel DOM Elements
            var sliderMain = $('.main-slider');
            var sliderLocation = $('.location-slider');
            var sliderCareerUrgent = $('.career-urgent-slider');
            var sliderNewspaper = $('.newspaper-slider');
            
            // Initialize plugin
            sliderMain.owlCarousel({
                loop:true,
                // margin:10,
                // nav:true,
                responsive:{
                    0: {
                        items: 1
                    }
                }
            });

            sliderLocation.owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                dots:false,
                responsive:{
                    0: {
                        items: 2
                    },
                    576: {
                        items: 2
                    },
                    768: {
                        items: 3
                    },
                    992: {
                        items: 3
                    },
                    1170: {
                        items: 3
                    }
                }
            });

            sliderCareerUrgent.owlCarousel({
                loop:false,
                // margin:10,
                dots:false,
                nav:true,
                responsive:{
                    0: {
                        items: 1
                    },
                    576: {
                        items: 1
                    },
                    768: {
                        items: 2
                    },
                    992: {
                        items: 3
                    },
                    1170: {
                        items: 3
                    }
                }
            });

            sliderNewspaper.owlCarousel({
                loop:true,
                margin:20,
                nav:true,
                dots:false,
                responsive:{
                    0: {
                        items: 3
                    },
                    576: {
                        items: 4
                    },
                    768: {
                        items: 5
                    },
                    992: {
                        items: 5
                    },
                    1170: {
                        items: 5
                    }
                }
            });
            
        }
		
		// ---------------------------------------------------------------------------------------
		// -------------------------------------------------------------- NORMAL SCRIPT ----------
		// ---------------------------------------------------------------------------------------
		this.jScript = function() {
            var $w_ = $(window).width();

            $('#filter-se').find('.form-group').on({
                mouseenter: function () {
                    
                },
                mouseleave: function () {
                    
                    if ($('#filter-se .collapse').hasClass('show')) {
                        $('#filter-se .collapse').removeClass('show');
                        $('#filter-se .btn-fake').addClass('collapsed').attr('aria-expanded', false);
                    }
                }
            });

            // ------------> OPEN TAB PASSWORD IN MODAL
            jQuery('#forgetMe').on('click', function() {
                jQuery('#tab-2')[0].click();
            });
		}

		// ---------------------------------------------------------------------
		// -------------------------------------------- WINDOW SCROOL ----------
		// ---------------------------------------------------------------------
		this.scrollMe = function() {
            var sticky = $('#sidebar').offset().top;

            // -----------> SCROLL FUNCTION
            $(window).scroll(function() {
                var $h_ = $(window).height();
                var $srt_ = $(window).scrollTop();

                if ($srt_ >= sticky) {
                    $('#sidebar').addClass('sticky');
                } else {
                    $('#sidebar').removeClass('sticky');
                }
                
            });
			

        }

        // ---------------------------------------------------------------------
		// -------------------------------------------- WINDOW RESIZE ----------
		// ---------------------------------------------------------------------
        this.resizeMe = function(){
            // -----------> RESIZE FUNCTION
            $(window).resize(function() {
                var $w_ = $(window).width();
                
                if ($w_ < 768) {
                    if ($('#showFilter').hasClass('show')) {
                        $('#showFilter').removeClass('show');
                    }
                } else {
                    $('#showFilter').addClass('show');
                }
            });
            
        }
    }


    function earl() {

        var self = this;

		this.init = function(){
            self.assLoad();
        };

        // ---------------------------------------------------------------------------------------
		// --------------------------------------------------------------- WINDOW LOAD  ----------
		// ---------------------------------------------------------------------------------------
		this.assLoad = function() {
            var $h_ = $(window).height();
            var $w_ = $(window).width();

            // ----------->
            if ($w_ < 768) {
                if ($('#showFilter').hasClass('show')) {
                    $('#showFilter').removeClass('show');
                }
            }
        }
    }
});

// SHOW MODAL NOTIFICATION
function showModalErr(title, content, type) {
    
    $('#mdError').on('shown.bs.modal', function (e) {
        // var ele = $(e.relatedTarget);
        var modal = $(this);

        modal.find('.modal-title').text(title)
        modal.find('.modal-body p').text(content)
    });
}
