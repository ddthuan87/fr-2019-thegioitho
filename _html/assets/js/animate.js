	/**
	 *  @name animate
	 *  @version 1.0
	 */
	(function($, window, undefined) {
	"use strict";

	var pluginName = "animate";

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend(
		{},
		$.fn[pluginName].defaults,
		this.element.data(),
		options
		);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
		var that = this,
			ele = that.element;

		var tl = new TimelineLite();

		// tl.to(ele.find('[data-tower]'), 0.3, {bottom: 0, ease: Back.easeOut.config(0.5)})
		//     .to(ele.find('[data-tower-light]'), 1, {opacity: 1}, 'step1')
		//     .to(ele.find('[data-warior-1]'), 0.5, {bottom: "11.41%", left: 0, ease: Back.easeOut.config(0.7)}, '0.3')
		//     .to(ele.find('[data-warior-2]'), 0.5, {bottom: 0, right: 0, ease: Back.easeOut.config(0.7)}, '0.4')
		//     .to(ele.find('[data-txt-1]'), 1, {css: {scaleX: 1, scaleY: 1, transformOrigin: "center center"}, ease: Bounce.easeOut}, '0.4')
		//     .to(ele.find('[data-txt-2]'), .7, {top: '40.51%', left: '28.125%', opacity: 1, ease: Bounce.easeOut}, '0.4')
		//     .to(ele.find('[data-txt-3]'), .7, {top: '48.84%', left: '33.26%', opacity: 1, ease: Bounce.easeOut}, '0.4')
		//     .to(ele.find('[data-join]'), .7, {top: '55.25%', left: '40.13%', opacity: 1, ease: Bounce.easeOut}, '0.4');

		tl.to(ele.find("[data-tower]"), 0.3, {
			bottom: 0,
			ease: Back.easeOut.config(0.5)
		})
			.to(ele.find("[data-tower-1]"), 0.5, { opacity: 1, bottom: 0 })
			.to(ele.find("[data-sun]"), 0.5, { opacity: 1 })
			.to(ele.find("[data-sun-1]"), 0.7, { opacity: 1, top: 0 })
			.to(ele.find("[data-fire]"), 0.7, { opacity: 1 })
			.to(ele.find("[data-fire-1]"), 0.7, { opacity: 1 })
			.to(ele.find("[data-warior-1]"), 0.5, {
			bottom: 0,
			left: 0,
			ease: Back.easeOut.config(0.7)
			})
			.to(ele.find("[data-warior-2]"), 0.5, {
			bottom: 0,
			right: 0,
			ease: Back.easeOut.config(0.7)
			})
			.to(ele.find("[data-txt-1]"), 0.5, {
			css: { scaleX: 1, scaleY: 1, transformOrigin: "center center" },
			ease: Bounce.easeOut
			})
			.to(ele.find("[data-txt-2]"), 0.5, {
			top: "40.51%",
			left: "28.125%",
			opacity: 1,
			ease: Bounce.easeOut
			})
			.to(ele.find("[data-txt-3]"), 0.5, {
			top: "48.84%",
			left: "33.26%",
			opacity: 1,
			ease: Bounce.easeOut
			})
			.to(ele.find("[data-join]"), 0.5, {
			top: "55.25%",
			left: "40.13%",
			opacity: 1,
			ease: Bounce.easeOut
			});
		},
		destroy: function() {
		// remove events
		// deinitialize
		$.removeData(this.element[0], pluginName);
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
		var instance = $.data(this, pluginName);
		if (!instance) {
			$.data(this, pluginName, new Plugin(this, options));
		} else if (instance[options]) {
			instance[options](params);
		}
		});
	};

	$.fn[pluginName].defaults = {};

	$(function() {
		$("[data-" + pluginName + "]").on("customEvent", function() {
		// to do
		});

		$("[data-" + pluginName + "]")[pluginName]({
		key: "custom"
		});
	});
	})(jQuery, window);

	