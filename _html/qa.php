<?php include 'header.php'; ?>
    
<div class="page-aqa">

    <!-- BLOCK SLIDER -->
    <section class="container-fluid sec-main-slider">
        <div class="row">
            <div class="inner">
                <div class="owl-carousel owl-theme main-slider big-slider">
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK QA -->
    <section class="container-fluid py-3 py-xl-4">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-qa w-34"></span>
                    <h3 class="headline text-bold mt-2 mt-md-3 mb-4 mb-md-5">Hỏi Đáp & Đánh Giá</h3>
                </div>
            </div>
            
            <!-- MAIN CONTENT -->
            <div class="row">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="row mx-sm-n2">

                        <!-- COL LEFT -->
                        <div class="col-sm-12 col-xl-9 px-sm-2 mb-5 mb-xl-0 tab-wrap tab-left tab-type-1">
                            <!-- TAB -->
                            <ul class="row mx-sm-n2 nav nav-tabs" id="" role="tablist">
                                <li class="col-sm-12 col-md-8 px-sm-2 nav-item text-center">
                                    <a class="nav-link active" id="tab-1" 
                                        data-toggle="tab" href="#tab-content-1" role="tab" 
                                        aria-controls="tab-content-1" aria-selected="true">HỎI ĐÁP VỚI THEGIOITHO.COM</a>
                                </li>
                                <li class="col-sm-12 col-md-4 px-sm-2 nav-item text-center">
                                    <a class="nav-link" id="tab-2" 
                                        data-toggle="tab" href="#tab-content-2" role="tab" 
                                        aria-controls="tab-content-2" aria-selected="true">GÓP Ý / NHẬN XÉT</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content with-cus with-cus-side" id="myTab">
                                <!-- TAB 1 -->
                                <div class="tab-pane px-4 px-xl-4 fade show active" id="tab-content-1" role="tabpanel" aria-labelledby="tab-1">

                                    <div class="item-wrap">
                                        <div class="item py-3 py-lg-4">
                                            <div class="row no-gutters">
                                                <!-- ANSWER -->
                                                <div class="col col-12 col-md-2 col-cus-1 mb-3 mb-md-0 align-self-center text-center text-md-left">
                                                    <p class="mb-0 text-bold text-uper clo-ora">Câu Trả Lời</p>
                                                </div>
                                                
                                                <!-- CONTENT -->
                                                <div class="col col-12 col-md-10 col-cus-2 align-self-center">
                                                    <div class="row mx-sm-n2">
                                                        <div class="col-sm-12 px-sm-2 align-self-center  text-center text-md-left">
                                                            <a href="javascript:;" class="btn btn-custom ora text-uper">Tạo câu hỏi của bạn</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <!-- LOOP -->
                                        <?php for ($x = 0; $x <= 3; $x++) { ?>
                                        <div class="item py-3 py-lg-4">
                                            <div class="row no-gutters">
                                                <!-- ANSWER -->
                                                <div class="col col-12 col-md-2 col-cus-1 mb-3 mb-md-0 align-self-center text-center text-md-left">
                                                    <span class="chat-box ml-0 ml-md-4"><span class="text-bold clo-ora">1</span></span>
                                                </div>
                                                
                                                <!-- CONTENT -->
                                                <div class="col col-12 col-md-10 col-cus-2 align-self-center">
                                                    <div class="row mx-sm-n2">
                                                        <div class="col-sm-12 col-md-6 px-sm-2 align-self-center">
                                                            <p class="text-bold text-uper clo-ora mb-1">Chuyên viên tư vấn tài chính</p>
                                                            <p class="text-13 mb-1 mb-md-0">Công ty TNHH K-HOME</p>
                                                        </div>
                                                        <div class="col-sm-12 col-md-6 px-sm-2 align-self-center text-md-right">
                                                            <span class="mb-1 mr-2">12:00</span>
                                                            <span class="mb-1">20/10/2019</span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>

                                    <!-- PAGINATION -->
                                    <nav aria-label="" class="page-pagination py-5 py-md-4">
                                        <ul class="pagination justify-content-center justify-content-md-end">
                                            <li class="page-item">
                                                <a class="page-link page-prev" href="javascript:;" aria-label="Previous">
                                                    <span aria-hidden="true"><i class="icofont-caret-left"></i></span>
                                                </a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">...</a></li>
                                            <li class="page-item">
                                                <a class="page-link page-next" href="javascript:;" aria-label="Next">
                                                    <span aria-hidden="true"><i class="icofont-caret-right"></i></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>

                                <div class="tab-pane px-xl-4 fade" id="tab-content-2" role="tabpanel" aria-labelledby="tab-2">
                                    <!-- PAGINATION -->
                                    <nav aria-label="" class="page-pagination py-5 py-md-4">
                                        <ul class="pagination justify-content-center justify-content-md-end">
                                            <li class="page-item">
                                                <a class="page-link page-prev" href="javascript:;" aria-label="Previous">
                                                    <span aria-hidden="true"><i class="icofont-caret-left"></i></span>
                                                </a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">...</a></li>
                                            <li class="page-item">
                                                <a class="page-link page-next" href="javascript:;" aria-label="Next">
                                                    <span aria-hidden="true"><i class="icofont-caret-right"></i></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <!-- COL RIGHT -->
                        <div class="col-sm-12 col-xl-3 px-sm-2 tab-wrap tab-right tab-type-1">
                            <!-- TAB -->
                            <ul class="row mx-sm-n2 nav nav-tabs with-cus-2" id="" role="tablist">
                                <li class="col-sm-12 px-sm-2 nav-item text-center">
                                    <a class="nav-link active" id="tab-alone" 
                                        data-toggle="tab" href="#tab-content-alone" role="tab" 
                                        aria-controls="tab-content-alone" aria-selected="true">CÂU HỎI THƯỜNG GẶP</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content with-cus-2" id="tab-content-alone">
                                <div class="tab-pane px-4 px-xl-4 with-cus fade show active" id="" role="tabpanel" aria-labelledby="">
                                    
                                    <!-- CONTENT -->
                                    <div class="item-wrap">
                                        
                                        <?php for ($x = 0; $x <= 5; $x++) { ?>
                                            <div class="item py-3 py-md-4 bdbt01-ora">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        
                                                        <a href="javascript:;" class="clo-ora">Thế giới thợ có nhận đào tạo chuyên môn về thợ nail không?</a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                        
                                    <!-- BUTTON VIEW MORE -->
                                    <div class="text-right btn-wrap">
                                        <a href="javascript:;" class="btn btn-custom ora">Xem Thêm</a>
                                    </div>
                                    
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    </div>
<?php include 'footer.php'; ?>