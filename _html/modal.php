

<!-- Modal login - forget password -->
<div class="modal fade callback custom-close " id="mdLogin" tabindex="-1" role="dialog" aria-labelledby="mdCallbackTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <!-- modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 tab-wrap">
                        <!-- TAB -->
                        <ul class="row mx-n2 nav nav-tabs" id="" role="tablist">
                            <li class="col-4 px-2 nav-item text-center">
                                <a class="nav-link active h100pr" id="tab-1" 
                                    data-toggle="tab" href="#tab-content-1" role="tab" 
                                    aria-controls="tab-content-1" aria-selected="true">ĐĂNG NHẬP</a>
                            </li>
                            <li class="col-4 px-2 nav-item text-center h100pr">
                                <a class="nav-link" id="tab-2" 
                                    data-toggle="tab" href="#tab-content-2" role="tab" 
                                    aria-controls="tab-content-2" aria-selected="true">QUÊN MẬT KHẨU</a>
                            </li>
                        </ul>

                        <!-- TAB CONTENT -->
                        <div class="tab-content with-cus with-cus-side" id="myTab">
                            <!-- TAB 1 -->
                            <div class="tab-pane px-4 px-xl-4 fade show active" id="tab-content-1" role="tabpanel" aria-labelledby="tab-1">
                                <!-- CONTENT -->
                                <div class="row">
                                    <div class="col-sm-12 pt-3 pt-md-4">
                                        <div class="form-row">
                                            <div class="col-12">
                                                <div class="mb-2 mb-md-2">
                                                    <h4 class="text-center clo-ora text-bold text-uper">
                                                        Đăng nhập
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <form>
                                            <!-- Title cv, phone number -->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-md-12 col-xl-12">
                                                    <div class="form-group">
                                                        <label for="">Username <span class="clo-ora">*</span></label>
                                                        <input class="form-control" type="text" 
                                                        placeholder="Tên đăng nhập">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-xl-12">
                                                    <div class="form-group">
                                                        <label for="">Password <span class="clo-ora">*</span></label>
                                                        <input class="form-control" type="password" 
                                                        placeholder="Password">
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Check-->
                                            <div class="form-row">
                                                <div class="col-sm-6 col-md-6 col-xl-6">
                                                    <div class="form-group">
                                                        <input type="checkbox" class="iCheckO" id="rememberMe">
                                                        <label for="rememberMe" class="ml-1 mt-1">Nhớ đăng nhập</label>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 col-md-6 col-xl-6 text-right">
                                                    <div class="form-group pt-1">
                                                        <a href="javascript:;" class="clo-ora" id="forgetMe">Quên Mật khẩu</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Submit -->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-xl-12 ">
                                                    <div class="form-group text-center">
                                                        <button type="submit" class="btn btn-custom ora text-uper mr-2 mt-2">Đăng nhập</button>
                                                        <button type="button" class="btn btn-custom blu text-uper mt-2" data-dismiss="modal">Hủy Bỏ</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane px-4 px-xl-4 fade" id="tab-content-2" role="tabpanel" aria-labelledby="tab-2">
                                <!-- CONTENT -->
                                <div class="row">
                                    <div class="col-sm-12 pt-3 pt-md-4">
                                        <div class="form-row">
                                            <div class="col-12">
                                                <div class="mb-2 mb-md-2">
                                                    <h4 class="text-center clo-ora text-bold text-uper">
                                                        QUÊN MẬT KHẨU
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <form>
                                            <!-- Title cv, phone number -->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-md-12 col-xl-12">
                                                    <div class="form-group">
                                                        <label for="">Email <span class="clo-ora">*</span></label>
                                                        <input class="form-control" type="text" 
                                                        placeholder="Email">
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Submit -->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-xl-12 ">
                                                    <div class="form-group text-center">
                                                        <button type="submit" class="btn btn-custom ora text-uper mr-2 mt-2">Gởi</button>
                                                        <button type="button" class="btn btn-custom blu text-uper mt-2" data-dismiss="modal">Hủy Bỏ</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Errpr -->
<div class="modal abc fade callback custom-close " id="mdError" tabindex="-1" role="dialog" aria-labelledby="mdCallbackTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- modal header -->
            <div class="modal-header">
                <h5 class="modal-title" id=""></h5>
            </div>

            <!-- modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 tab-wrap">
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal CV -->
<div class="modal fade callback custom-close " id="mdFacebookLog" tabindex="-1" role="dialog" aria-labelledby="mdCallbackTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-custom" role="document">
        <div class="modal-content">

            <!-- modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 tab-wrap">
                        <!-- TAB -->
                        <ul class="row mx-n2 nav nav-tabs" id="" role="tablist">
                            <li class="col-4 px-2 nav-item text-center">
                                <a class="nav-link active h100pr" id="tab-1" 
                                    data-toggle="tab" href="#tab-content-1" role="tab" 
                                    aria-controls="tab-content-1" aria-selected="true">ĐĂNG KÍ TÌM VIỆC</a>
                            </li>
                            <li class="col-4 px-2 nav-item text-center h100pr">
                                <a class="nav-link" id="tab-2" 
                                    data-toggle="tab" href="#tab-content-2" role="tab" 
                                    aria-controls="tab-content-2" aria-selected="true">ĐĂNG KÍ TUYỂN DỤNG</a>
                            </li>
                        </ul>

                        <!-- TAB CONTENT -->
                        <div class="tab-content with-cus with-cus-side" id="myTab">
                            <!-- TAB 1 -->
                            <div class="tab-pane px-4 px-xl-4 fade show active" id="tab-content-1" role="tabpanel" aria-labelledby="tab-1">
                                <!-- CONTENT -->
                                <div class="row">
                                    <div class="col-sm-12 pt-3 pt-md-4">
                                        <div class="form-row">
                                            <div class="col-12">
                                                <div class="mb-4 mb-md-4">
                                                    <h5 class="text-center clo-ora text-bold text-uper">Hãy để THEGIOITHO.COM giúp bạn có được công việc tốt nhất</h5>
                                                    <ul class="mb-0 mt-3 mt-md-3 pl-3">
                                                        <li><p class="mb-2">Nâng cao khả năng tìm việc làm</p></li>
                                                        <li><p class="mb-2">Kết nối gần hơn với nhà tuyển dụng</p></li>
                                                        <li><p class="mb-2">Chia sẻ việc làm với người thân, bạn bè</p></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <form>
                                            <!-- Title cv, phone number -->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-md-6 col-xl-6">
                                                    <div class="form-group">
                                                        <label for="">Tiêu Đề Hồ Sơ <span class="clo-ora">*</span></label>
                                                        <input class="form-control" type="text" 
                                                        placeholder="VD: Xin việc thợ vẽ móng tay tại Mini Beauty">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-xl-6">
                                                    <div class="form-group">
                                                        <label for="">Điện Thoại <span class="clo-ora">*</span></label>
                                                        <input class="form-control" type="number" 
                                                        placeholder="VD: 0909123456">
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Fullname, mail -->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-md-6 col-xl-6">
                                                    <div class="form-group">
                                                        <label for="">Họ & Tên <span class="clo-ora">*</span></label>
                                                        <input class="form-control" type="text" 
                                                        placeholder="VD: Nguyễn Văn A">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-xl-6">
                                                    <div class="form-group">
                                                        <label for="">Email <span class="clo-ora">*</span></label>
                                                        <input class="form-control" type="email" 
                                                        placeholder="VD: nguyenvana@gmail.com">
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Workplace-->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-md-12 col-xl-12">
                                                    <div class="form-row align-items-end">
                                                        <div class="col-sm-12 col-md-4 col-xl-4">
                                                            <div class="form-group">
                                                                <label for="">Nơi Muốn Làm Việc <span class="clo-ora">*</span></label>
                                                                <select class="select-2" name="">
                                                                    <option value="AL">Tỉnh / Thành</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-4 col-xl-4">
                                                            <div class="form-group">
                                                                <select class="select-2" name="">
                                                                    <option value="AL">Quận / Huyện</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-4 col-xl-4">
                                                            <div class="form-group">
                                                                <select class="select-2" name="">
                                                                    <option value="AL">Phường / Xã</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- CArrerr -->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-md-12 col-xl-12">
                                                        <div class="form-group">
                                                            <label for="">Ngành Nghề <span class="clo-ora">*</span></label>
                                                            <select class="select-2" name="">
                                                                <option value="AL">Chọn</option>
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>

                                            <!-- Check-->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-md-12 col-xl-12">
                                                    <div class="form-row align-items-end">
                                                        <div class="col-sm-12 col-md-4 col-xl-4">
                                                            <div class="form-group">
                                                                <label for="">Tần Suất Nhận Email <span class="clo-ora">*</span></label>
                                                                <div class="checkBox">
                                                                    <label for="iCheckD" class="btn btn-custom whi-line fullw text-left">Hàng ngày</label>
                                                                    <input type="radio" name="iCheck" class="iCheckC" id="iCheckD">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-4 col-xl-4">
                                                            <div class="form-group">
                                                                <div class="checkBox">
                                                                    <label for="iCheckW" class="btn btn-custom whi-line fullw text-left">Hàng tuần</label>
                                                                    <input type="radio" name="iCheck" class="iCheckC" id="iCheckW" checked>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-4 col-xl-4">
                                                            <div class="form-group">
                                                                <div class="checkBox">
                                                                    <label for="iCheckM" class="btn btn-custom whi-line fullw text-left">Hàng tháng</label>
                                                                    <input type="radio" name="iCheck" class="iCheckC" id="iCheckM">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Submit -->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-xl-12 ">
                                                    <div class="form-group text-center">
                                                        <button type="submit" class="btn btn-custom ora text-uper mr-2 mt-2">Gửi Việc</button>
                                                        <button type="button" class="btn btn-custom blu text-uper mt-2" data-dismiss="modal">Hủy Bỏ</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane px-4 px-xl-4 fade" id="tab-content-2" role="tabpanel" aria-labelledby="tab-2">
                                <!-- CONTENT -->
                                <div class="row">
                                    <div class="col-sm-12 pt-3 pt-md-4">
                                        <div class="form-row">
                                            <div class="col-12">
                                                <div class="mb-4 mb-md-4">
                                                    <h5 class="text-center clo-ora text-bold text-uper">Hãy để THEGIOITHO.COM giúp bạn có được nhân sự tốt nhất</h5>
                                                    <ul class="mb-0 mt-3 mt-md-3 pl-3">
                                                        <li><p class="mb-2">Hiệu quả (Effective): Tuyển đúng người, đúng việc</p></li>
                                                        <li><p class="mb-2">Am hiểu (Acknownledge): Từng ứng viên và doanh nghiệp trong ngành nhà máy - khu công nghiệp</p></li>
                                                        <li><p class="mb-2">Đồng hành (Together): Cùng sự phát triển của doanh nghiệp và sự nghiệp của ứng viên</p></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <form>
                                            <!-- Title cv, phone number -->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-md-6 col-xl-6">
                                                    <div class="form-group">
                                                        <label for="">Tên Đơn Vị <span class="clo-ora">*</span></label>
                                                        <input class="form-control" type="text" 
                                                        placeholder="VD: Công Ty CP & PT K-GROUP">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-xl-6">
                                                    <div class="form-group">
                                                        <label for="">Tên Người Liên Hệ <span class="clo-ora">*</span></label>
                                                        <input class="form-control" type="text" 
                                                        placeholder="VD: Nguyễn Văn A">
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <!-- Fullname, mail -->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-md-6 col-xl-6">
                                                    <div class="form-group">
                                                        <label for="">Điện Thoại <span class="clo-ora">*</span></label>
                                                        <input class="form-control" type="number" 
                                                        placeholder="VD: 0909123456">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-xl-6">
                                                    <div class="form-group">
                                                        <label for="">Email <span class="clo-ora">*</span></label>
                                                        <input class="form-control" type="email" 
                                                        placeholder="VD: nguyenvana@gmail.com">
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Workplace-->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-md-12 col-xl-12">
                                                    <div class="form-row align-items-end">
                                                        <div class="col-sm-12 col-md-4 col-xl-4">
                                                            <div class="form-group">
                                                                <label for="">Nơi Muốn Tìm Nhân Sự <span class="clo-ora">*</span></label>
                                                                <select class="select-2" name="">
                                                                    <option value="AL">Tỉnh / Thành</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-4 col-xl-4">
                                                            <div class="form-group">
                                                                <select class="select-2" name="">
                                                                    <option value="AL">Quận / Huyện</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-4 col-xl-4">
                                                            <div class="form-group">
                                                                <select class="select-2" name="">
                                                                    <option value="AL">Phường / Xã</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <!-- Submit -->
                                            <div class="form-row">
                                                <div class="col-sm-12 col-xl-12 ">
                                                    <div class="form-group text-center">
                                                        <button type="submit" class="btn btn-custom ora text-uper mr-2 mt-2">Gửi Thông Tin</button>
                                                        <button type="button" class="btn btn-custom blu text-uper mt-2" data-dismiss="modal">Hủy Bỏ</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
