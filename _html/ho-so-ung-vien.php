<?php include 'header.php'; ?>
    
<div class="">

    <!-- BLOCK -->
    <section class="container-fluid py-3 py-5" style="background: #E6E7E8">
        <div class="container">
            <div class="row ">
                <div class="col-12 col-md-4 mb-4 mb-md-0">
                    <div class="inside text-center">
                        <img class="bdrd-50p mw300" src="http://fpoimg.com/300x300" alt="avatar">
                    </div>
                </div>

                <div class="col-12 col-md-8">
                    <div class="row">
                        <div class="col-12 text-center text-md-left">
                            <h3 class="text-bold clo-bla" >Nguyễn Văn A</h3>
                            <h4 class="text-22" style="color: #F26622;">Ứng Tuyển Lĩnh Vực Nhà Hàng</h4>
                        </div>
                        <div class="col-12 mt-4">
                            <p class="lihe22">
                                <i class="ico ico-top-1 ico-info-detail-home"></i>
                                <span class="text-bold ml-2">Địa Chỉ: </span> 123 Chấn Hưng, P/6, Q. Tân Bình</p>
                            <p class="lihe22">
                                <i class="ico ico-top-1 ico-info-detail-genre"></i>
                                <span class="text-bold ml-2">Giới Tính: </span> Nam</p>
                            <p class="lihe22">
                                <i class="ico ico-top-1 ico-info-detail-intro"></i>
                                <span class="text-bold ml-2">Giới Thiệu: </span> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--  -->
    <section class="container-fluid sec-call-center py-3 py-5">
        <div class="container">
            <div class="row">
                <!-- ITEM -->
                <div class="col-12 col-md-6 col-lg-3 mb-4 mb-md-4">
                    <div class="row">
                        <div class="col-12">    
                            <div class="row align-items-center">
                                <div class="col-4 px-2 text-center text-sm-right">
                                    <span class="ico ico-acc-info ico-acc-info-1"></span>
                                </div>
                                <div class="col-8 px-2 text-center text-sm-left">
                                    <p class="mb-1 text-bold clo-whi lihe24">Kinh Nghiệm Làm Việc</p>
                                    <p class="mb-0 text-lig clo-whi">Từ 3 đến 5 năm</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-12 col-md-6 col-lg-3 mb-4 mb-md-4">
                    <div class="row">
                        <div class="col-12">    
                            <div class="row mx-n2">
                                <div class="col-4 px-2 text-center text-sm-right">
                                    <span class="ico ico-acc-info ico-acc-info-2"></span>
                                </div>
                                <div class="col-8 px-2 text-center text-sm-left">
                                    <p class="mb-1 text-bold clo-whi lihe24">Số Tuổi Hiện Tại</p>
                                    <p class="mb-0 text-lig clo-whi">25 tuổi</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-12 col-md-6 col-lg-3 mb-4 mb-md-4">
                    <div class="row">
                        <div class="col-12">    
                            <div class="row mx-n2">
                                <div class="col-4 px-2 text-center text-sm-right">
                                    <span class="ico ico-acc-info ico-acc-info-3"></span>
                                </div>
                                <div class="col-8 px-2 text-center text-sm-left">
                                    <p class="mb-1 text-bold clo-whi lihe24">Trình Độ Học Vấn</p>
                                    <p class="mb-0 text-lig clo-whi">Trung Cấp</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-12 col-md-6 col-lg-3 mb-4 mb-md-4">
                    <div class="row">
                        <div class="col-12">    
                            <div class="row mx-n2">
                                <div class="col-4 px-2 text-center text-sm-right">
                                    <span class="ico ico-acc-info ico-acc-info-4"></span>
                                </div>
                                <div class="col-8 px-2 text-center text-sm-left">
                                    <p class="mb-1 text-bold clo-whi lihe24">Vị Trí Mong Muốn</p>
                                    <p class="mb-0 text-lig clo-whi">Giám Sát / Tổ Trưởng</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-12 col-md-6 col-lg-3 mb-4 mb-md-0">
                    <div class="row">
                        <div class="col-12">    
                            <div class="row align-items-center">
                                <div class="col-4 px-2 text-center text-sm-right">
                                    <span class="ico ico-acc-info ico-acc-info-5"></span>
                                </div>
                                <div class="col-8 px-2 text-center text-sm-left">
                                    <p class="mb-1 text-bold clo-whi lihe24">Mức Lương Mong Muốn</p>
                                    <p class="mb-0 text-lig clo-whi">Thỏa thuận</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-12 col-md-6 col-lg-3 mb-4 mb-md-0">
                    <div class="row">
                        <div class="col-12">    
                            <div class="row mx-n2">
                                <div class="col-4 px-2 text-center text-sm-right">
                                    <span class="ico ico-acc-info ico-acc-info-6"></span>
                                </div>
                                <div class="col-8 px-2 text-center text-sm-left">
                                    <p class="mb-1 text-bold clo-whi lihe24">Nơi Làm Việc</p>
                                    <p class="mb-0 text-lig clo-whi">Tp.Hồ Chí Minh</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-12 col-md-6 col-lg-3 mb-4 mb-md-0">
                    <div class="row">
                        <div class="col-12">    
                            <div class="row mx-n2">
                                <div class="col-4 px-2 text-center text-sm-right">
                                    <span class="ico ico-acc-info ico-acc-info-7"></span>
                                </div>
                                <div class="col-8 px-2 text-center text-sm-left">
                                    <p class="mb-1 text-bold clo-whi lihe24">Công Việc Mong Muốn</p>
                                    <p class="mb-0 text-lig clo-whi">Bếp trưởng</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-12 col-md-6 col-lg-3 mb-4 mb-md-0">
                    <div class="row">
                        <div class="col-12">    
                            <div class="row mx-n2">
                                <div class="col-4 px-2 text-center text-sm-right">
                                    <span class="ico ico-acc-info ico-acc-info-8"></span>
                                </div>
                                <div class="col-8 px-2 text-center text-sm-left">
                                    <p class="mb-1 text-bold clo-whi lihe24">Quy Mô Công Ty</p>
                                    <p class="mb-0 text-lig clo-whi">Nhà nước</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- BLOCK RECRUITMENT INFOMATION -->
    <section class="container-fluid py-5">
        <div class="container">
            
            <!-- MAIN CONTENT -->
            <div class="row">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="row mx-sm-n2">

                        <!-- COL LEFT -->
                        <div class="col-sm-12 col-lg-9 px-sm-2 mb-5 mb-xl-0">
                            <!-- TAB -->
                            <ul class="row mx-sm-n2 nav nav-tabs" id="" role="tablist">
                                <li class="col-6 px-sm-2 nav-item text-center">
                                    <a class="nav-link h100pr active" id="tab-1" 
                                        data-toggle="tab" href="#tab-ct-1" role="tab" 
                                        aria-controls="tab-ct-1" aria-selected="true">THÔNG TIN CHI TIẾT</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content with-cus" id="">
                                <!-- TAB CONTENT 1 -->
                                <div class="tab-pane px-4 px-xl-4 fade show active" id="tab-ct-1" role="tabpanel" aria-labelledby="tab-1">
                                    <div class="item-wrap  pt-3 pt-md-4">

                                        <div class="item pb-3 pb-lg-2 mb-lg-3">
                                            <div class="row no-gutters">
                                                <div class="col">
                                                    <p class="text-bold text-uper clo-ora">KINH NGHIỆM LÀM VIỆC</p>
                                                    <p class="text-justify">- Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                                    <p class="text-justify">- Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                                    <p class="text-justify">- Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                                    <p class="text-justify">- Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="item pb-3 pb-lg-2">
                                            <div class="row no-gutters">
                                                <div class="col">
                                                    <p class="text-bold text-uper clo-ora">NGOẠI NGỮ</p>
                                                    <p class="text-justify">- Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                                    <p class="text-justify">- Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- COL RIGHT -->
                        <div class="col-sm-12 col-lg-3 px-sm-2">
                            <!-- TAB -->
                            <ul class="row mx-sm-n2 nav nav-tabs with-cus-2" id="" role="tablist">
                                <li class="col-sm-12 px-sm-2 nav-item text-center">
                                    <a class="nav-link active" id="abc-tab" 
                                        data-toggle="tab" href="#abc" role="tab" 
                                        aria-controls="abc" aria-selected="true">KỸ NĂNG KHÁC</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content with-cus-2" id="">
                                <div class="tab-pane px-4 px-xl-4 with-cus fade show active" id="" role="tabpanel" aria-labelledby="">
                                    <div class="py-4">
                                        <!-- CONTENT -->
                                        <div class="row">
                                            <div class="col-12 text-center">
                                                <div class="row align-items-center">
                                                    <div class="col-12 text-left mb-3 mb-5">
                                                        <p class="text-bold clo-ora mb-2">Kỹ Năng Giao Tiếp</p>
                                                        <!-- STAR ONLY DESK & TABLET -->    
                                                        <div class="result-rate">
                                                            <?php for ($i = 0; $i <= 4; $i++) { ?>
                                                            <span class='rt rt-empty <?php echo $i<2 ? "rt-filled" : ""; ?> s26'></span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-12 text-left mb-3 mb-5">
                                                        <p class="text-bold clo-ora mb-2">Kỹ Năng Giao Tiếp</p>
                                                        <!-- STAR ONLY DESK & TABLET -->    
                                                        <div class="result-rate">
                                                            <?php for ($i = 0; $i <= 4; $i++) { ?>
                                                            <span class='rt rt-empty <?php echo $i<2 ? "rt-filled" : ""; ?> s26'></span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-12 text-left mb-3">
                                                        <p class="text-bold clo-ora mb-2">Kỹ Năng Giao Tiếp</p>
                                                        <!-- STAR ONLY DESK & TABLET -->    
                                                        <div class="result-rate">
                                                            <?php for ($i = 0; $i <= 4; $i++) { ?>
                                                            <span class='rt rt-empty <?php echo $i<2 ? "rt-filled" : ""; ?> s26'></span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
<?php include 'footer.php'; ?>