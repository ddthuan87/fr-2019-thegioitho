<?php include 'header.php'; ?>
    
<div class="page-ab">

    <!-- BLOCK SLIDER -->
    <section class="container-fluid sec-main-slider">
        <div class="row">
            <div class="inner">
                <div class="owl-carousel owl-theme main-slider big-slider">
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </section>

    
    <!-- BLOCK FIND CAREER NEW -->
    <section class="container-fluid sec-career-new py-3 py-md-5">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-job-new w-34"></span>
                    <h3 class="headline text-bold mt-xl-3">Tạo Hồ Sơ Của Bạn</h3>
                    <p class="text-16 clo-ora">Hiện tại có <span class="clo-ora text-bold">1,235</span> công việc đang chờ bạn</p>
                </div>
            </div>
            
            <!-- CONTENT -->
            <div class="row mt-3 mt-md-4">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 mb-3 mb-md-5">
                            <div class="bder03-ora bdrd-08 px-2 px-md-3 px-lg-5">
                                    <img class="w100p" src="./assets/images/bg/c-cv-1.jpg" alt="">
                                    <div class="col text-center my-sm-4">
                                        <a href="javascript:;" class="btn btn-custom ora text-bold text-uper">hồ sơ đầy đủ</a>
                                    </div>
                                    <div class="custom-bullis pl-3 pb-3 pt-2">
                                        <p class="item">Cung cấp nhiều thông tin</p>
                                        <p class="item">Được nhà tuyển dụng quan tâm hơn</p>
                                        <p class="item">Bắt buộc đăng nhập / Đăng ký tài khoảng mới</p>
                                    </div>
                                </div>
                        </div>
                        <div class="col-sm-12 col-md-6 mb-3 mb-md-5">
                            <div class="bder03-ora bdrd-08 px-2 px-md-3 px-lg-5 h100pr">
                                <img class="w100p" src="./assets/images/bg/c-cv-2.jpg" alt="">
                                <div class="col text-center my-sm-4">
                                    <a href="javascript:;" class="btn btn-custom ora text-bold text-uper">hồ sơ đơn giản</a>
                                </div>
                                <div class="custom-bullis pl-3 pb-3 pt-2">
                                    <p class="item">Tạo hồ sơ nhanh gọn</p>
                                    <p class="item">Không cần đăng nhập</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </section>

</div>  
<?php include 'footer.php'; ?>