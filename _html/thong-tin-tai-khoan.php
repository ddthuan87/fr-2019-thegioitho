<?php include 'header.php'; ?>
    
<div class="page-ab">

    <!-- BLOCK BANNER -->
    <section class="container-fluid sec-banner">
        <div class="row">
            <div class="item-bg">
                <img class="w100p" src="./assets/images/main-banner.jpg" alt="">
            </div>
        </div>
    </section>

    <!-- BLOCK CAREER NEW -->
    <section class="container-fluid sec-career-new py-5">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-info-acc w-34"></span>
                    <h3 class="headline text-bold mt-3">Thông tin tài khoản</h3>
                </div>
            </div>
            
            <div class="row mt-2 mt-md-5">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="row">

                        <!-- COL LEFT -->
                        <div class="col-sm-12 col-lg-3">
                            <div class="pr-0 pr-lg-2" id="sidebar">
                                <div class="mb-2 mb-md-4">
                                    <div class="d-flex" id="">
                                        <a class="text-bold" href="javascript:;" data-toggle="collapse" data-target="" aria-expanded="" aria-controls="">
                                            Dashboard
                                        </a>
                                    </div>
                                </div>

                                <div class="mb-2 mb-md-4">
                                    <div class="d-flex" id="link1">
                                        <a class="text-bold mr-auto" href="javascript:;" data-toggle="collapse" data-target="#linkWrap1" 
                                        aria-expanded="true" aria-controls="linkWrap1">
                                            Nhà Tuyển Dụng
                                        </a>
                                        <i class="icofont-caret-down clo-ora text-bold text-20"></i>
                                    </div>
                                    <div id="linkWrap1" class="collapse" aria-labelledby="link1" data-parent="#sidebar">
                                        <div class="pl-0 pl-md-3 mt-2 mt-md-3 ">
                                            <ul class="nav flex-column">
                                                <li class="nav-item mb-2 mb-md-3">
                                                    <a class="text-uper text-bold clo-greyD active" href="javascript:;">
                                                        <i class="icofont-caret-right"></i> Gian Tuyển Dụng
                                                    </a>
                                                </li>
                                                <li class="nav-item mb-2 mb-md-3">
                                                    <a class="text-uper text-bold clo-greyD" href="javascript:;">
                                                        <i class="icofont-caret-right"></i> Thống Kê Giao Dịch
                                                    </a>
                                                </li>
                                                <li class="nav-item mb-2 mb-md-3">
                                                    <a class="text-uper text-bold clo-greyD" href="javascript:;">
                                                        <i class="icofont-caret-right"></i> Hồ Sơ Đã Thanh Toán
                                                    </a>
                                                </li>
                                                <li class="nav-item mb-2 mb-md-3">
                                                    <a class="text-uper text-bold clo-greyD" href="javascript:;">
                                                        <i class="icofont-caret-right"></i> Đăng Ký Xem Hồ Sơ
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-2 mb-md-4">
                                    <div class="d-flex" id="link2">
                                        <a class="text-bold mr-auto" href="javascript:;" data-toggle="collapse" data-target="#linkWrap2" 
                                        aria-expanded="false" aria-controls="linkWrap2">
                                            Ứng Viên
                                        </a>
                                        <i class="icofont-caret-down clo-ora text-bold text-20"></i>
                                    </div>
                                    <div id="linkWrap2" class="collapse" aria-labelledby="link2" data-parent="#sidebar">
                                        <div class="pl-0 pl-md-3 mt-2 mt-md-3 ">
                                            <ul class="nav flex-column">
                                                <li class="nav-item mb-2 mb-md-3">
                                                    <a class="text-uper text-bold clo-greyD active" href="javascript:;">
                                                        <i class="icofont-caret-right"></i> Gian Tuyển Dụng
                                                    </a>
                                                </li>
                                                <li class="nav-item mb-2 mb-md-3">
                                                    <a class="text-uper text-bold clo-greyD" href="javascript:;">
                                                        <i class="icofont-caret-right"></i> Thống Kê Giao Dịch
                                                    </a>
                                                </li>
                                                <li class="nav-item mb-2 mb-md-3">
                                                    <a class="text-uper text-bold clo-greyD" href="javascript:;">
                                                        <i class="icofont-caret-right"></i> Hồ Sơ Đã Thanh Toán
                                                    </a>
                                                </li>
                                                <li class="nav-item mb-2 mb-md-3">
                                                    <a class="text-uper text-bold clo-greyD" href="javascript:;">
                                                        <i class="icofont-caret-right"></i> Đăng Ký Xem Hồ Sơ
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-2 mb-md-4">
                                    <div class="d-flex" id="link3">
                                        <a class="text-bold mr-auto" href="javascript:;" data-toggle="collapse" data-target="#linkWrap3" 
                                        aria-expanded="false" aria-controls="collapseThree">
                                            Tài Khoản
                                        </a>
                                        <i class="icofont-caret-down clo-ora text-bold text-20"></i>
                                    </div>
                                    <div id="linkWrap3" class="collapse" aria-labelledby="link3" data-parent="#sidebar">
                                        <div class="pl-0 pl-md-3 mt-2 mt-md-3 ">
                                            <ul class="nav flex-column">
                                                <li class="nav-item mb-2 mb-md-3">
                                                    <a class="text-uper text-bold clo-greyD active" href="javascript:;">
                                                        <i class="icofont-caret-right"></i> Gian Tuyển Dụng
                                                    </a>
                                                </li>
                                                <li class="nav-item mb-2 mb-md-3">
                                                    <a class="text-uper text-bold clo-greyD" href="javascript:;">
                                                        <i class="icofont-caret-right"></i> Thống Kê Giao Dịch
                                                    </a>
                                                </li>
                                                <li class="nav-item mb-2 mb-md-3">
                                                    <a class="text-uper text-bold clo-greyD" href="javascript:;">
                                                        <i class="icofont-caret-right"></i> Hồ Sơ Đã Thanh Toán
                                                    </a>
                                                </li>
                                                <li class="nav-item mb-2 mb-md-3">
                                                    <a class="text-uper text-bold clo-greyD" href="javascript:;">
                                                        <i class="icofont-caret-right"></i> Đăng Ký Xem Hồ Sơ
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-2 mb-md-4">
                                    <div class="d-flex" id="">
                                        <a class="text-bold mr-auto" href="javascript:;" data-toggle="collapse" data-target="" aria-expanded="" aria-controls="">
                                            Nạp Tiền
                                        </a>
                                    </div>
                                </div>
                            </div>                            
                        </div>

                        <!-- COL RIGHT -->
                        <div class="col-sm-12 col-lg-9 bdl-lg-01-ora">
                            <div class="pl-0 pl-lg-2">
                                <div class="form-row mt-4 mt-md-5 mt-lg-0">
                                    <div class="col-12 mb-3">
                                        <h6 class="text-bold text-uper text-14">Gian Tuyển dụng</h6>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row">
                                            <p class="col-6 col-md-12 mb-1 mb-md-2">Số Tin Tuyển Dụng</p>
                                            <p class="col-6 col-md-12 text-right text-md-left clo-greyD">10</p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row">
                                            <p class="col-6 col-md-12 mb-1 mb-md-2">Số lượt xem gian</p>
                                            <p class="col-6 col-md-12 text-right text-md-left clo-greyD">10</p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row">
                                            <p class="col-6 col-md-12 mb-1 mb-md-2">Tổng Số Hồ Sơ</p>
                                            <p class="col-6 col-md-12 text-right text-md-left clo-greyD">10</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-12 text-center mt-3 mt-5">
                                        <img class="mw300 mb-2 mb-4" src="http://fpoimg.com/400x90" alt="">
                                        <h2 class="text-bold text-uper clo-bla">Công ty cổ phần k - Decor</h2>
                                    </div>
                                </div>

                                <div class="form-row mt-4 mt-md-5">
                                    <div class="col-12 col-md-4 col-lg-3 mb-2 mb-sm-0">
                                        <div class="row">
                                            <div class="col-6 col-md-12">
                                                <p class="mb-1 mb-md-2">Thời Hạn</p>
                                                <p class="clo-greyD">13/08/2019 - 13/09/2019</p>
                                            </div>
                                            <div class="col-6 col-md-12 text-right text-md-left">
                                                <a href="javascript:;" class="btn btn-custom ora text-bold text-uper mt-0 mt-md-4">
                                                    <i class="icofont-upload-alt"></i> Đăng Tin Tuyển Dụng</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-4 col-lg-3 mb-2 mb-sm-0 offset-lg-1">
                                        <div class="row">
                                            <div class="col-6 col-md-12">
                                                <p class="mb-1 mb-md-2">Cập Nhật</p>
                                                <p class="clo-greyD">13/08/2019 - 13/09/2019</p>
                                            </div>
                                            <div class="col-6 col-md-12 text-right text-md-left">
                                                <a href="javascript:;" class="btn btn-custom ora text-bold text-uper mt-0 mt-md-4">
                                                    <i class="icofont-upload-alt"></i> Sửa Gian Tuyển Dụng</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-4 col-lg-3 mb-2 mb-sm-0 offset-lg-1">
                                        <div class="row">
                                            <div class="col-6 col-md-12">
                                                <p class="mb-1 mb-md-2">Cấp Bậc Gian Hàng</p>
                                                <p class="clo-greyD">Deluxe</p>
                                            </div>
                                            <div class="col-6 col-md-12 text-right text-md-left">
                                                <a href="javascript:;" class="btn btn-custom ora text-bold text-uper mt-0 mt-md-4">
                                                <i class="icofont-upload-alt"></i> Huỷ Bỏ</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <form action="">
                                    <div class="form-row mt-4 mt-md-5">
                                        <div class="col-12 mb-3">
                                            <h6 class="text-bold text-uper text-14">Vai Trò Tên Gian</h6>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-3">
                                            <div class="form-group">
                                                <label for="">Email</label>
                                                <input class="form-control" type="email" 
                                                placeholder="VD: abc@gmail.com">
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-3 offset-lg-1">
                                            <div class="form-group">
                                                <label for="">Trạng Thái</label>
                                                <select class="select-2" name="">
                                                    <option value="AL">Chọn</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-3 offset-lg-1 text-right text-md-left">
                                            <button href="javascript:;" class="btn btn-custom h42 ora text-bold text-uper mt-0 mt-md-4">Thêm Vai Trò</button>
                                        </div>
                                    </div>
                                </form>

                                <!-- TABLE -->
                                <div class="row mt-4 mt-md-5">
                                    <!-- CONTENT -->
                                    <div class="col-sm-12">
                                        <div class="row mx-sm-n2 row-eq-height">

                                            <!-- COL LEFT -->
                                            <div class="col-12 px-sm-2 mb-5 mb-xl-0 tab-wrap tab-left tab-type-1">

                                                <!-- TAB -->
                                                <ul class="row mx-sm-n2 nav nav-tabs" id="myTab" role="tablist">
                                                    <li class="col-sm-6 px-sm-2 nav-item text-center">
                                                        <a class="nav-link active" id="n" 
                                                            data-toggle="tab" href="" role="tab" 
                                                            aria-controls="" aria-selected="true">DANH SÁCH ĐĂNG TUYỂN</a>
                                                    </li>
                                                </ul>

                                                <!-- TAB CONTENT -->
                                                <div class="tab-content with-cus" id="">
                                                    <!-- TAB 1 -->
                                                    <div class="tab-pane px-4 px-xl-4 fade show active" id="" role="tabpanel" aria-labelledby="">
                                                        <div class="item-wrap">
                                                            <!-- LOOP -->
                                                            <?php for ($x = 0; $x <= 2; $x++) { ?>
                                                            <div class="item py-4 py-lg-4">
                                                                <div class="row no-gutters">
                                                                    <!-- TIME -->
                                                                    <div class="col col-12 pl-3 pl-lg-0 order-2 order-lg-1 col-cus-1 align-self-center">
                                                                        <p class="text-13 mb-0 d-none d-lg-block">1 giờ trước</p>
                                                                    </div>
                                                                    
                                                                    <!-- CONTENT -->
                                                                    <div class="col col-12 order-1 order-lg-2 col-cus-2 align-self-center">
                                                                        <div class="row mx-sm-n2">
                                                                            
                                                                            <div class="col-12 col-md-7 col-lg-7 px-sm-2">
                                                                                <div class="row mx-sm-n2">
                                                                                    <div class="col-12 px-sm-2 mt-3 mt-sm-0 align-self-center">
                                                                                        <p class="text-bold text-uper mb-2">
                                                                                            <a href="javascript:;" class="clo-ora ">Nguyễn Thị Thu Trang</a>
                                                                                        </p>
                                                                                        <p class="text-13 mb-0">abc@gmail.com</p>
                                                                                        <p class="text-13 mb-0 mt-2 d-block d-lg-none">
                                                                                            1 giờ trước
                                                                                        </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-12 col-md-5 col-lg-5 px-sm-2 align-self-center">
                                                                                <div class="row mx-sm-n1">
                                                                                    <div class="col-12 px-sm-1 align-self-center">
                                                                                        <div class="row mx-sm-n1 align-items-center">
                                                                                            
                                                                                            <div class="col px-sm-1">
                                                                                                <p class="text-13 mb-0"><i class="clip-user mr-1 clo-ora"></i> Quản Trị Tin</p>
                                                                                            </div>
                                                                                            <div class="col-5 px-sm-1">
                                                                                                <a href="javascript:;" class="btn btn-custom ora h30 no-padH fullw">Thay Đổi</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                    
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>

                                                        <!-- PAGINATION -->
                                                        <nav aria-label="" class="page-pagination py-5 py-md-4">
                                                            <ul class="pagination justify-content-center justify-content-md-end">
                                                                <li class="page-item">
                                                                    <a class="page-link page-prev" href="javascript:;" aria-label="Previous">
                                                                        <span aria-hidden="true"><i class="icofont-caret-left"></i></span>
                                                                    </a>
                                                                </li>
                                                                <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                                                <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                                                <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                                                <li class="page-item"><a class="page-link" href="javascript:;">...</a></li>
                                                                <li class="page-item">
                                                                    <a class="page-link page-next" href="javascript:;" aria-label="Next">
                                                                        <span aria-hidden="true"><i class="icofont-caret-right"></i></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </nav>
                                                    </div>
                                                </div>
                                            
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <form action="">
                                    <div class="form-row mt-4 mt-md-5">
                                        <div class="col-12 mb-3">
                                            <h6 class="text-bold text-uper text-14">Vị Trí Tuyển Dụng</h6>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-3">
                                            <div class="form-group">
                                                <label for="">Từ Khóa</label>
                                                <input class="form-control" type="text" 
                                                placeholder="VD: Thu mua">
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-3 offset-lg-1">
                                            <div class="form-group">
                                                <label for="">Cấp Bậc</label>
                                                <select class="select-2" name="">
                                                    <option value="AL">Chọn</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-3 offset-lg-1">
                                            <div class="form-group">
                                                <label for="">Trạng Thái</label>
                                                <select class="select-2" name="">
                                                    <option value="AL">Chọn</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-12 col-lg-12 text-right text-lg-left">
                                            <button href="javascript:;" class="btn btn-custom h42 ora text-bold text-uper mt-2 mt-md-2 ml-auto d-inline-block">Tìm Kiếm</button>
                                        </div>
                                    </div>
                                </form>

                                <!-- TABLE -->
                                <div class="row mt-4 mt-md-5">
                                    <!-- CONTENT -->
                                    <div class="col-sm-12">
                                        <div class="row mx-sm-n2 row-eq-height">

                                            <!-- COL LEFT -->
                                            <div class="col-12 px-sm-2 mb-5 mb-xl-0 tab-wrap tab-left tab-type-1">

                                                <!-- TAB -->
                                                <ul class="row mx-sm-n2 nav nav-tabs" id="myTab" role="tablist">
                                                    <li class="col-sm-6 px-sm-2 nav-item text-center">
                                                        <a class="nav-link active" id="n" 
                                                            data-toggle="tab" href="" role="tab" 
                                                            aria-controls="" aria-selected="true">DANH SÁCH QUẢN TRỊ GIAN HÀNG</a>
                                                    </li>
                                                </ul>

                                                <!-- TAB CONTENT -->
                                                <div class="tab-content with-cus" id="">
                                                    <!-- TAB 1 -->
                                                    <div class="tab-pane px-4 px-xl-4 fade show active" id="" role="tabpanel" aria-labelledby="">
                                                        <div class="item-wrap">
                                                            <!-- LOOP -->
                                                            <?php for ($x = 0; $x <= 2; $x++) { ?>
                                                            <div class="item py-4 py-lg-4">
                                                                <div class="row no-gutters">
                                                                    <!-- TIME -->
                                                                    <div class="col col-12 pl-3 pl-lg-0 order-2 order-lg-1 col-cus-1 align-self-center">
                                                                        <p class="text-13 mb-0 d-none d-lg-block">1 giờ trước</p>
                                                                    </div>
                                                                    
                                                                    <!-- CONTENT -->
                                                                    <div class="col col-12 order-1 order-lg-2 col-cus-2 align-self-center">
                                                                        <div class="row mx-sm-n2">
                                                                            
                                                                            <div class="col-12 col-md-4 col-lg-4 px-sm-2">
                                                                                <div class="row mx-sm-n2">
                                                                                    <div class="col-12 px-sm-2 mt-3 mt-sm-0 align-self-center">
                                                                                        <p class="text-bold text-uper mb-2">
                                                                                            <a href="javascript:;" class="clo-ora ">Trưởng Phòng Kinh Doanh</a>
                                                                                        </p>
                                                                                        <p class="text-13 mb-0"><i class="clip-user"></i> Nguyễn Thị Thu Trang</p>
                                                                                        <p class="text-13 mb-0 mt-2 d-block d-lg-none">
                                                                                            1 giờ trước
                                                                                        </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-12 col-md-8 col-lg-8 px-sm-2 mt-2 mt-sm-0 align-self-center">
                                                                                <div class="row mx-sm-n1">
                                                                                    <div class="col-12 px-sm-1 align-self-center">
                                                                                        <div class="row mx-sm-n1 align-items-center">
                                                                                            <div class="col-6 col-sm-3 px-sm-1 mb-2 mb-md-0">
                                                                                                <p class="text-13 mb-0"><i class="ico-top-1 clip-clock mr-1 clo-ora"></i> 31/12/2019</p>
                                                                                            </div>
                                                                                            <div class="col-6 col-sm-2 px-sm-1 mb-2 mb-md-0">
                                                                                                <p class="text-13 mb-0"><i class="icofont-edit mr-1 clo-ora"></i> 2</p>
                                                                                            </div>
                                                                                            <div class="col-6 col-sm-2 px-sm-1 mb-2 mb-md-0">
                                                                                                <p class="text-13 mb-0"><i class="ico-top-1 fa-eye fa mr-1 clo-ora"></i>12</p>
                                                                                            </div>
                                                                                            <div class="col-6 col-sm-2 px-sm-1 mb-2 mb-md-0">
                                                                                                <p class="text-13 mb-0"><i class="ico-top-1 clip-file-2 mr-1 clo-ora"></i> <i class="clip-user"></i> Hiện</p>
                                                                                            </div>
                                                                                            <div class="col-12 col-sm-3 px-sm-1 mb-2 mb-md-0">
                                                                                                <a href="javascript:;" class="btn btn-custom ora h30 no-padH fullw">Thay Đổi</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                    
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>

                                                        <!-- PAGINATION -->
                                                        <nav aria-label="" class="page-pagination py-5 py-md-4">
                                                            <ul class="pagination justify-content-center justify-content-md-end">
                                                                <li class="page-item">
                                                                    <a class="page-link page-prev" href="javascript:;" aria-label="Previous">
                                                                        <span aria-hidden="true"><i class="icofont-caret-left"></i></span>
                                                                    </a>
                                                                </li>
                                                                <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                                                <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                                                <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                                                <li class="page-item"><a class="page-link" href="javascript:;">...</a></li>
                                                                <li class="page-item">
                                                                    <a class="page-link page-next" href="javascript:;" aria-label="Next">
                                                                        <span aria-hidden="true"><i class="icofont-caret-right"></i></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </nav>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    </div>
<?php include 'footer.php'; ?>