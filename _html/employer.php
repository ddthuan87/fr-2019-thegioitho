<?php include 'header.php'; ?>
    
<div class="page-ab">

        <!-- BLOCK BANNER -->
        <section class="container-fluid sec-banner">
            <div class="row">
            
                <div class="item-bg">
                    <img class="w100p" src="./assets/images/main-banner.jpg" alt="">
                </div>
            
            </div>
        </section>

        <!-- BLOCK ABOUT -->
        <section class="container-fluid about-us pt-5">
            <div class="container">
                <!-- HEADLINE -->
                <div class="row">
                    <div class="col-sm-12 text-center ">
                        <img class="mw100" src="./assets/images/brand-mina.png" alt="" class="">
                        <h3 class="headline text-upper text-bold mt-xl-4 mb-xl-3 clo-ora">CÔNG TY MINA BEAUTY NAILS</h3>
                        <p class="text-16 mb-xl-2 clo-ora text-lig">Hiện đang tuyển <span class="text-bold">10</span> vị trí</p>
                    </div>
                    <div class="col-sm-8 offset-sm-2 text-justify mt-4">
                        <p class="lihe22 text-thin">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                    </div>
                </div>
            </div>
        </section>


        <!-- BLOCK RECRUITMENT -->
        <section class="container-fluid pdy50">
            <div class="container">
                
                <!-- MAIN CONTENT -->
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-sm-12">
                        <div class="row mx-sm-n2">

                            <!-- COL LEFT -->
                            <div class="col-sm-12 col-xl-9 px-sm-2 mb-5 mb-xl-0 tab-wrap tab-left tab-type-1">
                                <!-- TAB -->
                                <ul class="row mx-sm-n2 nav nav-tabs" id="" role="tablist">
                                    <li class="col-sm-4 px-sm-2 nav-item text-center">
                                        <a class="nav-link active" id="home-tab" 
                                            data-toggle="tab" href="#home" role="tab" 
                                            aria-controls="home" aria-selected="true">VỊ TRÍ ĐANG TUYỂN</a>
                                    </li>
                                </ul>

                                <!-- TAB CONTENT -->
                                <div class="tab-content with-cus with-cus-side" id="">
                                    <!-- TAB 1 -->
                                    <div class="tab-pane px-4 px-xl-4 fade show active" id="top-recrui-content" role="tabpanel" aria-labelledby="">
                                        <div class="item-wrap">
                                            <!-- LOOP -->
                                            <?php for ($x = 0; $x <= 10; $x++) { ?>
                                            <div class="item py-4 py-lg-4">
                                                <div class="row no-gutters">
                                                    <!-- TIME -->
                                                    <div class="col col-12 pl-3 pl-lg-0 order-2 order-lg-1 col-cus-1 align-self-center">
                                                        <p class="text-13 mb-0 d-none d-lg-block">1 giờ trước</p>
                                                    </div>
                                                    
                                                    <!-- CONTENT -->
                                                    <div class="col col-12 order-1 order-lg-2 col-cus-2 align-self-center">
                                                        <div class="row mx-sm-n2">
                                                            
                                                            <div class="col-12 col-md-6 col-xl-6 px-sm-2">
                                                                <div class="row mx-sm-n2">
                                                                    <div class="col-12 col-sm-3 col-md-4 col-xl-4 px-sm-2 align-self-center text-center">
                                                                        <img class="mw70" src="./assets/images/brand-mina.png" alt="" class="">
                                                                    </div>
                                                                    <div class="col-12 col-sm-9 col-md-8 col-xl-8 px-sm-2 mt-3 mt-sm-0 align-self-center">
                                                                        <p class="text-bold text-uper mb-1">
                                                                            <a href="javascript:;" class="clo-ora ">Chuyên viên tư vấn tài chính</a>
                                                                        </p>
                                                                        <p class="text-13 mb-0">Công ty TNHH K-HOME</p>
                                                                        <p class="text-13 mb-0 mt-2 d-block d-lg-none">
                                                                            <i class="ico-top-1 clip-clock-2 mr-1 clo-ora"></i>1 giờ trước
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-12 col-md-6 col-xl-6 px-sm-2 align-self-center">
                                                                <div class="row mx-sm-n1">
                                                                    <div class="col-sm-9 col-md-8 col-xl-8 offset-sm-3 offset-md-0 px-sm-1 align-self-center">
                                                                        <div class="row mx-sm-n1">
                                                                            <div class="col-5 col-sm-5 px-sm-1 my-2 mt-md-0">
                                                                                <p class="text-13 clo-ora mb-0">$5 - 8 triệu</p>
                                                                            </div>
                                                                            <div class="col-4 col-sm-4 px-sm-1 my-2 mt-md-0">
                                                                                <p class="text-13 mb-0"><i class="ico-top-1  clip-location"></i> HCMC</p>
                                                                            </div>
                                                                            <div class="col-3 col-sm-3 px-sm-1 my-2 mt-md-0">
                                                                                <p class="text-13 mb-0"><i class="clip-user"></i> 8</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-6 col-sm-6 col-md-4 col-xl-4 offset-3 offset-md-0 px-sm-1">
                                                                        <a href="javascript:;" class="btn btn-custom ora h30 no-padH fullw">Ứng tuyển</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>

                                        <!-- PAGINATION -->
                                        <nav aria-label="" class="page-pagination py-5 py-md-4">
                                            <ul class="pagination justify-content-center justify-content-md-end">
                                                <li class="page-item">
                                                    <a class="page-link page-prev" href="javascript:;" aria-label="Previous">
                                                        <span aria-hidden="true"><i class="icofont-caret-left"></i></span>
                                                    </a>
                                                </li>
                                                <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript:;">...</a></li>
                                                <li class="page-item">
                                                    <a class="page-link page-next" href="javascript:;" aria-label="Next">
                                                        <span aria-hidden="true"><i class="icofont-caret-right"></i></span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>

                            <!-- COL RIGHT -->
                            <div class="col-sm-12 col-xl-3 px-sm-2">
                                <!-- TAB -->
                                <ul class="row mx-sm-n2 nav nav-tabs with-cus-2" id="" role="tablist">
                                    <li class="col-sm-12 px-sm-2 nav-item text-center">
                                        <a class="nav-link active" id="" 
                                            data-toggle="tab" href="javascript:;" role="tab" 
                                            aria-controls="abc" aria-selected="true">THÔNG TIN NHÀ TUYỂN DỤNG</a>
                                    </li>
                                </ul>

                                <!-- TAB CONTENT -->
                                <div class="tab-content with-cus-2" id="">
                                    <div class="tab-pane px-4 px-xl-4 with-cus fade show active" id="" role="tabpanel" aria-labelledby="">
                                        
                                        <div class="py-4">
                                        
                                            <!-- CONTENT -->
                                            <div class="row mb-4">
                                                <div class="col-xl-12 text-center">
                                                    <img class="mw100" src="./assets/images/brand-mina.png" alt="" class="">
                                                </div>
                                            </div>

                                            <div class="row mb-3">
                                                <div class="col-7">
                                                    <p class="text-bold text-uper clo-ora mb-0">Ngày thành lập</p>
                                                </div>
                                                <div class="col-5 text-right">
                                                    <p class="mb-0">20/20/1999</p>
                                                </div>
                                            </div>

                                            <div class="row mb-3">
                                                <div class="col-4">
                                                    <p class="text-bold text-uper clo-ora mb-0">Quy mô</p>
                                                </div>
                                                <div class="col-8 text-right">
                                                    <p class="mb-0">50-70 người</p>
                                                </div>
                                            </div>

                                            <div class="row  mb-3">
                                                <div class="col-sm-12">
                                                    <p class="text-bold text-uper clo-ora mb-3">Liên hệ</p>
                                                </div>
                                                <div class="col-sm-12 mb-2">
                                                    <p class="text-bold mb-2">- Tên đầy đủ</p>
                                                    <p class="">Công ty MINA BEAUTY NAILS</p>
                                                </div>
                                                <div class="col-sm-12 mb-2">
                                                    <p class="text-bold mb-2">- Địa chỉ</p>
                                                    <p class="">77 Thạch Cơ Thanh, p.An Lợi Đông, q.2, Tp.Hồ Chí Mính</p>
                                                </div>
                                                <div class="col-sm-12 mb-2">
                                                    <p class="text-bold mb-2">- Điện thoại</p>
                                                    <p class="">
                                                        <a href="tel:0123456789">012345789</a> -
                                                        <a href="tel:0123456789">012345789</a>
                                                    </p>
                                                </div>
                                                <div class="col-sm-12 mb-2">
                                                    <p class="text-bold mb-2">- Website</p>
                                                    <p class="">
                                                        <a href="javascript:">www.mybeauty.com</a>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="row  mb-3">
                                                <div class="col-sm-12">
                                                    <div class="my-map" id="map-my">
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30961.22033933558!2d108.27258429459201!3d14.06816806916266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31157a4d736a1e5f%3A0xb03bb0c9e2fe62be!2zVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1565465640318!5m2!1svi!2s" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- BLOCK COMMENT -->
        <section class="container-fluid sec-cmt pt-2">
            <div class="container">
                <!-- HEADLINE -->
                <div class="row">
                    <div class="col-sm-12 text-center ">
                        <span class="ico ico-cmt w-34"></span>
                        <h3 class="headline text-bold mt-xl-3 mb-xl-5">Bình luận và phản hồi</h3>
                    </div>
                </div>

                <!-- COMNENT -->
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-sm-12">
                        <form>
                            <div class="form-row">
                                
                                <!-- COL LEFT -->
                                <div class="col-sm-12 col-xl-9">
                                    <div class="form-group">
                                        <label for="">Nội dung <span>*</span></label>
                                        <textarea class="form-control" id="" rows="5"></textarea>
                                    </div>
                                </div>

                                <!-- COL RIGHT -->
                                <div class="col-sm-12 col-xl-3 ">
                                    <div class="form-group mb-0 pl-xl-2">
                                        <label for="">Đánh giá <span>*</span></label>
                                    </div>
                                    <div class="form-group pl-xl-2">
                                        <input type="hidden" id="rating-set" class="rating"
                                            data-filled="rt rt-filled" data-empty="rt rt-empty"
                                        />
                                    </div>
                                    <div class="form-group pl-xl-2">
                                        <button type="submit" class="btn btn-custom ora">GỬI BÌNH LUẬN</button>
                                    </div>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>


        <!-- BLOCK COMMENT LIST -->
        <section class="container-fluid sec-cmt-list pt-2">
            <!-- COMNENT COUNT -->
            <div class="container">
                
                <div class="row">
                    <div class="col-sm-12">
                        <p>Bình luận <span class="clo-ora">(1)</span></p>
                    </div>
                </div>
            </div>

            <!-- COMMENT LIST -->
            <div class="row with-bg pt-4 pb-3">
                
                <div class="container">
                <?php for ($x = 0; $x <= 5; $x++) { ?>
                    <div class="form-row">
                        <!-- COL LEFT -->
                        <div class="col-sm-12 col-xl-9 ">
                            <a class="text-bold clo-ora text-uper" href="javascript:;">User </a>
                            
                            <!-- STAR ONLY MOBILE -->
                            <span class="ml-2 d-inline d-md-none">  
                                <?php for ($i = 0; $i <= $x; $i++) { ?>
                                    <span class="rt rt-filled s16"></span>
                                <?php } ?>
                            </span>
                            <p class="text-lig mt-2">dsadsad dsad sad as đasa đâ </p>
                        </div>

                        <!-- COL RIGHT -->
                        <div class="col-sm-12 col-xl-3">
                            <!-- STAR ONLY DESK & TABLET -->    
                            <div class="result-rate pl-xl-2">
                                <?php for ($i = 0; $i <= $x; $i++) { ?>
                                <span class="rt rt-filled s16"></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </section>

    </div>
<?php include 'footer.php'; ?>