<?php include 'header.php'; ?>
    
<div class="page-ab">

    <!-- BLOCK SLIDER -->
    <section class="container-fluid sec-main-slider">
        <div class="row">
            <div class="inner">
                <div class="owl-carousel owl-theme main-slider big-slider">
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </section>

    
    <!-- BLOCK DANG TIN TUYEN DUNG -->
    <section class="container-fluid sec-apply py-4 py-md-5">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-cmt w-34"></span>
                    <h3 class="headline text-bold mt-2 mt-md-3 mb-4 mb-md-5">Đăng Tin Tuyển Dụng</h3>
                </div>
            </div>

            
            <form>
                <!-- P1. -->
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-sm-12">
                    
                        <!-- Headline -->
                        <h6 class="text-bold text-uper clo-ora mb-3">Phần 1: Thông Tin Cơ Bản</h6>
                        
                        <!-- Title CV, Quantity -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Tiêu Đề <span class="clo-ora">*</span></label>
                                    <input class="form-control" type="text" 
                                    placeholder="VD: Tuyển dụng 20 thợ vẽ móng tại Mita Beauty">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Số Lượng <span class="clo-ora">*</span></label>
                                    <input class="form-control" type="number" 
                                    placeholder="VD: 20">
                                </div>
                            </div>
                        </div>

                        <!-- Cấp bậc, lĩnh vực -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Cấp Bậc <span class="clo-ora">*</span></label>
                                    <select class="select-2" name="">
                                        <option value="AL">Chọn</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Lĩnh Vực (Tối Đa 3) <span class="clo-ora">*</span></label>
                                    <select class="select-2" name="">
                                        <option value="AL">Chọn</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- Mức lương, Thời Gian Làm Việc -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Mức Lương <span class="clo-ora">*</span></label>
                                    <select class="select-2" name="">
                                        <option value="AL">Chọn</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Thời Gian Làm Việc <span class="clo-ora">*</span></label>
                                    <select class="select-2" name="">
                                        <option value="AL">Chọn</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- Ngành Nghề, Địa Điểm -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-12 col-xl-6">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-6 col-xl-4">
                                        <div class="form-group">
                                            <label for="">Ngành Nghề <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Chọn</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-xl-8">
                                        <div class="form-group">
                                            <label for="">Ngành Nghề Chi Tiết </label>
                                            <select class="select-2" name="">
                                                <option value="AL">Chọn</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-xl-6">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-4 col-xl-4">
                                        <div class="form-group">
                                            <label for="">Địa Điểm Làm Việc <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Tỉnh / Thành</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 col-xl-4">
                                        <div class="form-group">
                                            <select class="select-2" name="">
                                                <option value="AL">Quận / Huyện</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 col-xl-4">
                                        <div class="form-group">
                                            <select class="select-2" name="">
                                                <option value="AL">Phường / Xã</option>
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Ngoại Ngữ, Hồ Sơ -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-3 col-xl-6">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-12 col-xl-12">
                                        <div class="form-group">
                                            <label for="">Yêu Cầu Ngoại Ngữ </label>
                                            <select class="select-2" name="">
                                                <option value="AL">Chọn</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-9 col-xl-6">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-4 col-xl-4">
                                        <div class="form-group">
                                            <label for="">Hạn Nộp Hồ Sơ <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Ngày</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 col-xl-4">
                                        <div class="form-group">
                                            <select class="select-2" name="">
                                                <option value="AL">Tháng</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 col-xl-4">
                                        <div class="form-group">
                                            <select class="select-2" name="">
                                                <option value="AL">Năm</option>
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Loại Công Việc -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-12 col-xl-12">
                                        <div class="form-group">
                                            <label for="">Loại Hình Công Việc </label>
                                            <select class="select-2" name="">
                                                <option value="AL">Chọn</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-4 col-xl-4">
                                        <div class="form-group">
                                            <label for="">Giới Tính <span class="clo-ora">*</span></label>
                                            <select class="select-2" name="">
                                                <option value="AL">Chọn</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- P2. -->
                <div class="row mt-3 mt-md-4 mt-lg-5">
                    <!-- CONTENT -->
                    <div class="col-sm-12">

                        <!-- Headline -->
                        <h6 class="text-bold text-uper clo-ora mb-3">Phần 2: Thông Tin Chi Tiết</h6>

                        <!--  -->
                        <div class="form-row mb-2 mb-lg-3">
                            <div class="col-sm-12 col-md-12 col-xl-12">
                                <label for="">Mô Tả Công Việc <span class="clo-ora">*</span></label>
                                <div class="form-group">
                                    <textarea name="" class="editor" cols="1" rows="1"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-xl-12">
                                <label for="">Quyền Lợi Được Hưởng <span class="clo-ora">*</span></label>
                                <div class="form-group">
                                    <textarea name="" class="editor" cols="1" rows="1"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-xl-12">
                                <label for="">Yêu Cầu Công Việc <span class="clo-ora">*</span></label>
                                <div class="form-group">
                                    <textarea name="" class="editor" cols="1" rows="1"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-xl-12">
                                <label for="">Yêu Cầu Hồ Sơ <span class="clo-ora">*</span></label>
                                <div class="form-group">
                                    <textarea name="" class="editor" cols="1" rows="1"></textarea>
                                </div>
                            </div>
                        </div>

                        <!-- Việc Cho SV -->
                        <div class="form-row">
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Việc Cho SV, Expats</label>
                                    <select class="select-2" name="">
                                        <option value="AL">Chọn</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Kinh Nghiệm Làm Việc Tại Nước Ngoài </label>
                                    <select class="select-2" name="">
                                        <option value="AL">Chọn</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- Từ Khóa -->
                        <div class="form-row mb-3 mt-3">
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Từ Khóa Tìm Kiếm</label>
                                    <select class="select-2" name="">
                                        <option value="AL">Chọn</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Người Phụ Trách <span class="clo-ora">*</span></label>
                                    <input class="form-control" type="number" 
                                    placeholder="VD: Mr. Nguyễn Văn A">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-sm-12 col-xl-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-custom ora text-uper mr-2">ĐĂNG TIN</button>
                                    <button type="submit" class="btn btn-custom blu text-uper">Hủy Bỏ</button>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </form>
        </div>
    </section>

</div>  
<?php include 'footer.php'; ?>