<?php include 'header.php'; ?>
    
<div class="page-aqa">

    <!-- BLOCK SLIDER -->
    <section class="container-fluid sec-main-slider">
        <div class="row">
            <div class="inner">
                <div class="owl-carousel owl-theme main-slider big-slider">
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK NEWS DETAIL -->
    <section class="container-fluid py-2 py-md-5">
        <div class="container">

            <div class="row">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="row">

                        <!-- COL LEFT -->
                        <div class="col-sm-12 col-lg-8 cl-xl-9 mb-5 mb-md">
                            <!-- DETAIL NEWS -->
                            <article>
                                <div class="inner">
                                    <!-- BREAD -->
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb align-items-center">
                                            <li class="breadcrumb-item"><a href="javascript:;"><span class="ico ico-about w-26 ico-top-2"></span></a></li>
                                            <li class="breadcrumb-item"><a href="javascript:;">Tin Tức</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Cho Ứng Viên</li>
                                        </ol>
                                    </nav>

                                    <h4 class="text-16 mt-4 mb-3">
                                        <a href="javascript:;" class="text-bold text-uper clo-ora mb-1 lihe20">
                                            Công ty cổ phần đầu tư TM quốc tế mặt trời đỏ (REDSUN)
                                        </a>
                                    </h4>

                                    <div class="row mt-0 mb-0 align-items-center">
                                        <div class="col text-left mr-auto">
                                            <div class="d-block d-md-inline">
                                                <i class="clip-clock ico-top-1 mr-1 text-bold"></i>
                                                <span class="clo-greyD mr-3">02/02/2019</span>
                                            </div>
                                            <div class="d-block d-md-inline mt-2 mt-md-0"> 
                                                <i class="clip-user-5 ico-top-1 mr-1 text-bold"></i>
                                                <span class="clo-greyD">Tác giả: </span>
                                            </div>
                                        </div>
                                        <!-- RATE -->
                                        <div class="col text-right">
                                            <div class="">
                                                <input type="hidden" id="rating-set" class="rating"
                                                    data-filled="rt rt-filled m3" data-empty="rt rt-empty m3"
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-justify mt-4">
                                        <p class="lihe22 text-bold">
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                        </p>
                                    </div>

                                    <figure>
                                        <img class="w100p" src="http://fpoimg.com/800x450" alt="">
                                    </figure>

                                    <div class="p-lihe22">
                                        <p>The standard Lorem Ipsum passage, used since the 1500s</p>
                                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                                        <p>Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</p>
                                        <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
                                        <p>The standard Lorem Ipsum passage, used since the 1500s</p>
                                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                                        <p>Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</p>
                                        <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
                                        
                                    </div>

                                    
                                </div>
                            </article>

                            <!-- COMNENT -->
                            <div class="row mt-5">
                                <!-- CONTENT -->
                                <div class="col-sm-12">
                                    <form>
                                        <div class="form-row">
                                            
                                            <!-- COL LEFT -->
                                            <div class="col-sm-12 col-xl-9">
                                                <div class="form-group">
                                                    <label for="">Nội dung <span>*</span></label>
                                                    <textarea class="form-control" id="" rows="5"></textarea>
                                                </div>
                                            </div>

                                            <!-- COL RIGHT -->
                                            <div class="col-sm-12 col-xl-3 ">
                                                <div class="form-group mb-0 pl-xl-2">
                                                    <label for="">Đánh giá <span>*</span></label>
                                                </div>
                                                <div class="form-group pl-xl-2">
                                                    <input type="hidden" id="rating-set" class="rating"
                                                        data-filled="rt rt-filled" data-empty="rt rt-empty"
                                                    />
                                                </div>
                                                <div class="form-group pl-xl-2">
                                                    <button type="submit" class="btn btn-custom ora">GỬI BÌNH LUẬN</button>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- COMMENT COUNT -->
                            <div class="row pt-2">
                                <div class="col-sm-12">
                                    <p>Bình luận <span class="clo-ora">(1)</span></p>
                                </div>
                            </div>

                            <!-- COMMENT LIST -->
                            <div class="sec-cmt-list">
                                <div class="col-sm-12 with-bg pt-4 pb-3">
                                    <?php for ($x = 0; $x <= 5; $x++) { ?>
                                    <div class="row">
                                        <!-- COL LEFT -->
                                        <div class="col-sm-12 col-xl-9 ">
                                            <a class="text-bold clo-ora text-uper" href="javascript:;">User </a>
                                            
                                            <!-- STAR ONLY MOBILE -->
                                            <span class="ml-2 d-inline d-md-none">  
                                                <?php for ($i = 0; $i <= $x; $i++) { ?>
                                                    <span class="rt rt-filled s16"></span>
                                                <?php } ?>
                                            </span>
                                            <p class="text-lig mt-2">dsadsad dsad sad as đasa đâ </p>
                                        </div>

                                        <!-- COL RIGHT -->
                                        <div class="col-sm-12 col-xl-3">
                                            <!-- STAR ONLY DESK & TABLET -->    
                                            <div class="result-rate pl-xl-2">
                                                <?php for ($i = 0; $i <= $x; $i++) { ?>
                                                <span class="rt rt-filled s16"></span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>   
                        </div>

                        <!-- COL RIGHT -->
                        <aside class="col-sm-12 col-lg-4 cl-xl-3">
                            <div class="tab-outer mb-3 mb-lg-3">
                                <!-- TAB -->
                                <ul class="row nav nav-tabs with-cus-2" id="" role="tablist">
                                    <li class="col-sm-12 nav-item text-center">
                                        <a class="nav-link active" id="tab-alone" 
                                            data-toggle="tab" href="#tab-content-alone" role="tab" 
                                            aria-controls="tab-content-alone" aria-selected="true">TIN XEM NHIỀU</a>
                                    </li>
                                </ul>

                                <!-- TAB CONTENT -->
                                <div class="tab-content with-cus-2" id="tab-content-alone">
                                    <div class="tab-pane px-4 px-xl-4 with-cus fade show active" id="" role="tabpanel" aria-labelledby="">
                                        
                                        <!-- CONTENT -->
                                        <div class="row pt-3 pt-md-4">
                                            <?php for ($x = 0; $x <= 3; $x++) { ?>
                                                <div class="col-12 col-md-6 col-lg-12">
                                                    <div class="item-wrap">
                                                        <div class="item pb-3 pb-lg-2 mb-lg-3">
                                                            <div class="hotspot">
                                                                <img class="w100p mb-2 mb-xl-3" src="http://fpoimg.com/600x350" alt="">
                                                                <a class="bubble" href="javascript:;"></a>
                                                            </div>
                                                            <h4 class="text-14">
                                                                <a href="javascript:;" class="text-uper text-bold lihe22">Danh sách tổng hợp việc làm nhà máy, công ty nhật - hàn tại đồng nai</a>
                                                            </h4>
                                                            <div class="text-right mt-0 mb-0">
                                                                <i class="clip-clock ico-top-1 mr-1 text-bold"></i>
                                                                <span class="clo-greyD">02/02/2019</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                            
                                        <!-- BUTTON VIEW MORE -->
                                        <div class="text-right btn-wrap">
                                            <a href="javascript:;" class="btn btn-custom ora">Xem Thêm</a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="tab-outer mb-3 mb-lg-0">
                                <!-- TAB -->
                                <ul class="row nav nav-tabs with-cus-2" id="" role="tablist">
                                    <li class="col-sm-12 nav-item text-center">
                                        <a class="nav-link active" id="tab-alone" 
                                            data-toggle="tab" href="#tab-content-alone" role="tab" 
                                            aria-controls="tab-content-alone" aria-selected="true">TIN TUYỂN DỤNG HOT</a>
                                    </li>
                                </ul>

                                <!-- TAB CONTENT -->
                                <div class="tab-content with-cus-2" id="tab-content-alone">
                                    <div class="tab-pane px-3 px-xl-3 with-cus fade show active" id="" role="tabpanel" aria-labelledby="">
                                        
                                        <!-- CONTENT -->
                                        <div class="row">
                                            <?php for ($x = 0; $x <= 5; $x++) { ?>
                                                <div class="col-12 col-md-6 col-lg-12 py-3 py-md-4">
                                                    <div class="row">
                                                        <!-- TIME -->
                                                        <div class="col-sm-12">
                                                            <h4 class="text-14">
                                                                <a href="javascript:;" class="text-bold text-uper clo-ora mb-1 lihe20">
                                                                    Công ty cổ phần đầu tư TM quốc tế mặt trời đỏ (REDSUN)
                                                                </a>
                                                            </h4>
                                                            
                                                        </div>
                                                        
                                                        <!-- CONTENT -->
                                                        <div class="col-sm-12 mt-3">
                                                            <div class="row mx-xl-n1">
                                                                <div class="col-6 col-xl-4 px-1 px-md-2 px-xl-1 text-right">
                                                                    <img class="mw130" src="./assets/images/ctyRedsun.jpg" alt="" class="">
                                                                </div>
                                                                <div class="col-6 col-xl-8 px-1 px-md-2 px-xl-1">
                                                                    <p class="mb-0">Cần tuyển</p>
                                                                    <ul class="mb-0 pl-4 pt-1 text-left">
                                                                        <li><p class="mb-1 mb-xl-0"">Công nhân</p></li>
                                                                        <li><p class="mb-1 mb-xl-0">Công nhân SX thịt</p></li>
                                                                    </ul>
                                                                </div>
                                                                
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                            
                                        <!-- BUTTON VIEW MORE -->
                                        <div class="text-right btn-wrap">
                                            <a href="javascript:;" class="btn btn-custom ora">Xem Thêm</a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK RELATE LIST -->
    <section class="container-fluid pt-2 mb-4">
        <div class="container">
            <h5 class="text-bold text-uper clo-ora">Các bài viết liên quan</h4>
            <!-- LIST -->
            <div class="row pt-2 pt-md-2">
                <?php for ($x = 0; $x <= 3; $x++) { ?>
                    <div class="col-12 col-md-6 col-xl-3">
                        <div class="item-wrap">
                            <div class="item pb-3 pb-lg-2 mb-lg-3">
                                <div class="hotspot">
                                    <img class="w100p mb-2 mb-xl-3" src="http://fpoimg.com/600x350" alt="">
                                    <a class="bubble" href="javascript:;"></a>
                                </div>
                                <h4 class="text-14">
                                    <a href="javascript:;" class="text-uper text-bold lihe22">Danh sách tổng hợp việc làm nhà máy, công ty nhật - hàn tại đồng nai</a>
                                </h4>
                                <div class="text-right mt-0 mb-0">
                                    <i class="clip-clock ico-top-1 mr-1 text-bold"></i>
                                    <span class="clo-greyD">02/02/2019</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div>
    </section>

</div>
<?php include 'footer.php'; ?>