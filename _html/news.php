<?php include 'header.php'; ?>
    
<div class="page-aqa">

    <!-- BLOCK SLIDER -->
    <section class="container-fluid sec-main-slider">
        <div class="row">
            <div class="inner">
                <div class="owl-carousel owl-theme main-slider big-slider">
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </section>


    <!-- BLOCK NEWS -->
    <section class="container-fluid sec-news py-5">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-news w-34"></span>
                    <h3 class="headline text-bold mt-2 mt-md-3 mb-4 mb-md-5">Tin Tức - Tin Nghiệp Vụ</h3>
                </div>
            </div>

            <!-- BLOCK 1 -->
            <div class="row mb-3 mb-xl-5 blk-1">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="row mx-sm-n2">

                        <!-- COL LEFT -->
                        <div class="col-12 col-lg-8 col-xl-9 px-sm-2 mb-3 mb-xl-0 blk-1-l">
                            <div class="item pr-lg-3">

                                <div class="hotspot">
                                    <img class="w100p mb-3 mb-xl-4" src="http://fpoimg.com/600x350" alt="">
                                    <a class="bubble" href="javascript:;"></a>
                                </div>
                                <h4 class="text-14">
                                    <a href="javascript:;" class="text-uper text-bold clo-ora lihe22">Danh sách tổng hợp việc làm nhà máy, công ty nhật - hàn tại đồng nai</a>
                                </h4>
                                <div class="text-right my-2 my-xl-4">
                                    <i class="clip-clock ico-top-1 mr-1 text-bold"></i>
                                    <span class="clo-greyD">02/02/2019</span>
                                </div>
                                <p class=" mb-1 mb-md-0 mt-2 lihe22">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                            </div>
                        </div>

                        <!-- COL RIGHT -->
                        <div class="col-12 col-lg-4 col-xl-3 px-sm-2 mt-0 mt-md-3 mt-lg-0 blk-1-r">
                            <div class="row">
                                <div class="col-12 col-md-4 col-lg-12">
                                    <div class="item-wrap pl-lg-3 ">
                                        <div class="item pb-3 pb-lg-2 mb-lg-3">
                                            <div class="hotspot">
                                                <img class="w100p mb-2 mb-xl-3" src="http://fpoimg.com/600x350" alt="">
                                                <a class="bubble" href="javascript:;"></a>
                                            </div>
                                            <h4 class="text-14">
                                                <a href="javascript:;" class="text-uper text-bold lihe22">Danh sách tổng hợp việc làm nhà máy, công ty nhật - hàn tại đồng nai</a>
                                            </h4>
                                            <div class="text-right mt-2 mb-1">
                                                <i class="clip-clock ico-top-1 mr-1 text-bold"></i>
                                                <span class="clo-greyD">02/02/2019</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4 col-lg-12">
                                    <div class="item-wrap pl-lg-3">
                                        <div class="item pb-3 pb-lg-2 mb-lg-3">
                                            <div class="hotspot">
                                                <img class="w100p mb-2 mb-xl-3" src="http://fpoimg.com/600x350" alt="">
                                                <a class="bubble" href="javascript:;"></a>
                                            </div>
                                            <h4 class="text-14">
                                                <a href="javascript:;" class="text-uper text-bold lihe22">Danh sách tổng hợp việc làm nhà máy, công ty nhật - hàn tại đồng nai</a>
                                            </h4>
                                            <div class="text-right mt-2 mb-1">
                                                <i class="clip-clock ico-top-1 mr-1 text-bold"></i>
                                                <span class="clo-greyD">02/02/2019</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4 col-lg-12">
                                    <div class="item-wrap pl-lg-3">
                                        <div class="item pb-3 pb-lg-2 mb-lg-3">
                                            <div class="hotspot">
                                                <img class="w100p mb-2 mb-xl-3" src="http://fpoimg.com/600x350" alt="">
                                                <a class="bubble" href="javascript:;"></a>
                                            </div>
                                            <h4 class="text-14">
                                                <a href="javascript:;" class="text-uper text-bold lihe22">Danh sách tổng hợp việc làm nhà máy, công ty nhật - hàn tại đồng nai</a>
                                            </h4>
                                            <div class="text-right mt-2 mb-1">
                                                <i class="clip-clock ico-top-1 mr-1 text-bold"></i>
                                                <span class="clo-greyD">02/02/2019</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- BLOCK 2 -->
            <div class="row blk-2">
                <!-- CONTENT -->
                <div class="col-sm-12">
                    <div class="row mx-sm-n2">

                        <!-- COL LEFT -->
                        <div class="col-sm-12 col-xl-9 px-sm-2 mb-5 mb-xl-0 tab-wrap tab-left">
                            <!-- TAB -->
                            <ul class="row mx-sm-n2 nav nav-tabs" id="" role="tablist">
                                <li class="col-sm-12 col-md-4 px-sm-2 nav-item text-center">
                                    <a class="nav-link active" id="tab-1" 
                                        data-toggle="tab" href="#tab-content-1" role="tab" 
                                        aria-controls="tab-content-1" aria-selected="true">CHO ỨNG VIÊN</a>
                                </li>
                                <li class="col-sm-12 col-md-8 px-sm-2 nav-item text-center">
                                    <a class="nav-link" id="tab-2" 
                                        data-toggle="tab" href="#tab-content-2" role="tab" 
                                        aria-controls="tab-content-2" aria-selected="true">CHO NHÀ TUYỂN DỤNG</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content with-cus with-cus-side blk-2-l" id="myTab">
                                <!-- TAB 1 -->
                                <div class="tab-pane px-4 px-xl-4 fade show active" id="tab-content-1" role="tabpanel" aria-labelledby="tab-1">

                                    <div class="item-wrap">
                                        <div class="item py-3 py-lg-4">
                                            <div class="row">
                                                <!-- ANSWER -->
                                                <div class="col col-12 col-md-5 mb-3 mb-md-0 align-self-center text-center text-md-left">
                                                    <div class="hotspot">
                                                        <img class="w100p" src="http://fpoimg.com/600x350" alt="">
                                                        <a class="bubble" href="javascript:;"></a>
                                                    </div>
                                                </div>
                                                
                                                <!-- CONTENT -->
                                                <div class="col col-12 col-md-7 ">
                                                    <div class="row mx-sm-n2">
                                                        <div class="col-sm-12 px-sm-2 ">
                                                            <h4 class="text-14">
                                                                <a href="javascript:;" class="text-uper text-bold clo-ora lihe22">Danh sách tổng hợp việc làm nhà máy, công ty nhật - hàn tại đồng nai</a>
                                                            </h4>
                                                            <p class=" mb-1 mb-md-0 mt-2 lihe22">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                        </div>
                                                        <div class="col-sm-12 px-sm-2  mt-2 text-md-right">
                                                            <i class="clip-clock ico-top-1 mr-2 text-bold"></i>
                                                            <span class="clo-greyD mr-2">09:14</span>
                                                            <span class="clo-greyD">02/02/2019</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- LOOP -->
                                        <?php for ($x = 0; $x <= 3; $x++) { ?>
                                        <div class="item py-3 py-lg-4">
                                            <div class="row">
                                                <!-- ANSWER -->
                                                <div class="col col-12 col-md-2 order-2 order-md-1 col-cus-1 mb-0 align-self-center text-left text-md-center">
                                                    <span class="clo-greyD">02/07/2019</span>
                                                </div>
                                                
                                                <!-- CONTENT -->
                                                <div class="col col-12 col-md-10 order-1 order-md-2 col-cus-2 align-self-center">
                                                    <div class="row mx-sm-n2">
                                                        <div class="col-sm-12 col-md-12 px-sm-2 align-self-center">
                                                            <h4 class="text-14">
                                                                <a class="text-bold text-uper clo-ora lihe22" href="javascript:;">Danh sách tổng hợp việc làm nhà máy, công ty nhật - hàn tại đồng nai</a>
                                                            </h4>
                                                            <p class=" mb-1 mb-md-0 mt-2 lihe22">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>

                                    <!-- PAGINATION -->
                                    <nav aria-label="" class="page-pagination py-5 py-md-4">
                                        <ul class="pagination justify-content-center justify-content-md-end">
                                            <li class="page-item">
                                                <a class="page-link page-prev" href="javascript:;" aria-label="Previous">
                                                    <span aria-hidden="true"><i class="icofont-caret-left"></i></span>
                                                </a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">...</a></li>
                                            <li class="page-item">
                                                <a class="page-link page-next" href="javascript:;" aria-label="Next">
                                                    <span aria-hidden="true"><i class="icofont-caret-right"></i></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>

                                <div class="tab-pane px-xl-4 fade" id="tab-content-2" role="tabpanel" aria-labelledby="tab-2">
                                    <!-- PAGINATION -->
                                    <nav aria-label="" class="page-pagination py-5 py-md-4">
                                        <ul class="pagination justify-content-center justify-content-md-end">
                                            <li class="page-item">
                                                <a class="page-link page-prev" href="javascript:;" aria-label="Previous">
                                                    <span aria-hidden="true"><i class="icofont-caret-left"></i></span>
                                                </a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                            <li class="page-item"><a class="page-link" href="javascript:;">...</a></li>
                                            <li class="page-item">
                                                <a class="page-link page-next" href="javascript:;" aria-label="Next">
                                                    <span aria-hidden="true"><i class="icofont-caret-right"></i></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <!-- COL RIGHT -->
                        <div class="col-sm-12 col-xl-3 px-sm-2 tab-wrap tab-right tab-type-1">
                            <!-- TAB -->
                            <ul class="row mx-sm-n2 nav nav-tabs with-cus-2" id="" role="tablist">
                                <li class="col-sm-12 px-sm-2 nav-item text-center">
                                    <a class="nav-link active" id="tab-alone" 
                                        data-toggle="tab" href="#tab-content-alone" role="tab" 
                                        aria-controls="tab-content-alone" aria-selected="true">TIN TUYỂN DỤNG HOT</a>
                                </li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content with-cus-2" id="tab-content-alone">
                                <div class="tab-pane px-4 px-xl-4 with-cus fade show active" id="" role="tabpanel" aria-labelledby="">
                                    
                                    <!-- CONTENT -->
                                    <div class="row">
                                        <?php for ($x = 0; $x <= 5; $x++) { ?>
                                            <div class="col-12 col-md-6 col-xl-12 py-3 py-md-4">
                                                <div class="row">
                                                    <!-- TIME -->
                                                    <div class="col-sm-12">
                                                        <a href="javascript:;" class="text-bold text-uper clo-ora mb-1 lihe20">
                                                            Công ty cổ phần đầu tư TM quốc tế mặt trời đỏ (REDSUN)
                                                        </a>
                                                    </div>
                                                    
                                                    <!-- CONTENT -->
                                                    <div class="col-sm-12 mt-3">
                                                        <div class="row mx-xl-n1">
                                                            <div class="col-6 col-xl-4 px-1 px-md-2 px-xl-1 text-right">
                                                                <img class="mw130" src="./assets/images/ctyRedsun.jpg" alt="" class="">
                                                            </div>
                                                            <div class="col-6 col-xl-8 px-1 px-md-2 px-xl-1">
                                                                <p class="mb-0">Cần tuyển</p>
                                                                <ul class="mb-0 pl-4 pt-1 text-left">
                                                                    <li><p class="mb-1 mb-xl-0"">Công nhân</p></li>
                                                                    <li><p class="mb-1 mb-xl-0">Công nhân SX thịt</p></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                        
                                    <!-- BUTTON VIEW MORE -->
                                    <div class="text-right btn-wrap">
                                        <a href="javascript:;" class="btn btn-custom ora">Xem Thêm</a>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
<?php include 'footer.php'; ?>