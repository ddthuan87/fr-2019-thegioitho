    </main>
    <!-- ./end MAIN CONTAINER -->

    <!-- CALL CENTER - IF HOMEPAGE NOT EXITS -->
    <section class="container-fluid sec-call-center py-4">
        <div class="container">
            
            <div class="row">
                <!-- ITEM -->
                <div class="col-12 col-md-6 col-xl-5 offset-xl-1">
                    <div class="row">
                        <div class="col-12 item-1">    
                            <div class="row align-items-center">
                                <!-- infomation -->
                                <div class="col-sm-4 text-center text-sm-right">
                                    <img class="mw100" src="./assets/images/icon/icon-support.png" alt="">
                                </div>
                                <!-- couting -->
                                <div class="col-sm-8 text-center text-sm-left">
                                    <p class="mb-1 text-16 clo-whi text-lig lihe22">Tổng Đài Tư Vấn và Hỗ Trợ Ứng Viên</p>
                                    <a href="tel:1800 1237" class="mb-0 text-bold text-30 clo-whi">1800 1237</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-12 col-md-6 col-xl-5">
                    <div class="row">
                        <div class="col-12 item-1">    
                            <div class="row align-items-center">
                                <!-- infomation -->
                                <div class="col-sm-4 text-center text-sm-right">
                                    <img class="mw100" src="./assets/images/icon/icon-phone.png" alt="">
                                </div>
                                <!-- couting -->
                                <div class="col-sm-8 text-center text-sm-left">
                                    <p class="mb-1 text-16 clo-whi text-lig lihe22">Tư Vấn Nhà Tuyển Dụng</p>
                                    <a href="tel:1800 1237" class="mb-0 text-bold text-30 clo-whi">1800 1237</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- FOOTER -->
    <footer class="navbar-footer mid-footer py-4">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="logo-footer mb-4 text-center text-lg-left">
                        <img src="./assets/images/icon/footer-logo.png" alt="">
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="row">
                        <!-- LOOP -->
                        <?php for ($x = 0; $x <= 20; $x++) { ?>
                            <!-- ITEM -->
                            <div class="col-12 col-sm-6 col-lg-3 mb-2 mb-sm-">
                                <div class="link-item text-center text-lg-left">
                                    <a style="clo-ora text-bold" href="javascript:;">Công ty cổ phần thế giới thợ</a>
                                </div>
                            </div>
                        <?php } ?>
                        
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12 text-center list-social">
                    <h3 class="headline text-bold clo-white">HEADING</h3>
                    
                    <!-- list social -->
                    <ul class="nav justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link active" href="javascript:;"><i class="icofont-facebook"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="javascript:;"><i class="icofont-twitter"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="javascript:;"><i class="icofont-skype"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="javascript:;"><i class="clip-yahoo"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="javascript:;"><i class="icofont-google-plus"></i></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="javascript:;"><i class="icofont-pinterest"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- FOOTER -->
    <footer class="navbar-footer bottom-footer">
        <div class="container">
            <p class="text-center text-bold">Copyright</p>
        </div>
    </footer>
    <!-- end: FOOTER -->
</div>
<!-- end: WRAPPER -->

<!-- call popup -->
<?php include 'modal.php'; ?>



<!-- SCRIPT -->
<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>
    <script src="assets/plugins/excanvas.min.js"></script>
<![endif]-->

<!-- Jquery -->
<script src="assets/js/jquery-3.3.1.min.js"></script>

<!-- Jquery UI -->
<script src="assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js"></script>

<!-- Bootstrap -->
<script src="assets/plugins/bootstrap-4.3.1/js/bootstrap.min.js"></script>

<!-- Select2 -->
<script src="assets/plugins/select2-4.0.6/dist/js/select2.min.js"></script>

<!-- iCheck -->
<script src="assets/plugins/icheck-1.x/icheck.min.js"></script>

<!-- Editor -->
<script src="assets/plugins/Trumbowyg-master/dist/trumbowyg.min.js"></script>

<!-- Rating -->
<script src="assets/plugins/bootstrap-rating-master/bootstrap-rating.min.js"></script>

<!-- owlCarousel -->
<script src="assets/plugins/OwlCarousel2-2.3.4/dist/owl.carousel.min.js"></script>
<!-- <script src="assets/plugins/owlcarousel/owl.linked.js"></script> -->

<!-- fancy app -->
<script src="assets/plugins/fancybox-master/dist/jquery.fancybox.min.js"></script>

<!-- Tweenmax -->
<script src="assets/js/TweenMax.min.js"></script>

<!-- Select File Custom -->
<script src="assets/js/jquery.custom-file-input.js"></script>

<!-- My JS -->
<script src="assets/js/main.js"></script>

</body>

</html>