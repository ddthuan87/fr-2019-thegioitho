<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="shortcut icon" href="assets/images/favicon.ico" />
    <title>Projects - thegioitho.me</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/plugins/bootstrap-4.3.1/css/bootstrap.min.css" type="text/css" charset="utf-8" />

    <!-- Jquery UI -->
    <link rel="stylesheet" href="assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css" type="text/css" charset="utf-8" />
    <link rel="stylesheet" href="assets/plugins/jquery-ui-1.12.1/jquery-ui.theme.min.css" type="text/css" charset="utf-8" />

    <!-- Select2 -->
    <link rel="stylesheet" href="assets/plugins/select2-4.0.6/dist/css/select2.min.css" type="text/css" charset="utf-8" />

    <!-- iCheck -->
    <link rel="stylesheet" href="assets/plugins/icheck-1.x/skins/minimal/orange.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/plugins/icheck-1.x/skins/line/orange.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/plugins/icheck-1.x/skins/square/orange.css" rel="stylesheet">

    <!-- Rating -->
    <link rel="stylesheet" href="assets/plugins/bootstrap-rating-master/bootstrap-rating.css" type="text/css" charset="utf-8" />

    <!-- OwlCarousel2-2.3.4 -->
    <link rel="stylesheet" href="assets/plugins/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css" type="text/css" charset="utf-8" />
    <link rel="stylesheet" href="assets/plugins/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css" type="text/css" charset="utf-8" />

    <!-- fancy app -->
    <link rel="stylesheet" href="assets/plugins/fancybox-master/dist/jquery.fancybox.min.css" type="text/css" charset="utf-8" />

    <!-- editor -->
    <link rel="stylesheet" href="assets/plugins/Trumbowyg-master/dist/ui/trumbowyg.min.css" type="text/css" charset="utf-8" />

    <!-- Custom Css -->
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" charset="utf-8" />

    <!-- Custom Font + Font ICO -->
    <link rel="stylesheet" href="assets/fonts/fontawesome-f5/css/all.css" type="text/css" charset="utf-8" />
    <link rel="stylesheet" href="assets/fonts/icofont.css" type="text/css" charset="utf-8" />
    <link rel="stylesheet" href="assets/fonts/clipfont.css" type="text/css" charset="utf-8" />
    <link rel="stylesheet" href="assets/fonts/fonts.css" type="text/css" charset="utf-8" />

    <!--[if IE 7]>
<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
<![endif]-->
    <!-- end: MAIN CSS -->
</head>

<body class="loading">
    <!-- <div id="fb-root"></div>
    <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1&appId=245336626238060&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script> -->


    <!-- WRAPPER -->
    <div class="wrapper">

        <!-- TOP HEADER -->
        <header class="navbar-header top-header">
            <nav class="navbar navbar-expand">
                <div class="container">
                    <!-- MENU -->
                    <ul class="navbar-nav ml-auto flex">
                        <li class="nav-item">
                            <a class="nav-link" href="tel:+1800 1237"><i class="clip-ico clip-phone"></i> 1800 1237</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pr-1" href="javascript:;"><i class="clip-ico clip-earth"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link px-1" href="javascript:;"><img class="w30" src="./assets/images/icon/ico-viet.png" ></a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link px-1" href="javascript:;"><img class="w30" src="./assets/images/icon/ico-eng.png" ></a>
                        </li>
                    </ul>
                    
                </div>
            </nav> 
        </header>

        <!-- HEADER CHUA LOGIN -->
        <header class="navbar-header py-lg-3 d-none">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <!-- LOGO -->
                    <a class="navbar-brand" href="#">
                        <img src="./assets/images/logo.png" >
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <!-- MENU -->
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item mr-lg-4 active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item mr-lg-4">
                                <a class="nav-link" href="#">Tạo Hồ Sơ</a>
                            </li>
                            <li class="nav-item mr-lg-4">
                                <a class="nav-link" href="#">Hỏi Đáp</a>
                            </li>
                            <li class="nav-item mr-lg-4">
                                <a class="nav-link" href="#">Dịch Vụ</a>
                            </li>
                            <li class="nav-item mr-lg-4">
                                <a class="nav-link" href="#">Tin Nghiệp Vụ</a>
                            </li>
                        </ul>

                        <form class="form-inline my-2 my-lg-0 justify-content-center">
                            <button class="btn btn-custom ora mr-2" type="button">Đăng Tuyển</button>
                            <button class="btn btn-custom blu" type="button" data-toggle="modal" data-target="#mdLogin">Đăng Nhập</button>
                        </form>
                    </div>
                </div>
            </nav>
        </header>
        <!-- end: HEADER -->

        <!-- HEADER DA LOGIN -->
        <header class="navbar-header py-lg-3">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <!-- LOGO -->
                    <a class="navbar-brand" href="#">
                        <img src="./assets/images/logo.png" >
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <!-- MENU -->
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item mr-lg-4 active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item mr-lg-4">
                                <a class="nav-link" href="#">Tạo Hồ Sơ</a>
                            </li>
                            <li class="nav-item mr-lg-4">
                                <a class="nav-link" href="#">Hỏi Đáp</a>
                            </li>
                            <li class="nav-item mr-lg-4">
                                <a class="nav-link" href="#">Dịch Vụ</a>
                            </li>
                            <li class="nav-item mr-lg-4">
                                <a class="nav-link" href="#">Tin Nghiệp Vụ</a>
                            </li>
                        </ul>

                        <form class="form-inline my-2 my-lg-0 justify-content-center">
                            <button class="btn btn-custom ora mr-2" type="button">Đăng Tuyển</button>
                            
                            <div class="navbar navbar-expand px-0 py-0">
                            <ul class="navbar-nav ml-auto flex mt-0">
                                <li class="nav-item ml-0 ml-lg-4">
                                    <!-- <a class="nav-link" href="tel:+1800 1237"><i class="clip-ico clip-phone"></i> 1800 1237</a> -->
                                    <a href="javascript:;" class=" text-bold clo-ora">USER</a>
                                </li>
                                <li class="nav-item ml-2">
                                    <span class="">
                                        (2000 
                                        <img class="w15 ico-bottom-1" src="./assets/images/icon/ico-dollar.png" >
                                        , 3 
                                        <img class="w15 ico-bottom-2" src="./assets/images/icon/ico-star.png" >
                                        )
                                    </span>
                                </li>
                                <li class="nav-item ml-2">
                                    <a class="position-relative" href="javascript:;">
                                        <span class="dot-noti"></span>
                                        <img class="w20" src="./assets/images/icon/ico-bell.png" >
                                    </a>
                                </li>
                                <li class="nav-item ml-2">
                                    <a class="" href="javascript:;">
                                        <img class="w20" src="./assets/images/icon/ico-settings.png" >
                                    </a>
                                </li>
                            </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </nav>
        </header>
        <!-- end: HEADER -->

        <!-- MAIN CONTAINER -->
        <main class="main-container">