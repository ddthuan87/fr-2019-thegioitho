<?php include 'header.php'; ?>
    
<div class="page-ab">

        <!-- BLOCK BANNER -->
        <section class="container-fluid sec-banner">
            <div class="row">
            
                <div class="item-bg">
                    <img class="w100p" src="./assets/images/main-banner.jpg" alt="">
                </div>
            
            </div>
        </section>

        <!-- BLOCK ABOUT -->
        <section class="container-fluid about-us pt-5">
            <div class="container">
                <!-- HEADLINE -->
                <div class="row">
                    <div class="col-sm-12 text-center ">
                        <img class="mw100" src="./assets/images/brand-mina.png" alt="" class="">
                        <h3 class="headline text-upper text-bold clo-ora mt-4">Tuyển Nhân Viên Làm Nail</h3>
                    </div>
                </div>
            </div>
        </section>

        <!-- BLOCK RECRUITMENT INFOMATION -->
        <section class="container-fluid py-3 py-lg-5">
            <div class="container">
                
                <!-- MAIN CONTENT -->
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-sm-12">
                        <div class="row mx-sm-n2">

                            <!-- COL LEFT -->
                            <div class="col-sm-12 col-lg-9 px-sm-2 mb-5 mb-xl-0">
                                <!-- TAB -->
                                <ul class="row mx-sm-n2 nav nav-tabs" id="" role="tablist">
                                    <li class="col-4 px-sm-2 nav-item text-center">
                                        <a class="nav-link h100pr active" id="tab-1" 
                                            data-toggle="tab" href="#tab-ct-1" role="tab" 
                                            aria-controls="tab-ct-1" aria-selected="true">MÔ TẢ CÔNG VIỆC</a>
                                    </li>
                                    <li class="col-4 px-sm-2 nav-item text-center">
                                        <a class="nav-link h100pr" id="tab-2" 
                                            data-toggle="tab" href="#tab-ct-2" role="tab" 
                                            aria-controls="tab-ct-2" aria-selected="true">THÔNG TIN NHÀ TUYỂN DỤNG</a>
                                    </li>
                                    <li class="col-4 px-sm-2 nav-item text-center">
                                        <a class="nav-link h100pr" id="tab-3" 
                                            data-toggle="tab" href="#tab-ct-3" role="tab" 
                                            aria-controls="tab-ct-3" aria-selected="true">TUYỂN DỤNG VỊ TRÍ KHÁC</a>
                                    </li>
                                </ul>

                                <!-- TAB CONTENT -->
                                <div class="tab-content with-cus" id="">
                                    <!-- TAB CONTENT 1 -->
                                    <div class="tab-pane px-4 px-xl-4 fade show active" id="tab-ct-1" role="tabpanel" aria-labelledby="tab-1">
                                        <div class="item-wrap  pt-3 pt-md-4">

                                            <div class="item pb-3 pb-lg-2 mb-lg-3">
                                                <div class="row no-gutters">
                                                    <div class="col">
                                                        <p class="text-bold text-uper clo-ora">Chuyên viên tư vấn tài chính</p>
                                                        <p class="text-justify">- Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                                                        <p class="text-justify">- Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="item pb-3 pb-lg-2 mb-lg-3">
                                                <div class="row no-gutters">
                                                    <div class="col">
                                                        <p class="text-bold text-uper clo-ora">Quyền lợi được hưởng</p>
                                                        <p class="text-justify">- Lorem Ipsum is simply dummy text of the </p>
                                                        <p class="text-justify">- Lorem Ipsum is simply dummy text of the </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="item pb-3 pb-lg-2 mb-lg-3">
                                                <div class="row no-gutters">
                                                    <div class="col">
                                                        <p class="text-bold text-uper clo-ora">Yêu cầu công việc</p>
                                                        <p class="text-justify">- Lorem Ipsum is simply dummy text of the </p>
                                                        <p class="text-justify">- Lorem Ipsum is simply dummy text of the </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="item">
                                                <div class="row">
                                                    <div class="col-12 col-md-7 col-lg-8">
                                                        <p class="text-bold text-uper clo-ora">Yêu cầu công việc</p>
                                                        <p class="text-justify">- Lorem Ipsum is simply dummy text of the </p>
                                                        <p class="text-justify">- Lorem Ipsum is simply dummy text of the </p>
                                                    </div>
                                                    <div class="col-12 col-md-5 col-lg-4 align-self-end">
                                                        <form class="form-inline mb-3 justify-content-center justify-content-md-end text-md-right">
                                                            <button class="btn btn-custom ora mr-2" type="button">Lưu việc làm</button>
                                                            <button class="btn btn-custom blu" type="button">Chia sẻ</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- TAB CONTENT 2 -->
                                    <div class="tab-pane px-xl-4 fade" id="tab-ct-2" role="tabpanel" aria-labelledby="tab-2">
                                        
                                    </div>

                                    <!-- TAB CONTENT 2 -->
                                    <div class="tab-pane px-xl-4 fade" id="tab-ct-3" role="tabpanel" aria-labelledby="tab-3">
                                        
                                    </div>
                                </div>
                            </div>

                            <!-- COL RIGHT -->
                            <div class="col-sm-12 col-lg-3 px-sm-2">
                                <!-- TAB -->
                                <ul class="row mx-sm-n2 nav nav-tabs with-cus-2" id="" role="tablist">
                                    <li class="col-sm-12 px-sm-2 nav-item text-center">
                                        <a class="nav-link active" id="abc-tab" 
                                            data-toggle="tab" href="#abc" role="tab" 
                                            aria-controls="abc" aria-selected="true">THÔNG TIN CƠ BẢN</a>
                                    </li>
                                </ul>

                                <!-- TAB CONTENT -->
                                <div class="tab-content with-cus-2" id="">
                                    <div class="tab-pane px-4 px-xl-4 with-cus fade show active" id="" role="tabpanel" aria-labelledby="">
                                        
                                        <div class="py-4">
                                        
                                            <!-- CONTENT -->
                                            <div class="row">
                                                <div class="col-12 col-sm-6 col-md-3 col-lg-12 text-center">
                                                    <div class="row mx-sm-n2 mb-sm-4 align-items-center">
                                                        <div class="col-5 px-sm-2 text-right">
                                                            <span class="ico w-60 ico-info-date"></span>
                                                        </div>
                                                        <div class="col-7 px-sm-2 text-left">
                                                            <p class="mb-1">Hạn nộp</p>
                                                            <p class="mb-2 clo-ora text-bold">20/20/1999</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-3 col-lg-12 text-center">
                                                    <div class="row mx-sm-n2 mb-sm-4 align-items-center">
                                                        <div class="col-5 px-sm-2 text-right">
                                                            <span class="ico w-60 ico-info-money"></span>
                                                        </div>
                                                        <div class="col-7 px-sm-2 text-left">
                                                            <p class="mb-1">Hạn nộp</p>
                                                            <p class="mb-2 clo-ora text-bold">20/20/1999</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-3 col-lg-12 text-center">
                                                    <div class="row mx-sm-n2 mb-sm-4 align-items-center">
                                                        <div class="col-5 px-sm-2 text-right">
                                                            <span class="ico w-60 ico-info-quantity"></span>
                                                        </div>
                                                        <div class="col-7 px-sm-2 text-left">
                                                            <p class="mb-1">Hạn nộp</p>
                                                            <p class="mb-2 clo-ora text-bold">20/20/1999</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-3 col-lg-12 text-center">
                                                    <div class="row mx-sm-n2 mb-sm-4 align-items-center">
                                                        <div class="col-5 px-sm-2 text-right">
                                                            <span class="ico w-60 ico-info-time"></span>
                                                        </div>
                                                        <div class="col-7 px-sm-2 text-left">
                                                            <p class="mb-1">Hạn nộp</p>
                                                            <p class="mb-2 clo-ora text-bold">20/20/1999</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-3 col-lg-12 text-center">
                                                    <div class="row mx-sm-n2 mb-sm-4 align-items-center">
                                                        <div class="col-5 px-sm-2 text-right">
                                                            <span class="ico w-60 ico-info-locate"></span>
                                                        </div>
                                                        <div class="col-7 px-sm-2 text-left">
                                                            <p class="mb-1">Hạn nộp</p>
                                                            <p class="mb-2 clo-ora text-bold">20/20/1999</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-3 col-lg-12 text-center">
                                                    <div class="row mx-sm-n2 mb-sm-4 align-items-center">
                                                        <div class="col-5 px-sm-2 text-right">
                                                            <span class="ico w-60 ico-info-type"></span>
                                                        </div>
                                                        <div class="col-7 px-sm-2 text-left">
                                                            <p class="mb-1">Hạn nộp</p>
                                                            <p class="mb-2 clo-ora text-bold">20/20/1999</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-3 col-lg-12 text-center">
                                                    <div class="row mx-sm-n2 mb-sm-4 align-items-center">
                                                        <div class="col-5 px-sm-2 text-right">
                                                            <span class="ico w-60 ico-info-job"></span>
                                                        </div>
                                                        <div class="col-7 px-sm-2 text-left">
                                                            <p class="mb-1">Hạn nộp</p>
                                                            <p class="mb-2 clo-ora text-bold">20/20/1999</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-3 col-lg-12 text-center">
                                                    <div class="row mx-sm-n2 mb-sm-0 align-items-center">
                                                        <div class="col-5 px-sm-2 text-right">
                                                            <span class="ico w-60 ico-info-vitri"></span>
                                                        </div>
                                                        <div class="col-7 px-sm-2 text-left">
                                                            <p class="mb-1">Hạn nộp</p>
                                                            <p class="mb-4 clo-ora text-bold">20/20/1999</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- BLOCK APPLY CV -->
        <section class="container-fluid sec-apply py-4 py-md-5">
            <div class="container">
                <!-- HEADLINE -->
                <div class="row">
                    <div class="col-sm-12 text-center ">
                        <span class="ico ico-cmt w-34"></span>
                        <h3 class="headline text-bold mt-2 mt-md-3 mb-4 mb-md-5">Nộp Hồ Sơ Ứng Tuyển</h3>
                    </div>
                </div>

                <!-- CONTENT -->
                <div class="row">
                    
                    <div class="col-sm-12">
                        <form>
                            <!-- Title cv, phone number -->
                            <div class="form-row">
                                <div class="col-sm-12 col-md-6 col-xl-6">
                                    <div class="form-group">
                                        <label for="">Tiêu Đề Hồ Sơ <span class="clo-ora">*</span></label>
                                        <input class="form-control" type="text" 
                                        placeholder="VD: Xin việc thợ vẽ móng tay tại Mini Beauty">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-xl-6">
                                    <div class="form-group">
                                        <label for="">Điện Thoại <span class="clo-ora">*</span></label>
                                        <input class="form-control" type="number" 
                                        placeholder="VD: 0909123456">
                                    </div>
                                </div>
                            </div>

                            <!-- Fullname, mail -->
                            <div class="form-row">
                                <div class="col-sm-12 col-md-6 col-xl-6">
                                    <div class="form-group">
                                        <label for="">Họ & Tên <span class="clo-ora">*</span></label>
                                        <input class="form-control" type="text" 
                                        placeholder="VD: Nguyễn Văn A">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-xl-6">
                                    <div class="form-group">
                                        <label for="">Email <span class="clo-ora">*</span></label>
                                        <input class="form-control" type="email" 
                                        placeholder="VD: nguyenvana@gmail.com">
                                    </div>
                                </div>
                            </div>

                            <!-- Date of birth, Address -->
                            <div class="form-row">
                                <div class="col-sm-12 col-md-12 col-xl-6">
                                    <div class="form-row align-items-end">
                                        <div class="col-sm-12 col-md-4 col-xl-4">
                                            <div class="form-group">
                                                <label for="">Ngày, Tháng, Năm Sinh <span class="clo-ora">*</span></label>
                                                <select class="select-2" name="">
                                                    <option value="AL">Ngày sinh</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-4 col-xl-4">
                                            <div class="form-group">
                                                <select class="select-2" name="">
                                                    <option value="AL">Tháng sinh</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-4 col-xl-4">
                                            <div class="form-group">
                                                <select class="select-2" name="">
                                                    <option value="AL">Năm sinh</option>
                                                </select>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12 col-xl-6">
                                    <div class="form-row align-items-end">
                                        <div class="col-sm-12 col-md-8 col-xl-8">
                                            <div class="form-group">
                                                <label for="">Địa Chỉ Nơi Ở Hiện Tại <span class="clo-ora">*</span></label>
                                                <input class="form-control" type="text" 
                                                placeholder="VD: 77 Nguyễn Cơ Thạch, An Lợi Đông, Q.2">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-4 col-xl-4">
                                            <div class="form-group">
                                                <select class="select-2" name="">
                                                    <option value="AL">Tỉnh / Thành</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Introduce about ur self -->
                            <div class="form-row">
                                <div class="col-sm-12 col-md-12 col-xl-12">
                                    <div class="form-group">
                                        <label for="">Giới Thiệu Bản Thân <span class="clo-ora">*</span></label>
                                        <textarea class="form-control" id="" rows="5"
                                        placeholder="Mô tả về bản thân, nếu các năng lực, các kĩ năng, lợi thế và các điểm còn hạn chế (nếu có) của bạn"></textarea>
                                    </div>
                                </div>
                            </div>

                            <!-- Upload -->
                            <div class="form-row mb-3">
                                <div class="col-sm-12 col-xl-6">
                                    <div class="form-group">
                                        <label for="">Hồ Sơ Của Bạn <span class="clo-ora">*</span></label>
                                        <div class="custom-file-my">
                                            <input type="file" 
                                                name="file-1[]" 
                                                id="file-1" 
                                                class="inputfile" 
                                                data-multiple-caption='<i class="icofont-check"></i> Đã chọn {count} files' multiple="">
                                            <label for="file-1">
                                                <span class="text-bold text-uper"><i class="icofont-upload-alt"></i> Tải lên hồ sơ / Ảnh</span>
                                            </label>
                                        </div>
                                        <small id="fileHelp" class="form-text text-muted">Tải lên hồ sơ hoặc ít nhất 1 hình ảnh của bạn (Tải hồ sơ mẫu). <br />
                                            Chỉ chấp thuận tập tin có định dạng .doc, .docx, .xls, .xlsx, .pdf, .gif, .png, .jpg <br />
                                            Dung lượng file không quá 3MB
                                        </small>
                                        <p class="text-12 text-bold clo-ora mt-2">
                                            Hãy luôn là ứng viên chuyên nghiệp. Nghiêm túc đến phỏng vấn theo lịch hẹn bạn nhé. Cảm ơn bạn!
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <!-- Submit -->
                            <div class="form-row">
                                <div class="col-sm-12 col-xl-12 ">
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-custom ora text-uper mr-2">Nộp Hồ Sơ</button>
                                        <button type="submit" class="btn btn-custom blu text-uper">Hủy Bỏ</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <!-- BLOCK COMMENT -->
        <section class="container-fluid sec-cmt py-2 py-md-3 py-lg-5">
            <div class="container">
                <!-- HEADLINE -->
                <div class="row">
                    <div class="col-sm-12 text-center ">
                        <span class="ico ico-cmt w-34"></span>
                        <h3 class="headline text-bold mt-2 mt-md-3 mb-4 mb-md-5">Bình Luận Và Phản Hồi</h3>
                    </div>
                </div>

                <!-- COMNENT -->
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-sm-12">
                        <form>
                            <div class="form-row">
                                
                                <!-- COL LEFT -->
                                <div class="col-sm-12 col-xl-9">
                                    <div class="form-group">
                                        <label for="">Nội dung <span>*</span></label>
                                        <textarea class="form-control" id="" rows="5"></textarea>
                                    </div>
                                </div>

                                <!-- COL RIGHT -->
                                <div class="col-sm-12 col-xl-3 ">
                                    <div class="form-group mb-0 pl-xl-2">
                                        <label for="">Đánh giá <span>*</span></label>
                                    </div>
                                    <div class="form-group pl-xl-2">
                                        <input type="hidden" id="rating-set" class="rating"
                                            data-filled="rt rt-filled" data-empty="rt rt-empty"
                                        />
                                    </div>
                                    <div class="form-group pl-xl-2">
                                        <button type="submit" class="btn btn-custom ora">GỬI BÌNH LUẬN</button>
                                    </div>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <!-- BLOCK COMMENT LIST -->
        <section class="container-fluid sec-cmt-list pt-2">
            <!-- COMNENT COUNT -->
            <div class="container">
                
                <div class="row">
                    <div class="col-sm-12">
                        <p>Bình luận <span class="clo-ora">(1)</span></p>
                    </div>
                </div>
            </div>

            <!-- COMMENT LIST -->
            <div class="row with-bg pt-4 pb-3">
                
                <div class="container">
                <?php for ($x = 0; $x <= 2; $x++) { ?>
                    <div class="form-row mb-sm-3 mb-md-0">
                        <!-- COL LEFT -->
                        <div class="col-sm-12 col-md-8 col-xl-9 ">
                            <a class="text-bold clo-ora text-uper" href="javascript:;">
                                User <?php echo $x; ?>
                            </a>

                            <!-- STAR ONLY MOBILE -->
                            <span class="ml-2 d-inline d-md-none">
                                <?php for ($i = 0; $i <= $x; $i++) { ?>
                                    <span class="rt rt-filled s16"></span>
                                <?php } ?>
                            </span>
                            <p class="text-lig mt-2 mb-sm-2 mb-md-3">dsadsad dsad sad as đasa đâ </p>
                        </div>

                        <!-- COL RIGHT -->
                        <div class="col-sm-12 col-md-4 col-xl-3 d-none d-md-block">
                            <!-- STAR ONLY DESK & TABLET -->    
                            <div class="result-rate pl-xl-2">
                                <?php for ($i = 0; $i <= $x; $i++) { ?>
                                <span class="rt rt-filled s16"></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </section>

        <!-- BLOCK CAREER RELATE -->
        <section class="container-fluid sec-career-new py-5">
            <div class="container">
                <!-- HEADLINE -->
                <div class="row">
                    <div class="col-sm-12 text-center ">
                        <span class="ico ico-job-new w-34"></span>
                        <h3 class="headline text-bold mt-3">Việc Làm Liên Quan</h3>
                        <p class="text-16 clo-ora mt-2 mt-md-3 mb-4 mb-md-5">Làm việc tại: <span class="clo-ora text-bold">HCM</span>, Mức lương <span class="clo-ora text-bold">Thỏa thuận</span></p>
                    </div>
                </div>
                
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-sm-12">
                        <div class="row mx-sm-n2 row-eq-height">

                            <!-- COL LEFT -->
                            <div class="col-sm-12 col-xl-9 px-sm-2 mb-5 mb-xl-0 tab-wrap tab-left tab-type-1">

                                <!-- TAB -->
                                <ul class="row mx-sm-n2 nav nav-tabs" id="myTab" role="tablist">
                                    <li class="col-sm-6 px-sm-2 nav-item text-center">
                                        <a class="nav-link active" id="new-recruitment" 
                                            data-toggle="tab" href="#recruitment" role="tab" 
                                            aria-controls="recruitment" aria-selected="true">VIỆC LÀM LIÊN QUAN</a>
                                    </li>
                                    <li class="col-sm-6 px-sm-2 nav-item text-center">
                                        <a class="nav-link" id="coop-foreign" 
                                            data-toggle="tab" href="#foreign" role="tab" 
                                            aria-controls="foreign" aria-selected="false">CÔNG TY LIÊN DOANH / NƯỚC NGOÀI</a>
                                    </li>
                                </ul>

                                <!-- TAB CONTENT -->
                                <div class="tab-content with-cus" id="myTabContent">
                                    <!-- TAB 1 -->
                                    <div class="tab-pane px-4 px-xl-4 fade show active" id="recruitment" role="tabpanel" aria-labelledby="new-recruitment">
                                        <div class="item-wrap">
                                            <!-- LOOP -->
                                            <?php for ($x = 0; $x <= 10; $x++) { ?>
                                            <div class="item py-4 py-lg-4">
                                                <div class="row no-gutters">
                                                    <!-- TIME -->
                                                    <div class="col col-12 pl-3 pl-lg-0 order-2 order-lg-1 col-cus-1 align-self-center">
                                                        <p class="text-13 mb-0 d-none d-lg-block">1 giờ trước</p>
                                                    </div>
                                                    
                                                    <!-- CONTENT -->
                                                    <div class="col col-12 order-1 order-lg-2 col-cus-2 align-self-center">
                                                        <div class="row mx-sm-n2">
                                                            
                                                            <div class="col-12 col-md-6 col-xl-6 px-sm-2">
                                                                <div class="row mx-sm-n2">
                                                                    <div class="col-12 col-sm-3 col-md-4 col-xl-4 px-sm-2 align-self-center text-center">
                                                                        <img class="mw100r" src="./assets/images/k-home.png" alt="" class="">
                                                                    </div>
                                                                    <div class="col-12 col-sm-9 col-md-8 col-xl-8 px-sm-2 mt-3 mt-sm-0 align-self-center">
                                                                        <p class="text-bold text-uper mb-1">
                                                                            <a href="javascript:;" class="clo-ora ">Chuyên viên tư vấn tài chính</a>
                                                                        </p>
                                                                        <p class="text-13 mb-0">Công ty TNHH K-HOME</p>
                                                                        <p class="text-13 mb-0 mt-2 d-block d-lg-none">
                                                                            <i class="ico-top-1 clip-clock-2 mr-1 clo-ora"></i>1 giờ trước
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-12 col-md-6 col-xl-6 px-sm-2 align-self-center">
                                                                <div class="row mx-sm-n1">
                                                                    <div class="col-sm-9 col-md-8 col-xl-8 offset-sm-3 offset-md-0 px-sm-1 align-self-center">
                                                                        <div class="row mx-sm-n1">
                                                                            <div class="col-5 col-sm-5 px-sm-1 my-2 mt-md-0">
                                                                                <p class="text-13 clo-ora mb-0">$5 - 8 triệu</p>
                                                                            </div>
                                                                            <div class="col-4 col-sm-4 px-sm-1 my-2 mt-md-0">
                                                                                <p class="text-13 mb-0"><i class="ico-top-1  clip-location"></i> HCMC</p>
                                                                            </div>
                                                                            <div class="col-3 col-sm-3 px-sm-1 my-2 mt-md-0">
                                                                                <p class="text-13 mb-0"><i class="clip-user"></i> 8</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-6 col-sm-6 col-md-4 col-xl-4 offset-3 offset-md-0 px-sm-1">
                                                                        <a href="javascript:;" class="btn btn-custom ora h30 no-padH fullw">Ứng tuyển</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>

                                        <!-- PAGINATION -->
                                        <nav aria-label="" class="page-pagination py-5 py-md-4">
                                            <ul class="pagination justify-content-center justify-content-md-end">
                                                <li class="page-item">
                                                    <a class="page-link page-prev" href="javascript:;" aria-label="Previous">
                                                        <span aria-hidden="true"><i class="icofont-caret-left"></i></span>
                                                    </a>
                                                </li>
                                                <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript:;">...</a></li>
                                                <li class="page-item">
                                                    <a class="page-link page-next" href="javascript:;" aria-label="Next">
                                                        <span aria-hidden="true"><i class="icofont-caret-right"></i></span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>

                                    <!-- TAB 2 -->
                                    <div class="tab-pane px-xl-4 fade" id="foreign" role="tabpanel" aria-labelledby="coop-foreign">
                                        
                                        <!-- PAGINATION -->
                                        <nav aria-label="" class="page-pagination py-5 py-md-4">
                                            <ul class="pagination justify-content-center justify-content-md-end">
                                                <li class="page-item">
                                                    <a class="page-link page-prev" href="javascript:;" aria-label="Previous">
                                                        <span aria-hidden="true"><i class="icofont-caret-left"></i></span>
                                                    </a>
                                                </li>
                                                <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript:;">...</a></li>
                                                <li class="page-item">
                                                    <a class="page-link page-next" href="javascript:;" aria-label="Next">
                                                        <span aria-hidden="true"><i class="icofont-caret-right"></i></span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>

                            <!-- COL RIGHT -->
                            <div class="col-sm-12 col-xl-3 px-sm-2">
                                <!-- TAB -->
                                <ul class="row mx-sm-n2 nav nav-tabs with-cus-2" id="" role="tablist">
                                    <li class="col-sm-12 px-sm-2 nav-item text-center">
                                        <a class="nav-link active" id="tab-alone" 
                                            data-toggle="tab" href="#tab-content-alone" role="tab" 
                                            aria-controls="tab-content-alone" aria-selected="true">TIN TUYỂN DỤNG HOT</a>
                                    </li>
                                </ul>

                                <!-- TAB CONTENT -->
                                <div class="tab-content with-cus-2" id="tab-content-alone">
                                    <div class="tab-pane px-4 px-xl-4 with-cus fade show active" id="" role="tabpanel" aria-labelledby="">
                                        
                                        <!-- CONTENT -->
                                        <div class="row">
                                            <?php for ($x = 0; $x <= 5; $x++) { ?>
                                                <div class="col-12 col-md-6 col-xl-12 py-3 py-md-4">
                                                    <div class="row">
                                                        <!-- TIME -->
                                                        <div class="col-sm-12">
                                                            <a href="javascript:;" class="text-bold text-uper clo-ora mb-1 lihe20">
                                                                Công ty cổ phần đầu tư TM quốc tế mặt trời đỏ (REDSUN)
                                                            </a>
                                                        </div>
                                                        
                                                        <!-- CONTENT -->
                                                        <div class="col-sm-12 mt-3">
                                                            <div class="row mx-xl-n1">
                                                                <div class="col-6 col-xl-4 px-1 px-md-2 px-xl-1 text-right">
                                                                    <img class="mw130" src="./assets/images/ctyRedsun.jpg" alt="" class="">
                                                                </div>
                                                                <div class="col-6 col-xl-8 px-1 px-md-2 px-xl-1">
                                                                    <p class="mb-0">Cần tuyển</p>
                                                                    <ul class="mb-0 pl-4 pt-1 text-left">
                                                                        <li><p class="mb-1 mb-xl-0"">Công nhân</p></li>
                                                                        <li><p class="mb-1 mb-xl-0">Công nhân SX thịt</p></li>
                                                                    </ul>
                                                                </div>
                                                                
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                            
                                        <!-- BUTTON VIEW MORE -->
                                        <div class="text-right btn-wrap">
                                            <a href="javascript:;" class="btn btn-custom ora">Xem Thêm</a>
                                        </div>
                                        
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
<?php include 'footer.php'; ?>