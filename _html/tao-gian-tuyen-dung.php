<?php include 'header.php'; ?>
    
<div class="page-ab">

    <!-- BLOCK SLIDER -->
    <section class="container-fluid sec-main-slider">
        <div class="row">
            <div class="inner">
                <div class="owl-carousel owl-theme main-slider big-slider">
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                    <div class="item"><img src="./assets/images/main-slider.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </section>

    
    <!-- BLOCK DANG TIN TUYEN DUNG -->
    <section class="container-fluid sec-apply py-4 py-md-5">
        <div class="container">
            <!-- HEADLINE -->
            <div class="row">
                <div class="col-sm-12 text-center ">
                    <span class="ico ico-cmt w-34"></span>
                    <h3 class="headline text-bold mt-2 mt-md-3 mb-4 mb-md-5">Tạo Gian Tuyển Dụng</h3>
                </div>
            </div>

            
            <form>
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-sm-12">
                    
                        <!-- Headline -->
                        <h6 class="text-bold text-uper clo-ora mb-3">Thông Tin Nhà Tuyển Dụng</h6>
                        
                        <!-- Tên CTY -->
                        <div class="form-row mb-2 mb-lg-3">
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Tên Gian Tuyển Dụng (Tên Công Ty) <span class="clo-ora">*</span></label>
                                    <input class="form-control" type="text" 
                                    placeholder="VD: Công Ty TNHH ABC">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <div class="form-group">
                                    <label for="">Tên Công Ty </label>
                                    <input class="form-control" type="number" 
                                    placeholder="VD: Công Ty TNHH ABC">
                                </div>
                            </div>
                        </div>

                        <!-- Upload -->
                        <div class="form-row mb-2 mb-lg-3">
                            <div class="col-sm-12 col-md-12 col-xl-12">
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item mr-2">
                                        <a class="nav-link active btn btn-custom btn-custom-2" id="pills-home-tab" 
                                        data-toggle="pill" href="#pills-home" role="tab" 
                                        aria-controls="pills-home" aria-selected="true"><i class="icofont-upload mr-2"></i> TẢI LOGO LÊN</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link btn btn-custom btn-custom-2" id="pills-profile-tab" 
                                        data-toggle="pill" href="#pills-profile" role="tab" 
                                        aria-controls="pills-profile" aria-selected="false"><i class="icofont-plus-square mr-2"></i> TẠO LOGO MỚI</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                        <div class="row mb-2 mb-lg-3">
                                            <div class="col">
                                                <img id="uploadLogoPreview" class="w100p mb-2 mb-md-0" src="http://fpoimg.com/600x600" alt="">
                                            </div>
                                            <div class="col-sm-6 col-md-9 col-xl-10 align-self-end">
                                                <div class="custom-file-my d-flex">
                                                    <input type="file" 
                                                        name="file-1[]" 
                                                        id="file-uploadlogo" 
                                                        class="inputfile" 
                                                        data-multiple-caption='<i class="icofont-check"></i> Đã chọn {count} files' multiple="">
                                                    <label for="file-uploadlogo" class="">
                                                        <span class="text-bold text-uper"><i class="icofont-upload-alt"></i> Upload</span>
                                                    </label>
                                                </div>
                                                <div class=" d-flex">
                                                    <span class="text-12">Đăng hình dưới dạng tập tin PNG, JPG dung lượng không vượt quá 3MB</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                        v
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Editor -->
                        <div class="form-row mb-2 mb-lg-3">
                            <div class="col-sm-12 col-md-12 col-xl-12">
                                <label for="">Giới Thiệu Chung <span class="clo-ora">*</span></label>
                                <div class="form-group">
                                    <textarea name="" class="editor" cols="1" rows="1" ></textarea>
                                </div>
                            </div>
                        </div>

                        <!-- Quy Mô, Địa Điểm -->
                        <div class="form-row ">
                            <div class="col-sm-12 col-md-12 col-xl-6 order-last order-md-first">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-12 col-xl-12">
                                        <div class="form-group">
                                            <label for="">Quy Mô Công Ty</label>
                                            <select class="select-2" name="">
                                                <option value="AL">Chọn</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-xl-12">
                                        <div class="form-group">
                                            <label for="">Vốn Đầu Tư Nước Ngoài </label>
                                            <select class="select-2" name="">
                                                <option value="AL">Chọn</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-xl-12">
                                        <div class="form-group">
                                            <label for="">Website </label>
                                            <input class="form-control" type="text" 
                                            placeholder="VD: www.tencongty.com">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-xl-12">
                                        <div class="form-group">
                                            <label for="">Email </label>
                                            <input class="form-control" type="email" 
                                            placeholder="VD: www.tencongty.com">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-xl-12">
                                        <div class="form-group">
                                            <label for="">Thời Gian Vip Kết Thúc </label>
                                            <input class="form-control" type="email" 
                                            placeholder="VD: www.tencongty.com">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-xl-6 order-first order-md-last">
                                <div class="form-row align-items-end">
                                    <div class="col-sm-12 col-md-12 col-xl-12">
                                        <div class="form-group">
                                            <label for="">Địa Chỉ </label>
                                            <input class="form-control" type="text" 
                                            placeholder="VD: 120 ABC XYZ">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-xl-12">
                                        <div class="form-group">
                                            <div class="my-map" id="map-my">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15677.985992490378!2d106.6929876!3d10.7732337!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x559552e530933b04!2sNguyen%20Du%20Gymnasium!5e0!3m2!1sen!2s!4v1566838639556!5m2!1sen!2s" width="100%" height="320" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Thông Tin Liên Hệ -->
                        <div class="" id="form-contact" data-quantity="1">
                            <div class="form-row align-items-end" id="form-contact-1">
                                <div class="col-sm-12 col-md-4 col-xl-4">
                                    <div class="form-group">
                                        <label for="">Người Liên Hệ <span class="clo-ora">*</span></label>
                                        <input class="form-control" type="text" 
                                        placeholder="Họ Tên">
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-4 col-xl-4">
                                    <div class="form-group">
                                        <input class="form-control" type="email" 
                                        placeholder="Họ Tên">
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-4 col-xl-4">
                                    <div class="form-group">
                                        <input class="form-control" type="number" 
                                        placeholder="Điện Thoại">
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-4 col-xl-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" 
                                        placeholder="Chức Vụ">
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-4 col-xl-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" 
                                        placeholder="Skype">
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-4 col-xl-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" 
                                        placeholder="Yahoo">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Them lien he -->
                        <div class="form-row align-items-end">
                            <div class="col-12 text-right">
                                <div class="form-group">
                                    <a href="javascript:;" id="addOne"><i class="clip-plus-circle-2 mr-1 ico-top-1"></i>Thêm người liên hệ</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- SUBMIT -->
                <div class="row mt-3 mt-md-4 mt-lg-5">
                    <!-- CONTENT -->
                    <div class="col-sm-12">

                        <div class="form-row">
                            <div class="col-sm-12 col-xl-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-custom ora text-uper mr-2">ĐĂNG TIN</button>
                                    <button type="submit" class="btn btn-custom blu text-uper">Hủy Bỏ</button>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <!-- Thông Tin Liên Hệ Template -->
                <div class="d-none" id="form-contact-template">
                    <div class="form-row align-items-end">
                        <div class="col-sm-12 col-md-12 col-xl-12">
                            <div class="form-row">
                                <div class="col">
                                    <label for="">Người Liên Hệ {{num_contact}}</label>
                                </div>

                                <div class="col text-right delete-contact">
                                    <a class="mb-10" href="javascript:;" onclick="deleteContact({{id}})" >
                                        <i class="icofont-trash mr-2"></i> Xóa liên hệ {{num_quantity}}</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-xl-4">
                            <div class="form-group">
                                <input class="form-control" type="text" 
                                        placeholder="Họ Tên">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-xl-4">
                            <div class="form-group">
                                <input class="form-control" type="email" 
                                placeholder="Họ Tên">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-xl-4">
                            <div class="form-group">
                                <input class="form-control" type="number" 
                                placeholder="Điện Thoại">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-xl-4">
                            <div class="form-group">
                                <input class="form-control" type="text" 
                                placeholder="Chức Vụ">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-xl-4">
                            <div class="form-group">
                                <input class="form-control" type="text" 
                                placeholder="Skype">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-xl-4">
                            <div class="form-group">
                                <input class="form-control" type="text" 
                                placeholder="Yahoo">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>  
<?php include 'footer.php'; ?>

<script>
    
    $(document).ready(function() {
        $(document).on('click', '#addOne', function() {

            var quantity_id = $('#form-contact').attr('data-quantity');
            if (quantity_id > 2) {
                $("#mdError").modal();
                showModalErr('Error!', 'Đã đạt giới hạn số lượng người liên hệ!', 'danger');
                return;
            }

            var html = $('#form-contact-template').html();
            var html_done = html.replace('{{id}}', parseInt(quantity_id) + 1)
                                .replace('{{num_contact}}', parseInt(quantity_id) + 1)
                                .replace('{{num_quantity}}', parseInt(quantity_id) + 1)

            $('#form-contact').attr('data-quantity', (parseInt(quantity_id) + 1));
            $('#form-contact')
                .append(('<div id="form-contact-' + 
                    (parseInt(quantity_id) + 1) + '">' + html_done + '</div>')
                );
            return;
        });

        // -----> Xoa contact
        deleteContact = (num) => {
            
            var quantity_id = $('#form-contact').attr('data-quantity');
            if (quantity_id == 1) {
                showModalErr('Có lỗi xảy ra!', 'Không the xóa hết!', 'danger');
                return;
            }
            $('#form-contact-' + num).remove();
            $('#form-contact').attr('data-quantity', (parseInt(quantity_id) - 1));
            return;
        }
    });
    
</script>